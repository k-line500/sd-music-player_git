#if !defined(AFX_COLORBUTTON_H__BF85D284_2DE8_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_COLORBUTTON_H__BF85D284_2DE8_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorButton.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CColorButton ウィンドウ

class CColorButton : public CButton
{
	DECLARE_DYNAMIC(CColorButton)
// コンストラクション
public:
	CColorButton();

// アトリビュート
public:

// オペレーション
public:
	BOOL AutoLoad(const UINT nID, CWnd* pParent);
	void f_Repaint();

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CColorButton)
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CColorButton();

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CColorButton)
	afx_msg void OnPaint();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_COLORBUTTON_H__BF85D284_2DE8_11D7_BC30_009027BFCD6A__INCLUDED_)
