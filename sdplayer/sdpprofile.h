// SdpProfile.h: CSdpProfile クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDPPROFILE_H__38E81084_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPPROFILE_H__38E81084_91C0_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSdpProfile  
{
public:
	CSdpProfile();
	virtual ~CSdpProfile();
	void f_write_profile();
	void f_read_profile();

	BOOL opt_undisp_rep;
	CStringArray opt_cb_items;
	CString opt_cb_usertext;
	int opt_cb_sel;
	int opt_fadeout_len;

};

#endif // !defined(AFX_SDPPROFILE_H__38E81084_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
