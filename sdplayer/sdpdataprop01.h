#if !defined(AFX_SDPDATAPROP01_H__13922204_910F_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPDATAPROP01_H__13922204_910F_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SdpDataProp01.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSdpDataProp01 ダイアログ

class CSdpDataProp01 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSdpDataProp01)

// コンストラクション
public:
	CSdpDataProp01();
	~CSdpDataProp01();

// ダイアログ データ
	//{{AFX_DATA(CSdpDataProp01)
	enum { IDD = IDD_DataProp01 };
	BOOL	m_relpath;
	BOOL	m_relpath2;
	//}}AFX_DATA
	CMusic *music;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。

	//{{AFX_VIRTUAL(CSdpDataProp01)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CSdpDataProp01)
	afx_msg void OnBUTTONFile();
	afx_msg void OnBUTTONFile2();
	virtual BOOL OnInitDialog();
	afx_msg void OnCHECKRelPath();
	afx_msg void OnCHECKRelPath2();
	afx_msg void OnCueSheet();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPDATAPROP01_H__13922204_910F_11D7_BC30_009027BFCD6A__INCLUDED_)
