// MainFrm.h : CMainFrame クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__D049956E_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_MAINFRM_H__D049956E_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ChildFrm.h"
#include "DlgBar.h"

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// アトリビュート
private:
	int cx0;
	int cy0;
	int cx1;
	int cy1;
	int panel_no;
public:
	CFont *m_view_font;
	int m_view_column_order[SDP_DATABASE_ITEMS];
	int m_view_column_width[SDP_DATABASE_ITEMS];
	void f_InitColumnWidth();
	CString m_find_str;

// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CMainFrame();
	void f_SetStatusText(int no,const char *text);
	class CSdPlayerView *f_GetView();
	class CSdPlayerDoc *f_GetDocument();
	CStringArray *f_GetDocumentData();
	void f_SetDocumentData(CStringArray *sa);
	void f_AddDocumentData(CStringArray *sa);
	CString f_GetDocumentDir();
	void f_NextWindow(int n);
	void f_ChangeCPanel(int n);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // コントロール バー用メンバ
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
	CDlgBar     m_wndDlgBar;

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnEditData();
	afx_msg void OnAddData();
	afx_msg void OnRADIOPatter();
	afx_msg void OnRADIOSinging();
	afx_msg void OnLoad();
	afx_msg void OnUnLoad();
	afx_msg void OnRewind();
	afx_msg void OnPlay();
	afx_msg void OnRepeat();
	afx_msg void OnTipTimer();
	afx_msg void OnPlayByKey();
	afx_msg void OnRepeatByKey();
	afx_msg void OnUpdateLoad(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlay(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRepeat(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRewind(CCmdUI* pCmdUI);
	afx_msg void OnUpdateUnLoad(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTipTimer(CCmdUI* pCmdUI);
	afx_msg void OnNextWindow();
	afx_msg void OnVIEWPlayerControl();
	afx_msg void OnUpdateVIEWPlayerControl(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePatter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSinging(CCmdUI* pCmdUI);
	afx_msg void OnReset();
	afx_msg void OnUpdateReset(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnClose();
	afx_msg void OnWinStdSize();
	afx_msg void OnVIEWTimeDisplay();
	afx_msg void OnUpdateVIEWTimeDisplay(CCmdUI* pCmdUI);
	afx_msg void OnSetFont();
	afx_msg void OnWinFlatSize();
	afx_msg void OnSaveColumnOrder();
	afx_msg void OnResetColumnOrder();
	afx_msg void OnSaveColumnWidth();
	afx_msg void OnResetColumnWidth();
	afx_msg void OnCueSheet();
	afx_msg void OnUpdateCueSheet(CCmdUI* pCmdUI);
	afx_msg void OnSetOptions();
	afx_msg void OnEditFind();
	//}}AFX_MSG
	//
    afx_msg LRESULT OnSetMessageString(WPARAM wParam, LPARAM lParam);
	afx_msg void OnCPanel(UINT nCommandID);
	afx_msg void OnUpdateCPanel(CCmdUI* pCmdUI);
	afx_msg LONG OnFindReplace(WPARAM wParam, LPARAM lParam);
	//
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_MAINFRM_H__D049956E_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
