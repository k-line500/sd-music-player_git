// SdPlayerView.h : CSdPlayerView クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDPLAYERVIEW_H__D0499574_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPLAYERVIEW_H__D0499574_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSdPlayerView : public CListView
{
protected: // シリアライズ機能のみから作成します。
	CSdPlayerView();
	DECLARE_DYNCREATE(CSdPlayerView)

// アトリビュート
public:
	CSdPlayerDoc* GetDocument();

// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSdPlayerView)
	public:
	virtual void OnDraw(CDC* pDC);  // このビューを描画する際にオーバーライドされます。
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // 構築後の最初の１度だけ呼び出されます。
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	void f_SetColumnOrder();
	void f_SetColumnOrder(int *order);
	void f_GetColumnOrder(int *order);
	void f_SetColumnWidth();
	void f_GetColumnWidth(int *width);
	void f_SetFont();
	virtual ~CSdPlayerView();
	void f_set_check(int i);
	void f_get_check();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	int m_char_width;

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CSdPlayerView)
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnEDITMoveUp();
	afx_msg void OnUpdateEDITMoveUp(CCmdUI* pCmdUI);
	afx_msg void OnEDITMoveDown();
	afx_msg void OnUpdateEDITMoveDown(CCmdUI* pCmdUI);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRandomSelect();
	afx_msg void OnUpdateRandomSelect(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectAll();
	afx_msg void OnFILEReOpen();
	afx_msg void OnUpdateFILEReOpen(CCmdUI* pCmdUI);
	afx_msg void OnFILEAdd();
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnClearAllCheck();
	afx_msg void OnPathAbsToRel();
	afx_msg void OnPathRelToAbs();
	afx_msg void OnClearCheck();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // SdPlayerView.cpp ファイルがデバッグ環境の時使用されます。
inline CSdPlayerDoc* CSdPlayerView::GetDocument()
   { return (CSdPlayerDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPLAYERVIEW_H__D0499574_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
