// SdpDataProp01.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "Music.h"
#include "SdpDataProp01.h"
#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdpDataProp01 プロパティ ページ

IMPLEMENT_DYNCREATE(CSdpDataProp01, CPropertyPage)

CSdpDataProp01::CSdpDataProp01() : CPropertyPage(CSdpDataProp01::IDD)
{
	//{{AFX_DATA_INIT(CSdpDataProp01)
	m_relpath = FALSE;
	m_relpath2 = FALSE;
	//}}AFX_DATA_INIT
}

CSdpDataProp01::~CSdpDataProp01()
{
}

void CSdpDataProp01::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSdpDataProp01)
	DDX_Check(pDX, IDC_CHECK_RelPath, m_relpath);
	DDX_Check(pDX, IDC_CHECK_RelPath2, m_relpath2);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT_CueFile, music->m_cue_file);
	DDX_Text(pDX, IDC_EDIT_CueProg, music->m_cue_prog);
}


BEGIN_MESSAGE_MAP(CSdpDataProp01, CPropertyPage)
	//{{AFX_MSG_MAP(CSdpDataProp01)
	ON_BN_CLICKED(IDC_BUTTON_File, OnBUTTONFile)
	ON_BN_CLICKED(IDC_BUTTON_File2, OnBUTTONFile2)
	ON_BN_CLICKED(IDC_CHECK_RelPath, OnCHECKRelPath)
	ON_BN_CLICKED(IDC_CHECK_RelPath2, OnCHECKRelPath2)
	ON_BN_CLICKED(IDC_CueSheet, OnCueSheet)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdpDataProp01 メッセージ ハンドラ

BOOL CSdpDataProp01::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: この位置に初期化の補足処理を追加してください
	m_relpath=SdpPathIsRelative(music->m_cue_file);

	if (music->m_cue_prog!="")
	{
		m_relpath2=SdpPathIsRelative(music->m_cue_prog);
	}

	UpdateData(FALSE);
	
	return TRUE;  // コントロールにフォーカスを設定しないとき、戻り値は TRUE となります
	              // 例外: OCX プロパティ ページの戻り値は FALSE となります
}

void CSdpDataProp01::OnBUTTONFile() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CFileDialog *fd;
	UpdateData(TRUE);
	fd=new CFileDialog(TRUE,NULL,NULL,OFN_HIDEREADONLY,_T("All Files (*.*)|*.*||"));
	fd->m_ofn.lpstrInitialDir=music->m_base_dir;
	if (fd->DoModal()==IDOK)
	{
		music->m_cue_file=fd->GetPathName();
		if (m_relpath)
		{
			m_relpath=SdpPathAbsToRel(music->m_cue_file,music->m_base_dir,music->m_cue_file);
		}
	}
	delete fd;
	UpdateData(FALSE);
}

void CSdpDataProp01::OnBUTTONFile2() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CFileDialog *fd;
	UpdateData(TRUE);
	fd=new CFileDialog(TRUE,NULL,NULL,OFN_HIDEREADONLY,_T("exe files (*.exe)|*.exe|All Files (*.*)|*.*||"));
	fd->m_ofn.lpstrInitialDir=music->m_base_dir;
	if (fd->DoModal()==IDOK)
	{
		music->m_cue_prog=fd->GetPathName();
		if (m_relpath2)
		{
			m_relpath2=SdpPathAbsToRel(music->m_cue_prog,music->m_base_dir,music->m_cue_prog);
		}
	}
	delete fd;
	UpdateData(FALSE);
}

void CSdpDataProp01::OnCHECKRelPath() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	if (m_relpath)
	{
		m_relpath=SdpPathAbsToRel(music->m_cue_file,music->m_base_dir,music->m_cue_file);
	}
	else
	{
		m_relpath=!SdpPathRelToAbs(music->m_cue_file,music->m_base_dir,music->m_cue_file);
	}
	UpdateData(FALSE);
}

void CSdpDataProp01::OnCHECKRelPath2() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	if (m_relpath2)
	{
		m_relpath2=SdpPathAbsToRel(music->m_cue_prog,music->m_base_dir,music->m_cue_prog);
	}
	else
	{
		m_relpath2=!SdpPathRelToAbs(music->m_cue_prog,music->m_base_dir,music->m_cue_prog);
	}
	UpdateData(FALSE);
}

void CSdpDataProp01::OnCueSheet() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	int res=0;
	CString str;
	UpdateData(TRUE);
	music->m_cue_file.TrimLeft();
	music->m_cue_prog.TrimLeft();
	if (music->m_cue_file=="")
	{
		res=ERROR_FILE_NOT_FOUND;
	}
	else if (music->m_cue_prog=="")
	{
		res=(int)ShellExecute(NULL,"open",music->m_cue_file,NULL,music->m_base_dir,SW_SHOWNORMAL);
	}
	else
	{
		res=(int)ShellExecute(NULL,"open",music->m_cue_prog,"\""+music->m_cue_file+"\"",music->m_base_dir,SW_SHOWNORMAL);
	}

	if (res>32)
	{
		return;
	}

	switch (res)
	{
	case 0:
		str="out of memory";
		break;
	case ERROR_FILE_NOT_FOUND:
		str="file not found";
		break;
	case ERROR_PATH_NOT_FOUND:
		str="pass not found";
		break;
	case ERROR_BAD_FORMAT:
		str=".exe file is invalid";
		break;
	case SE_ERR_ACCESSDENIED:
		str="access denied";
		break;
	case SE_ERR_ASSOCINCOMPLETE:
		str="association incomplete";
		break;
	case SE_ERR_NOASSOC:
		str="no application associated";
		break;
	default:
		str.Format("file open error (%d)",res);
		break;
	}

	str="*** "+str+" ***\n\n file: "+music->m_cue_file+"\n prog: "+music->m_cue_prog+"\n dir: "+music->m_base_dir;
	AfxMessageBox(str);
}
