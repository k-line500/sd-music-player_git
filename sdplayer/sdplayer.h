// SdPlayer.h : SDPLAYER アプリケーションのメイン ヘッダー ファイル
//

#if !defined(AFX_SDPLAYER_H__D0499569_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPLAYER_H__D0499569_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // メイン シンボル

#define SDP_DATABASE_ITEMS 20

#include "SdpProfile.h"

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerApp:
// このクラスの動作の定義に関しては SdPlayer.cpp ファイルを参照してください。
//

class CSdPlayerApp : public CWinApp
{
public:
	CSdPlayerApp();
	CSdpProfile m_profile;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSdPlayerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// インプリメンテーション
//	void f_SetStatusText(int no,const char *text);

	COleTemplateServer m_server;
		// ドキュメントを作成するためのサーバー オブジェクト
	//{{AFX_MSG(CSdPlayerApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileOpen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPLAYER_H__D0499569_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
