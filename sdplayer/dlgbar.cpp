// DlgBar.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "SdPlayer.h"
#include "DlgBar.h"
#include "MainFrm.h"
#include "SdpDataBox.h"
#include "Music.h"
#include "MusicWave.h"
#include "SdPlayerDoc.h"
#include "SdPlayerView.h"
#include "Global.h"

#include <afxole.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CDlgBar

CDlgBar::CDlgBar()
{
	m_music_sel=0;
	music=music_data[0]=music_data[1]=NULL;
	m_timer_mode=-1;
	m_timer_state=0;
	m_timer_count=0;
	m_tip_timer = _T("00:00");
	m_rest_time = 0;
	m_tip_time = 0;
}

CDlgBar::~CDlgBar()
{
	f_DestroySubWindow();
}

/////////////////////////////////////////////////////////////////////////////

void CDlgBar::DoDataExchange(CDataExchange* pDX) 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください
	CDialogBar::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_EDIT_Title1, music_data[0]->m_music_name);
	DDX_Text(pDX, IDC_EDIT_Title2, music_data[1]->m_music_name);
	DDX_Radio(pDX, IDC_RADIO_Patter, m_music_sel);
	DDX_Text(pDX, IDC_EDIT_MusicInfo, music->m_music_info);
	DDX_Text(pDX, IDC_EDIT_OutDevice, music->m_output_device_info);

	DDX_Control(pDX, IDC_SLIDER_Time, m_time_slider);
	DDX_Control(pDX, IDC_SPIN_Rest, m_resttime_updown);
	DDX_Control(pDX, IDC_SPIN_Tip, m_tiptime_updown);
	DDX_Control(pDX, IDC_SLIDER_Speed, m_speed_slider);
	DDX_Control(pDX, IDC_SLIDER_Pitch, m_pitch_slider);
	DDX_Control(pDX, IDC_SLIDER_Tempo, m_tempo_slider);

	DDX_Text(pDX, IDC_EDIT_Tempo2, music->m_tempo_bpm);
	DDX_Text(pDX, IDC_EDIT_Time, music->m_time);
	DDX_Text(pDX, IDC_EDIT_Length, music->m_music_len);
	DDX_Text(pDX, IDC_EDIT_Speed, music->m_speed_control);
	DDX_Text(pDX, IDC_EDIT_Pitch, music->m_pitch_control);
	DDX_Text(pDX, IDC_EDIT_Tempo, music->m_tempo_control);
	DDX_Check(pDX, IDC_Repeat, music->m_stat_repeat);
	DDX_Check(pDX, IDC_Play, music->m_play_on);

	DDX_Text(pDX, IDC_EDIT_Rest, m_rest_time);
	DDX_Text(pDX, IDC_EDIT_Tip, m_tip_time);
	DDX_Text(pDX, IDC_EDIT_TipTimer, m_tip_timer);
	DDX_Radio(pDX, IDC_RADIO_TipPatter, m_timer_mode);
}

/////////////////////////////////////////////////////////////////////////////

BEGIN_MESSAGE_MAP(CDlgBar, CDialogBar)
	//{{AFX_MSG_MAP(CDlgBar)
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI(IDC_UnLoad, OnUpdateUnLoad)
	ON_UPDATE_COMMAND_UI(IDC_Load, OnUpdateLoad)
	ON_UPDATE_COMMAND_UI(IDC_Rewind, OnUpdateRewind)
	ON_UPDATE_COMMAND_UI(IDC_Play, OnUpdatePlay)
	ON_UPDATE_COMMAND_UI(IDC_Repeat, OnUpdateRepeat)
	ON_UPDATE_COMMAND_UI(IDC_Reset, OnUpdateReset)
	ON_UPDATE_COMMAND_UI(IDC_RADIO_Patter, OnUpdatePatter)
	ON_UPDATE_COMMAND_UI(IDC_RADIO_Singing, OnUpdateSinging)
	ON_UPDATE_COMMAND_UI(IDC_TipTimer, OnUpdateTipTimer)
	ON_UPDATE_COMMAND_UI(IDC_CueSheet, OnUpdateCueSheet)
	ON_UPDATE_COMMAND_UI_RANGE(IDC_RADIO_TipPatter, IDC_RADIO_TipRest, OnUpdateTimerMode)
	ON_UPDATE_COMMAND_UI_RANGE(IDC_SLIDER_Speed, IDC_EDIT_Tempo, OnUpdateSlider)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgBar メッセージ ハンドラ

void CDlgBar::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	CDataExchange dx(this,FALSE);

	if ((CSliderCtrl*)pScrollBar==&m_time_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				if (music->m_play_on) {music->KillTimer(1);}
				music->m_time.Format("%02d:%02d",(m_time_slider.GetPos()/60)%100,m_time_slider.GetPos()%60);
				DDX_Text(&dx, IDC_EDIT_Time, music->m_time);
				break;
			case TB_ENDTRACK:
				music->f_Reset();
				music->m_wait=0;
				music->f_SetStartTime(m_time_slider.GetPos());
				music->SendMessage(WM_TIMER,0);
				music->OnPlay();
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_speed_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetSpeedData(m_speed_slider.GetPos());
//				m_pitch_slider.SetPos(music->m_pitch);
//				m_tempo_slider.SetPos(music->m_tempo);
				if (music->m_speed >=m_speed_slider.GetRangeMax())
				{
					m_speed_slider.SetRange(m_speed_slider.GetRangeMin()+1,m_speed_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_speed <=m_speed_slider.GetRangeMin())
				{
					m_speed_slider.SetRange(m_speed_slider.GetRangeMin()-1,m_speed_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangeSpeed();
				m_speed_slider.SetPos(music->m_speed);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_pitch_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetPitchData(m_pitch_slider.GetPos());
				if (music->m_pitch >=m_pitch_slider.GetRangeMax())
				{
					m_pitch_slider.SetRange(m_pitch_slider.GetRangeMin()+1,m_pitch_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_pitch <=m_pitch_slider.GetRangeMin())
				{
					m_pitch_slider.SetRange(m_pitch_slider.GetRangeMin()-1,m_pitch_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangePitch();
				m_pitch_slider.SetPos(music->m_pitch);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_tempo_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetTempoData(m_tempo_slider.GetPos());
//				m_speed_slider.SetPos(music->m_speed);
				if (music->m_tempo >=m_tempo_slider.GetRangeMax())
				{
					m_tempo_slider.SetRange(m_tempo_slider.GetRangeMin()+1,m_tempo_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_tempo <=m_tempo_slider.GetRangeMin())
				{
					m_tempo_slider.SetRange(m_tempo_slider.GetRangeMin()-1,m_tempo_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangeTempo();
				m_tempo_slider.SetPos(music->m_tempo);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}
}

BOOL CDlgBar::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください
	CMainFrame *mwd;
	mwd=(CMainFrame*)AfxGetMainWnd();

    switch (pMsg->message)
    {
    case WM_KEYDOWN:
        switch (pMsg->wParam)
        {
        case VK_TAB:
			if (GetKeyState(VK_CONTROL)&0x80)
			{
				mwd->f_NextWindow(1);
				return TRUE;
			}
			if (GetFocus()!=GetDlgItem(IDC_EDIT_Tip) && IsDialogMessage( pMsg ) )
		        return TRUE;
            break;
        }
    }
    	
	return CDialogBar::PreTranslateMessage(pMsg);
}


//DEL int CDlgBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
//DEL {
//DEL 	if (CDialogBar::OnCreate(lpCreateStruct) == -1)
//DEL 		return -1;
//DEL 	
//DEL 	// TODO: この位置に固有の作成用コードを追加してください
//DEL 	music=new CMusicWave;
//DEL 	music->m_dialog=this;
//DEL 	RECT rect1={0,0,100,100};
//DEL 	music->Create(NULL,"Music Data 1",WS_CHILD,rect1,this,1);
//DEL 	music_data[0]=music;
//DEL 
//DEL 	RECT rect2={100,0,100,100};
//DEL 	music=new CMusicWave;
//DEL 	music->m_dialog=this;
//DEL 	music->Create(NULL,"Music Data 2",WS_CHILD,rect2,this,2);
//DEL //	CString classname=AfxRegisterWndClass(0,0,(HBRUSH)COLOR_WINDOW);
//DEL //	music->CreateEx(WS_EX_CLIENTEDGE,classname,"Music Data 2",WS_OVERLAPPEDWINDOW,rect2,this,0);
//DEL //	music->ShowWindow(SW_SHOW);
//DEL 	music_data[1]=music;
//DEL 
//DEL 	music=music_data[0];
//DEL 
//DEL 	int w=AfxGetApp()->GetProfileInt("TimeDisp","w",80);
//DEL 	int h=AfxGetApp()->GetProfileInt("TimeDisp","h",48);
//DEL 	int x=AfxGetApp()->GetProfileInt("TimeDisp","x",::GetSystemMetrics(SM_CXSCREEN)-100);
//DEL 	int y=AfxGetApp()->GetProfileInt("TimeDisp","y",0);
//DEL 
//DEL 	if (w>::GetSystemMetrics(SM_CXSCREEN))
//DEL 	{
//DEL 		w=::GetSystemMetrics(SM_CXSCREEN);
//DEL 	}
//DEL 	if (h>::GetSystemMetrics(SM_CYSCREEN))
//DEL 	{
//DEL 		h=::GetSystemMetrics(SM_CYSCREEN);
//DEL 	}
//DEL 	if (x>::GetSystemMetrics(SM_CXSCREEN))
//DEL 	{
//DEL 		x=::GetSystemMetrics(SM_CXSCREEN)-100;
//DEL 	}
//DEL 	if (y>::GetSystemMetrics(SM_CYSCREEN))
//DEL 	{
//DEL 		y=::GetSystemMetrics(SM_CYSCREEN)-100;
//DEL 	}
//DEL 
//DEL 	
//DEL 	RECT rect3;
//DEL 	rect3.left=x;
//DEL 	rect3.right=x+w;
//DEL 	rect3.top=y;
//DEL 	rect3.bottom=y+h;
//DEL 	m_pwndTimeDisp=new CTimeDisp;
//DEL 	m_pwndTimeDisp->Create(NULL,"TipTimer",WS_OVERLAPPED|WS_THICKFRAME,rect3,this,NULL,WS_EX_TOPMOST|WS_EX_TOOLWINDOW,NULL);
//DEL //	m_pwndTimeDisp->Create(AfxRegisterWndClass(NULL),"TipTimer",WS_OVERLAPPED|WS_THICKFRAME,rect3,this,NULL,WS_EX_TOPMOST|WS_EX_TOOLWINDOW,NULL);
//DEL 
//DEL 	int on=AfxGetApp()->GetProfileInt("TimeDisp","on",0);
//DEL 	if (on)
//DEL 	{
//DEL 		m_pwndTimeDisp->ShowWindow(SW_SHOW);
//DEL 	}
//DEL 
//DEL 	return 0;
//DEL }

void CDlgBar::f_CreateSubWindow()
{
	music=new CMusicWave;
	music->m_dialog=this;
	RECT rect1={0,0,100,100};
//	music->Create(NULL,"Music Data 1",WS_CHILD,rect1,this,1);
	music->CreateEx(WS_EX_CLIENTEDGE,AfxRegisterWndClass(0,0,(HBRUSH)COLOR_WINDOW),"Music Data 1",WS_OVERLAPPEDWINDOW,rect1,this,0);
//	music->ShowWindow(SW_SHOW);
	music_data[0]=music;

	RECT rect2={100,0,100,100};
	music=new CMusicWave;
	music->m_dialog=this;
//	music->Create(NULL,"Music Data 2",WS_CHILD,rect2,this,2);
	music->CreateEx(WS_EX_CLIENTEDGE,AfxRegisterWndClass(0,0,(HBRUSH)COLOR_WINDOW),"Music Data 2",WS_OVERLAPPEDWINDOW,rect2,this,0);
//	music->ShowWindow(SW_SHOW);
	music_data[1]=music;

	music=music_data[0];

	int w=AfxGetApp()->GetProfileInt("TimeDisp","w",80);
	int h=AfxGetApp()->GetProfileInt("TimeDisp","h",48);
	int x=AfxGetApp()->GetProfileInt("TimeDisp","x",::GetSystemMetrics(SM_CXSCREEN)-100);
	int y=AfxGetApp()->GetProfileInt("TimeDisp","y",0);

	if (w>::GetSystemMetrics(SM_CXSCREEN))
	{
		w=::GetSystemMetrics(SM_CXSCREEN);
	}
	if (h>::GetSystemMetrics(SM_CYSCREEN))
	{
		h=::GetSystemMetrics(SM_CYSCREEN);
	}
	if (x>::GetSystemMetrics(SM_CXSCREEN))
	{
		x=::GetSystemMetrics(SM_CXSCREEN)-100;
	}
	if (y>::GetSystemMetrics(SM_CYSCREEN))
	{
		y=::GetSystemMetrics(SM_CYSCREEN)-100;
	}

	
	RECT rect3;
	rect3.left=x;
	rect3.right=x+w;
	rect3.top=y;
	rect3.bottom=y+h;
	m_pwndTimeDisp=new CTimeDisp;
	m_pwndTimeDisp->Create(NULL,"TipTimer",WS_OVERLAPPED|WS_THICKFRAME,rect3,this,NULL,WS_EX_TOPMOST|WS_EX_TOOLWINDOW,NULL);
//	m_pwndTimeDisp->Create(AfxRegisterWndClass(NULL),"TipTimer",WS_OVERLAPPED|WS_THICKFRAME,rect3,this,NULL,WS_EX_TOPMOST|WS_EX_TOOLWINDOW,NULL);

	int on=AfxGetApp()->GetProfileInt("TimeDisp","on",0);
	if (on)
	{
		m_pwndTimeDisp->ShowWindow(SW_SHOW);
	}

	return;
}

//DEL void CDlgBar::OnDestroy() 
//DEL {
//DEL 	CDialogBar::OnDestroy();
//DEL 	
//DEL 	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください
//DEL 	music_data[0]->DestroyWindow();
//DEL 	music_data[1]->DestroyWindow();
//DEL 	delete music_data[0];
//DEL 	delete music_data[1];
//DEL 
//DEL //	m_pwndTimeDisp->DestroyWindow();
//DEL //	m_pwndTimeDisp=NULL;
//DEL }

void CDlgBar::f_DestroySubWindow() 
{
	if (music_data[0])
	{
		music_data[0]->DestroyWindow();
		delete music_data[0];
	}
	if (music_data[1])
	{
		music_data[1]->DestroyWindow();
		delete music_data[1];
	}
	music=music_data[0]=music_data[1]=NULL;

//	m_pwndTimeDisp->DestroyWindow();
//	m_pwndTimeDisp=NULL;
}

void CDlgBar::f_InitDialog() 
{
	UpdateData(FALSE);
	m_resttime_updown.SetRange(0,99);
	m_tiptime_updown.SetRange(0,99);
	m_time_slider.SetRange(0,180,TRUE);
	m_time_slider.SetTicFreq(10);
	m_speed_slider.SetRange(-50,50,TRUE);
	m_pitch_slider.SetRange(-60,60,TRUE);
	m_tempo_slider.SetRange(-50,50,TRUE);

	m_patter_radio.AutoLoad(IDC_RADIO_Patter,this);
	m_singing_radio.AutoLoad(IDC_RADIO_Singing,this);
	m_go_button.AutoLoad(IDC_TipTimer,this);
	m_edit_button.AutoLoad(IDC_EditData,this);
	m_add_button.AutoLoad(IDC_AddData,this);
	m_unload_button.AutoLoad(IDC_UnLoad,this);
	m_load_button.AutoLoad(IDC_Load,this);
	m_rewind_button.AutoLoad(IDC_Rewind,this);
	m_play_button.AutoLoad(IDC_Play,this);
	m_repeat_button.AutoLoad(IDC_Repeat,this);
	m_reset_button.AutoLoad(IDC_Reset,this);
}

void CDlgBar::f_RestartDialog() 
{
	if (music->m_stat_load)
	{
		m_time_slider.SetRange(0,(int)(music->f_GetLength())+1,TRUE);
		m_speed_slider.SetRange(music->m_speed-50,music->m_speed+50,TRUE);
		m_speed_slider.SetPos(music->m_speed);
		m_pitch_slider.SetRange(music->m_pitch-60,music->m_pitch+60,TRUE);
		m_pitch_slider.SetPos(music->m_pitch);
		m_tempo_slider.SetRange(music->m_tempo-50,music->m_tempo+50,TRUE);
		m_tempo_slider.SetPos(music->m_tempo);
	}
	if (m_timer_state==1||m_timer_state==3||m_timer_state==5)
	{
		SetTimer(3,1000,NULL);
	}
}

void CDlgBar::f_MusicSelect(int sel)
{
	if (sel==m_music_sel)		{return;}
	else if (sel==0 || sel==1)	{m_music_sel=sel;}
	else						{return;}

	music->f_InActivate();
	music=music_data[m_music_sel];
	music->f_Activate();

	if (music->m_stat_load)
	{
		m_time_slider.SetRange(0,(int)(music->f_GetLength())+1,TRUE);
		m_speed_slider.SetRange(music->m_speed-50,music->m_speed+50,TRUE);
		m_speed_slider.SetPos(music->m_speed);
		m_pitch_slider.SetRange(music->m_pitch-60,music->m_pitch+60,TRUE);
		m_pitch_slider.SetPos(music->m_pitch);
		m_tempo_slider.SetRange(music->m_tempo-50,music->m_tempo+50,TRUE);
		m_tempo_slider.SetPos(music->m_tempo);
	}
	else
	{
		m_time_slider.SetRange(0,180,TRUE);
		m_speed_slider.SetRange(-50,50,TRUE);
		m_speed_slider.SetPos(0);
		m_pitch_slider.SetRange(-60,60,TRUE);
		m_pitch_slider.SetPos(0);
		m_tempo_slider.SetRange(-50,50,TRUE);
		m_tempo_slider.SetPos(0);
	}
	music->f_SetStatusText();
	OnTimer(0);
}

void CDlgBar::OnEditData() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CMainFrame *mwd;
	CStringArray *sa;
	CSdpDataBox db;

	OnUnLoad();

	UpdateData(TRUE);
	mwd=(CMainFrame*)AfxGetMainWnd();
	music->m_base_dir=mwd->f_GetDocumentDir();
	sa=mwd->f_GetDocumentData();

	db.music=music;
	db.music->m_dialog=&db;

	db.music->f_SetData(sa);
	int res=db.DoModal();
	music=music_data[m_music_sel]=db.music;
	music->m_dialog=this;

	switch (res)
	{
		case IDOK:
			if (sa)
			{
				db.music->f_GetData(sa);
				mwd->f_SetDocumentData(sa);
				break;
			}
		case IDC_Add:
			sa = new CStringArray;
			db.music->f_GetData(sa);
			mwd->f_AddDocumentData(sa);
			break;
		default:
			if (sa)
			{
				sa->RemoveAll();
				delete sa;
			}
			mwd->f_SetDocumentData(NULL);
//			OnUnLoad();
			break;
	}
	
	if (music->m_stat_ready)	{OnLoad();}
	else						{OnUnLoad();}
	
	UpdateData(FALSE);
}

void CDlgBar::OnAddData() 
{
	CMainFrame *mwd;
	CStringArray *sa;
	CSdpDataBox db;

	OnUnLoad();

	UpdateData(TRUE);
	mwd=(CMainFrame*)AfxGetMainWnd();
	music->m_base_dir=mwd->f_GetDocumentDir();
	sa=NULL;

	db.music=music;
	db.music->m_dialog=&db;

	int res=db.DoModal();
	music=music_data[m_music_sel]=db.music;
	music->m_dialog=this;

	switch (res)
	{
		case IDOK:
		case IDC_Add:
			sa = new CStringArray;
			db.music->f_GetData(sa);
			mwd->f_AddDocumentData(sa);
			break;
		default:
			mwd->f_SetDocumentData(NULL);
			OnUnLoad();
			break;
	}
	
	if (music->m_stat_ready)	{OnLoad();}
	else						{OnUnLoad();}
	UpdateData(FALSE);
}

void CDlgBar::OnRADIOPatter() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	f_MusicSelect(0);
	UpdateData(FALSE);
}

void CDlgBar::OnRADIOSinging() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	f_MusicSelect(1);
	UpdateData(FALSE);
}

void CDlgBar::OnLoad()
{
	CMainFrame *mwd;
	CStringArray *sa;
	CSdPlayerView *view;
	CString str;

	OnUnLoad();
	UpdateData(TRUE);
	mwd=(CMainFrame*)AfxGetMainWnd();
	music->m_base_dir=mwd->f_GetDocumentDir();
	sa=mwd->f_GetDocumentData();
	view=mwd->f_GetView();
	if (!sa)
	{
		return;
	}
	music->f_SetData(sa);

	music->m_data_type=music->f_GetFileType();
	UpdateData(FALSE);
	if (music->m_data_type<0)
	{
		str="*** "+music->m_status_text0+" ***\n\n file: "+music->m_status_text1+"\n  dir: "+music->m_base_dir;
		AfxMessageBox(str);
		OnUnLoad();
		return;
	}

	if (music->f_GetObjectType()!=music->m_data_type)
	{
		music=CMusic::f_ObjectTypeChange(music);
		music_data[m_music_sel]=music;
	}
	list_data[m_music_sel]=view;

	music->OnLoad();
	if (music->m_stat_load)
	{
		m_time_slider.SetRange(0,(int)(music->f_GetLength())+1,TRUE);
	}
	else
	{
		m_time_slider.SetRange(0,180,TRUE);
	}

	m_speed_slider.SetRange(music->m_speed-50,music->m_speed+50,TRUE);
	m_speed_slider.SetPos(music->m_speed);
	m_pitch_slider.SetRange(music->m_pitch-60,music->m_pitch+60,TRUE);
	m_pitch_slider.SetPos(music->m_pitch);
	m_tempo_slider.SetRange(music->m_tempo-50,music->m_tempo+50,TRUE);
	m_tempo_slider.SetPos(music->m_tempo);

	if (music->m_data_type!=1)
	{
		m_pitch_slider.EnableWindow(FALSE);
		m_tempo_slider.EnableWindow(FALSE);
	}
	else
	{
		m_pitch_slider.EnableWindow(TRUE);
		m_tempo_slider.EnableWindow(TRUE);
	}
	
	UpdateData(FALSE);
}

void CDlgBar::OnUnLoad()
{
	UpdateData(TRUE);
	music->OnUnLoad();
	UpdateData(FALSE);
}

void CDlgBar::OnRewind()
{
	UpdateData(TRUE);
	music->OnRewind();
//	UpdateData(FALSE);
}

void CDlgBar::OnPlay()
{
	UpdateData(TRUE);
//	music->f_SetStartTime(music->f_GetTime() - music->m_wait);
	music->f_SetStartTime(music->f_GetTime());
	music->f_SetEndTime(music->f_GetLength());
	music->OnPlay();
}

void CDlgBar::OnPlayByKey()
{
	UpdateData(TRUE);
	if (music->m_play_on)	music->m_play_on=0;
	else					music->m_play_on=1;
	UpdateData(FALSE);
	OnPlay();
}

void CDlgBar::OnRepeat()
{
	UpdateData(TRUE);
	if (music->m_stat_repeat)
	{
		double time=music->f_GetTime();
		music->f_Reset();
		music->f_SetStartTime(time);
		music->OnPlay();
	}
}

void CDlgBar::OnRepeatByKey()
{
	UpdateData(TRUE);
	if (music->m_stat_repeat)	music->m_stat_repeat=0;
	else						music->m_stat_repeat=1;
	UpdateData(FALSE);
	OnRepeat();
}

void CDlgBar::OnReset()
{
	UpdateData(TRUE);
	double pos=SdpSetStringToTime(music->m_rep_start,2)/1000.0;
	music->f_Reset();
	music->f_SetStartTime(pos);
//	Sleep(500);
	OnTimer(0);
	music->OnPlay();
}

void CDlgBar::OnTimer(UINT nIDEvent) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
 	CDataExchange dx(this,FALSE);
 	CDataExchange dx2(this,TRUE);
	int timer=0;

 	if (nIDEvent==1)
 	{
 		if (!music->m_play_on)
 			DDX_Check(&dx, IDC_Play, music->m_play_on);
 		if (!music->m_stat_repeat)
 			DDX_Check(&dx, IDC_Repeat, music->m_stat_repeat);
	}
	else if (nIDEvent==3)
	{
		m_timer_count++;
		SdpSetTimeToString(m_timer_count,m_tip_timer,1);
		DDX_Text(&dx, IDC_EDIT_TipTimer, m_tip_timer);

		if (m_timer_state<5)
		{
			DDX_Text(&dx2, IDC_EDIT_Tip, m_tip_time);
			timer=m_tip_time*60;
		}
		else
		{
			DDX_Text(&dx2, IDC_EDIT_Rest, m_rest_time);
			timer=m_rest_time*60;
		}
		
		m_pwndTimeDisp->m_back_mode=1;
		if (timer==0)
		{
//			m_pwndTimeDisp->m_back_mode=0;
			m_pwndTimeDisp->m_back_color=RGB(192,192,192); //gray
		}
		else if (m_timer_count<timer-120)
		{
			m_pwndTimeDisp->m_back_color=RGB(255,255,255); //white
		}
		else if (m_timer_count<timer-60)
		{
			m_pwndTimeDisp->m_back_color=RGB(128,255,255); //cyan
		}
		else if (m_timer_count<timer-30)
		{
			m_pwndTimeDisp->m_back_color=RGB(128,255,128); //green
		}
		else if (m_timer_count<timer)
		{
			m_pwndTimeDisp->m_back_color=RGB(255,255,128); //yellow
		}
		else if (m_timer_count<timer+120)
		{
			m_pwndTimeDisp->m_back_color=RGB(255,192,64); //orange
		}
		else if (m_timer_count<timer+300)
		{
			m_pwndTimeDisp->m_back_color=RGB(255,128,128); // pink
		}
		else
		{
			m_pwndTimeDisp->m_back_color=RGB(255,0,0);   // red
		}
		m_pwndTimeDisp->m_time=m_tip_timer;
//		m_pwndTimeDisp->InvalidateRect(NULL,FALSE);
		m_pwndTimeDisp->f_DispTimer();

		if (m_timer_state==5)
		{
//			DDX_Text(&dx2, IDC_EDIT_Rest, m_rest_time);
			if (m_timer_count==timer)
			{
				if (music_data[0] && music_data[0]->m_stat_ready)
				{
					f_MusicSelect(0);
					music->m_stat_repeat=1;
					music->m_play_on=1;
					UpdateData(FALSE);
					OnRewind();
				}
			}
		}
	}

 	if (nIDEvent<3)
 	{
//		DDX_Text(&dx, IDC_EDIT_MusicInfo, music->m_music_info);
		DDX_Text(&dx, IDC_EDIT_Time, music->m_time);
		int time=SdpSetStringToTime(music->m_time,1);
		if (time<0)	{time=0;}
		m_time_slider.SetPos(time);
		//	DDX_Control(&dx, IDC_SLIDER_Time, m_time_slider);
		DDX_Text(&dx, IDC_EDIT_MusicInfo, music->m_music_info);
	}
	
//	CDialogBar::OnTimer(nIDEvent);
}

void CDlgBar::f_set_cb_string(CSdpProfile *profile,CMusic *music,CTime *time,CString &res)
{
	CString str="";
	CStringArray *items=&profile->opt_cb_items;
	for(int i=0, max=items->GetSize() ; i<max ; i++)
	{
		switch (*items->GetAt(i))
		{
		default:
		case '0': // end
			i=max;
			break;
		case '1': // title
			if (str!=""){str+=",";}
			str+=music->m_title;
			break;
		case '2': // date
			if (str!=""){str+=",";}
			str+=time->Format("%Y/%m/%d");
			break;
		case '3': // time
			if (str!=""){str+=",";}
			str+=time->Format("%H:%M:%S");
			break;
		case '4': // user text
			if (str!=""){str+=",";}
			str+=profile->opt_cb_usertext;
			break;
		}
	}
	if (res!=""){res+="\n";}
	res+=str;
	return;
}

void CDlgBar::OnTipTimer() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	KillTimer(3);
	m_timer_mode=-1;
	CSdpProfile *profile=&(((CSdPlayerApp*)AfxGetApp())->m_profile);

//	music->m_play_on=0;
//	music->OnPlay();
	music->OnFadeOut(profile->opt_fadeout_len*10);

	// m_timer_state=0 : waiting patter
	// m_timer_state=1 : start patter
	// m_timer_state=2 : waiting singing
	// m_timer_state=3 : start singing
	// m_timer_state=4 : waiting rest time
	// m_timer_state=5 : start rest time

	int ncheck=0;
	while(ncheck<6)
	{
		if (++m_timer_state>5)	{m_timer_state=0;}

		// m_timer_state=0 : waiting patter
		if (m_timer_state==0)
		{
			if (music_data[0] && music_data[0]->m_stat_load)
			{
				ncheck=6;
			}
			else
			{
				m_timer_state+=2;
				ncheck+=2;
			}
		}
		
		// m_timer_state=1 : start patter
		if (m_timer_state==1)
		{
			if (music_data[0] && music_data[0]->m_stat_load)
			{
				m_timer_mode=0;
				f_MusicSelect(0);
				music->m_stat_repeat=music->m_auto_repeat;
				music->m_play_on=1;
				UpdateData(FALSE);
				OnRewind();
				m_timer_count=0;
				SdpSetTimeToString(m_timer_count,m_tip_timer,1);
				SetTimer(3,1000,NULL);
				m_pwndTimeDisp->SetWindowText("PATTER");
				list_data[0]->f_set_check(music->m_data_num-1);
				ncheck=6;
			}
			else
			{
				m_timer_state+=1;
				ncheck+=1;
			}
		}
		
		// m_timer_state=2 : waiting singing
		if (m_timer_state==2)
		{
			if (music_data[1] && music_data[1]->m_stat_load)
			{
				ncheck=6;
			}
			else
			{
				m_timer_state+=2;
				ncheck+=2;
			}
		}
		
		// m_timer_state=3 : start singing
		if (m_timer_state==3)
		{
			if (music_data[1] && music_data[1]->m_stat_load)
			{
				m_timer_mode=1;
				f_MusicSelect(1);
				music->m_stat_repeat=0;
				music->m_play_on=1;
				UpdateData(FALSE);
				OnRewind();
				m_timer_count=0;
				SdpSetTimeToString(m_timer_count,m_tip_timer,1);
				SetTimer(3,1000,NULL);
				m_pwndTimeDisp->SetWindowText("SINGING");
				list_data[1]->f_set_check(music->m_data_num-1);
				ncheck=6;
			}
			else
			{
				m_timer_state+=1;
				ncheck+=1;
			}
		}
		
		// m_timer_state=4 : waiting rest time
		if (m_timer_state==4)
		{
			if (m_rest_time>0)
			{
				ncheck=6;
			}
			else
			{
				m_timer_state+=2;
				ncheck+=2;
			}
		}
		
		// m_timer_state=5 : start rest time
		if (m_timer_state==5)
		{
			if (m_rest_time>0)
			{
				m_timer_mode=2;
				f_MusicSelect(0);
				m_timer_count=0;
				SdpSetTimeToString(m_timer_count,m_tip_timer,1);
				SetTimer(3,1000,NULL);
				m_pwndTimeDisp->SetWindowText("REST");
				ncheck=6;
			}
			else
			{
				m_timer_state+=1;
				ncheck+=1;
			}
		}
	}

	m_pwndTimeDisp->m_back_mode=1;
	m_pwndTimeDisp->m_back_color=RGB(192,192,192); //gray
	m_pwndTimeDisp->m_time=m_tip_timer;
	m_pwndTimeDisp->f_DispTimer();

	// copy music data to clipboard
	CTime theTime = CTime::GetCurrentTime();
	CString str;
	if (music_data[0] && music_data[0]->m_stat_load && (profile->opt_cb_sel+1)&1)
	{
		f_set_cb_string(profile,music_data[0],&theTime,str);
	}
	if (music_data[1] && music_data[1]->m_stat_load && (profile->opt_cb_sel+1)&2)
	{
		f_set_cb_string(profile,music_data[1],&theTime,str);
	}
	DWORD len=str.GetLength()+1;
	HANDLE hData=GlobalAlloc(GMEM_FIXED,len);
	sprintf((char*)hData,"%s",str);
	// Create an OLE data source on the heap
	COleDataSource* pData = new COleDataSource;
	pData->CacheGlobalData( CF_TEXT, hData );
	pData->SetClipboard();

	UpdateData(FALSE);
}

void CDlgBar::OnCueSheet()
{
	int res=0;
	CString str;

	if (music_data[1]->m_cue_file=="")
	{
		res=ERROR_FILE_NOT_FOUND;
	}
	else if (music_data[1]->m_cue_prog=="")
	{
		res=(int)ShellExecute(NULL,"open",music_data[1]->m_cue_file,NULL,music_data[1]->m_base_dir,SW_SHOWNORMAL);
	}
	else
	{
		res=(int)ShellExecute(NULL,"open",music_data[1]->m_cue_prog,"\""+music_data[1]->m_cue_file+"\"",music_data[1]->m_base_dir,SW_SHOWNORMAL);
	}

	if (res>32)
	{
		return;
	}

	switch (res)
	{
	case 0:
		str="out of memory";
		break;
	case ERROR_FILE_NOT_FOUND:
		str="file not found";
		break;
	case ERROR_PATH_NOT_FOUND:
		str="pass not found";
		break;
	case ERROR_BAD_FORMAT:
		str=".exe file is invalid";
		break;
	case SE_ERR_ACCESSDENIED:
		str="access denied";
		break;
	case SE_ERR_ASSOCINCOMPLETE:
		str="association incomplete";
		break;
	case SE_ERR_NOASSOC:
		str="no application associated";
		break;
	default:
		str.Format("file open error (%d)",res);
		break;
	}

	str="*** "+str+" ***\n\n file: "+music_data[1]->m_cue_file+"\n prog: "+music_data[1]->m_cue_prog+"\n dir: "+music_data[1]->m_base_dir;
	AfxMessageBox(str);
}

void CDlgBar::OnUpdateUnLoad(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (!music->m_play_on && music->m_stat_load)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateLoad(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (!music->m_play_on)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateRewind(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music->m_stat_ready)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdatePlay(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music->m_stat_ready)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateRepeat(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music->m_stat_ready)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateReset(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music->m_stat_ready)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdatePatter(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (!music->m_play_on)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateSinging(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (!music->m_play_on)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateTipTimer(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music_data[0] && music_data[0]->m_stat_load)
	{
		pCmdUI->Enable(TRUE);
	}
	else if (music_data[1] && music_data[1]->m_stat_load)
	{
		pCmdUI->Enable(TRUE);
	}
	else if (m_rest_time>0)
	{
		pCmdUI->Enable(TRUE);
	}
	else if (m_timer_state==1||m_timer_state==3||m_timer_state==5)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CDlgBar::OnUpdateTimerMode(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください

	switch (pCmdUI->m_nID)
	{
	case IDC_RADIO_TipPatter:
		if (m_timer_state==0 || m_timer_state==1)
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
		break;
	case IDC_RADIO_TipSinging:
		if (m_timer_state==2 || m_timer_state==3)
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
		break;
	case IDC_RADIO_TipRest:
		if (m_timer_state==4 || m_timer_state==5)
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
		break;
	default:
		pCmdUI->Enable(TRUE);
		break;
	}
}

void CDlgBar::OnUpdateSlider(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください

	switch (pCmdUI->m_nID)
	{
	case IDC_SLIDER_Speed:
	case IDC_EDIT_Speed:
//		if (music->m_data_type==0 || music->m_data_type==2)
//		{
			pCmdUI->Enable(TRUE);
//		}
//		else
//		{
//			pCmdUI->Enable(FALSE);
//		}
		break;
	case IDC_SLIDER_Pitch:
	case IDC_EDIT_Pitch:
		if (music->m_dsp_on)
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
		break;
	case IDC_SLIDER_Tempo:
	case IDC_EDIT_Tempo:
		if (music->m_dsp_on)
		{
			pCmdUI->Enable(TRUE);
		}
		else
		{
			pCmdUI->Enable(FALSE);
		}
		break;
	default:
		pCmdUI->Enable(TRUE);
		break;
	}
}

void CDlgBar::OnUpdateCueSheet(CCmdUI* pCmdUI)
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (music_data[1] && music_data[1]->m_stat_load  && (music_data[1]->m_cue_file!=""))
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

