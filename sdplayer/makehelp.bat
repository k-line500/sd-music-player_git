@echo off
REM -- 最初に Microsoft Visual C++ で作成した resource.h からマップ ファイルを作成します
echo // MAKEHELP.BAT generated Help Map file.  Used by SDPLAYER.HPJ. >"hlp\SdPlayer.hm"
echo. >>"hlp\SdPlayer.hm"
echo // コマンド (ID_* , IDM_*) >>"hlp\SdPlayer.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\SdPlayer.hm"
echo. >>"hlp\SdPlayer.hm"
echo // プロンプト (IDP_*) >>"hlp\SdPlayer.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\SdPlayer.hm"
echo. >>"hlp\SdPlayer.hm"
echo // リソース (IDR_*) >>"hlp\SdPlayer.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\SdPlayer.hm"
echo. >>"hlp\SdPlayer.hm"
echo // ダイアログ (IDD_*) >>"hlp\SdPlayer.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\SdPlayer.hm"
echo. >>"hlp\SdPlayer.hm"
echo // フレーム コントロール (IDW_*) >>"hlp\SdPlayer.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\SdPlayer.hm"
REM -- プロジェクト SDPLAYER のヘルプを作成


echo Win32 ﾍﾙﾌﾟ ﾌｧｲﾙのﾋﾞﾙﾄﾞ中
start /wait hcw /C /E /M "hlp\SdPlayer.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\SdPlayer.hlp" goto :Error
if not exist "hlp\SdPlayer.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\SdPlayer.hlp" Debug
if exist Debug\nul copy "hlp\SdPlayer.cnt" Debug
if exist Release\nul copy "hlp\SdPlayer.hlp" Release
if exist Release\nul copy "hlp\SdPlayer.cnt" Release
echo.
goto :done

:Error
echo hlp\SdPlayer.hpj(1) : error: ヘルプファイルを作成する時に問題が発生しました。

:done
echo.
