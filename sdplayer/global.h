
extern long SdpSetTimeToString(double time, CString& str,int flag);
extern long SdpSetStringToTime(LPCTSTR str,int flag);
extern LPSTR SdpAllocBuffer(DWORD size);
extern LPSTR SdpFreeBuffer(LPSTR buf);
extern BOOL SdpPathAbsToRel(LPCTSTR path,LPCTSTR ref0,CString& res);
extern BOOL SdpPathAbsToRel0(LPCTSTR path,LPCTSTR ref0,CString& res);
extern BOOL SdpPathIsRelative(LPCTSTR path);
extern BOOL SdpPathRelToAbs(LPCTSTR path,LPCTSTR ref0,CString& res);
extern BOOL SdpPathRelToAbs0(LPCTSTR path,LPCTSTR ref0,CString& res);
