// SdPlayer.cpp : アプリケーション用クラスの機能定義を行います。
//

#include "stdafx.h"
#include "SdPlayer.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "SdPlayerDoc.h"
#include "SdPlayerView.h"

#include <afxadv.h>
#include <Afxpriv.h> // for WM_SETMESSAGESTRING

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerApp

BEGIN_MESSAGE_MAP(CSdPlayerApp, CWinApp)
	//{{AFX_MSG_MAP(CSdPlayerApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	//}}AFX_MSG_MAP
	// 標準のファイル基本ドキュメント コマンド
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
//	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// 標準の印刷セットアップ コマンド
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerApp クラスの構築

CSdPlayerApp::CSdPlayerApp()
{
	// TODO: この位置に構築用コードを追加してください。
	// ここに InitInstance 中の重要な初期化処理をすべて記述してください。
}

/////////////////////////////////////////////////////////////////////////////
// 唯一の CSdPlayerApp オブジェクト

CSdPlayerApp theApp;

// この ID はこのアプリケーションが統計的にユニークになるように生成されました。
// もし、特別な ID を指定したいならば、変更してもかまいません。

// {6FCFAB92-A6E5-11D5-BC30-009027BFCD6A}
static const CLSID clsid =
{ 0x6fcfab92, 0xa6e5, 0x11d5, { 0xbc, 0x30, 0x0, 0x90, 0x27, 0xbf, 0xcd, 0x6a } };

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerApp クラスの初期化

BOOL CSdPlayerApp::InitInstance()
{
	// OLE ライブラリの初期化
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// 標準的な初期化処理
	// もしこれらの機能を使用せず、実行ファイルのサイズを小さく
	// したければ以下の特定の初期化ルーチンの中から不必要なもの
	// を削除してください。

#ifdef _AFXDLL
	Enable3dControls();		// 共有 DLL の中で MFC を使用する場合にはここを呼び出してください。
#else
	Enable3dControlsStatic();	// MFC と静的にリンクしている場合にはここを呼び出してください。
#endif

	// 設定が保存される下のレジストリ キーを変更します。
	// TODO: この文字列を、会社名または所属など適切なものに
	// 変更してください。
	SetRegistryKey(_T("CSC"));

	LoadStdProfileSettings();  // 標準の INI ファイルのオプションをローﾄﾞします (MRU を含む)

	m_profile.f_read_profile();

	// アプリケーション用のドキュメント テンプレートを登録します。ドキュメント テンプレート
	//  はドキュメント、フレーム ウィンドウとビューを結合するために機能します。

	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(
		IDR_SDPLAYTYPE,
		RUNTIME_CLASS(CSdPlayerDoc),
		RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
		RUNTIME_CLASS(CSdPlayerView));
	AddDocTemplate(pDocTemplate);

	// ドキュメント テンプレートに COleTemplateServer を接続します。
	// COleTemplateServer はドキュメント テンプレートで指定された
	// 情報を使って OLE コンテナに要求する代わりに新規ドキュメント
	// を作成します。
	m_server.ConnectTemplate(clsid, pDocTemplate, FALSE);

	// 実行するとすべての OLE サーバー ファクトリを登録します。
	// 他のアプリケーションからオブジェクトを作るために OLE ライブラリを使用可能にします。
	COleTemplateServer::RegisterAll();
		// メモ: MDI アプリケーションはコマンドラインで /Embedding か /Automation が
		//       指定されていなくてもすべてのサーバー オブジェクトを登録します。

	// メイン MDI フレーム ウィンドウを作成
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;
	m_pMainWnd = pMainFrame;

	// ドラッグ/ドロップ のオープンを許可します
	m_pMainWnd->DragAcceptFiles();

	// DDE Execute open を使用可能にします。
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// DDE、file open など標準のシェル コマンドのコマンドラインを解析します。
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// OLE サーバーとして起動されているか確認します。
// /*
	if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
	{
		// アプリケーションが /Embedding か /Automation で実行されている時には
		// メイン ウィンドウ は表示できません。
		return TRUE;
	}

	// システム レジストリが壊れていてサーバー アプリケーションがスタンド アロンで
	// 起動された時には、システム レジストリを更新してください。
	m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();
// */

	if (cmdInfo.m_nShellCommand==CCommandLineInfo::FileNew && m_pRecentFileList->GetSize()>0)
	{
		if ((cmdInfo.m_strFileName=(*m_pRecentFileList)[0])!="")
		{
			CFileFind  find;
			if (!find.FindFile(cmdInfo.m_strFileName))
			{
				CString str;
				str=cmdInfo.m_strFileName+" dose not exist";
				AfxMessageBox(str);
				cmdInfo.m_strFileName="";
			}
			else
			{
				cmdInfo.m_nShellCommand=CCommandLineInfo::FileOpen;
			}
		}
	}
	
	// コマンドラインでディスパッチ コマンドを指定します。
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// メイン ウィンドウが初期化されたので、表示と更新を行います。
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();
	//
	pMainFrame->PostMessage(WM_SETMESSAGESTRING,AFX_IDS_IDLEMESSAGE);
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// アプリケーションのバージョン情報で使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard 仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	//{{AFX_MSG(CAboutDlg)
		// メッセージ ハンドラはありません。
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// メッセージ ハンドラはありません。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// ダイアログを実行するためのアプリケーション コマンド
void CSdPlayerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerApp メッセージ ハンドラ

/*
void CSdPlayerApp::f_SetStatusText(int no, const char* text)
{
	CMainFrame *wnd;
	if (wnd=(CMainFrame*)AfxGetMainWnd())
	{
		wnd->f_SetStatusText(no,text);
	}
}
*/

//+++++ 03.02.28 kawamura add start
// [開く]処理のカスタマイズ
void CSdPlayerApp::OnFileOpen() 
{
	CString open_file;

	CFileDialog open_dlg(TRUE, "", "",
		OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
		"SdPlayer Binary Database (*.sdm)|*.sdm|SdPlayer Text Database (*.csv)|*.csv|すべてのファイル (*.*)|*.*||",
		NULL);

	if (open_dlg.DoModal() == IDOK)
	{
		open_file = open_dlg.GetPathName();
		
		this->OpenDocumentFile(open_file);
	}

	return;
}
//+++++ 03.02.28 kawamura add end

int CSdPlayerApp::ExitInstance() 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください

	m_profile.f_write_profile();

	return CWinApp::ExitInstance();
}
