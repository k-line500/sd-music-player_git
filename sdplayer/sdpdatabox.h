#if !defined(AFX_SDPDATABOX_H__E9C75641_1954_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPDATABOX_H__E9C75641_1954_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SdpDataBox.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSdpDataBox ダイアログ

class CSdpDataBox : public CDialog
{
// コンストラクション
public:
	CSdpDataBox(CWnd* pParent = NULL);   // 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CSdpDataBox)
	enum { IDD = IDD_DATABOX };
	CSpinButtonCtrl	m_repmix_updown;
	CSpinButtonCtrl	m_repboth_updown;
	CSlider	m_repboth_slider;
	CSpinButtonCtrl	m_tempo_org_updown;
	CSpinButtonCtrl	m_waittime_updown;
	CSpinButtonCtrl	m_beatsperrep_updown;
	CSpinButtonCtrl	m_repend_updown;
	CSlider	m_repend_slider;
	CSpinButtonCtrl	m_repstart_updown;
	CSlider	m_repstart_slider;
	CSlider	m_time_slider;
	CSlider	m_speed_slider;
	CSlider	m_pitch_slider;
	CSlider	m_tempo_slider;
	CString	m_tempo_in_file;
	BOOL	m_relpath;
	int		m_rep_mix;
	//}}AFX_DATA

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSdpDataBox)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	class CMusic *music;
	long m_repeat_start;
	long m_repeat_end;

protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CSdpDataBox)
	afx_msg void OnAdd();
	afx_msg void OnLoad();
	afx_msg void OnPlay();
	afx_msg void OnRewind();
	afx_msg void OnRepeat();
	afx_msg void OnRepCheck();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSetRepStart();
	afx_msg void OnDeltaposSPINRepStart(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTestRepStart();
	afx_msg void OnTestRepStart2();
	afx_msg void OnSetRepEnd();
	afx_msg void OnDeltaposSPINRepEnd(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTestRepEnd();
	afx_msg void OnTestLoop();
	afx_msg void OnSetBeatsPerRep();
	afx_msg void OnBUTTONFile();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTestRepEnd2();
	afx_msg void OnRADIOWave();
	afx_msg void OnRadioMidi();
	afx_msg void OnRadioMpeg();
	afx_msg void OnSetOrigBPM();
	afx_msg void OnSetRepLength();
	afx_msg void OnChangeEDITTempoOrg();
	afx_msg void OnDeltaposSPINTempOrg(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSPINRepBoth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCHECKRelPath();
	afx_msg void OnRepClear();
	afx_msg void OnMore();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPDATABOX_H__E9C75641_1954_11D5_BC30_009027BFCD6A__INCLUDED_)
