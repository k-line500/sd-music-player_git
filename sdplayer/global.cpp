#include "stdafx.h"
#include "Global.h"
#include "shlwapi.h"

long SdpSetStringToTime(LPCTSTR str,int flag)
{
	long min,sec,msec;

	if (sscanf(str,"%d:%d.%d",&min,&sec,&msec)==3)
	{
		;
	}
	else if (sscanf(str,"%d:%d",&min,&sec,&msec)==2)
	{
		msec=0;
	}
	else
	{
		return 0;
	}

	int sign=1;
	if (min==0)
	{
		for ( ; *str==' ' ; str++);
		if (*str=='-')
		{
			sign=-1;
		}
	}
	else if (min<0)
	{
		sign=-1;
	}

	switch (flag)
	{
		case 1:
			if (sign<0)
			{
				if (msec>0)		{sec++;}
				if (sec==60)	{min--;sec=0;}
				return min*60-sec;
			}
			else
			{
				return min*60+sec;
			}
			break;
		case 2:
			if (sign<0)
			{
				return min*60000-sec*1000-msec;
			}
			else
			{
				return min*60000+sec*1000+msec;
			}
			break;
		default:
			break;
	}

	return 0;
}

long SdpSetTimeToString(double time, CString& str,int flag)
{
	long min,sec,msec;

	if (time<0.0)
	{
		time+=-0.0001;
		time=-time;
		sec=(DWORD)time;
		msec=(DWORD)((time-sec)*1000);
		min=sec/60;
		sec%=60;
		time=-time;
	}
	else
	{
		time+=0.0001;
		sec=(DWORD)time;
		msec=(DWORD)((time-sec)*1000);
		min=sec/60;
		sec%=60;
	}
	
	switch (flag)
	{
		case 1:
			if (time<0.0)
			{
				if (msec>0)		{sec++;}
				if (sec==60)	{min++;sec=0;}
				str.Format("-%02d:%02d",min%100,sec);
				return -(min*60+sec);
			}
			else
			{
				str.Format("%02d:%02d",min%100,sec);
				return min*60+sec;
			}
			break;
		case 2:
			if (time<0.0)
			{
				str.Format("-%02d:%02d.%03d",min%100,sec,msec);
				return -(min*60000+sec*1000+msec);
			}
			else
			{
				str.Format("%02d:%02d.%03d",min%100,sec,msec);
				return min*60000+sec*1000+msec;
			}
			break;
		default:
			break;
	}

	return 0;
}

BOOL SdpPathAbsToRel(LPCTSTR path,LPCTSTR ref0,CString& res)
{
	if (!SdpPathAbsToRel0(path,ref0,res))
	{
		CString str;
		str.Format("cannot convert to relative path\n  %s\n  %s",path,ref0);
		AfxMessageBox(str);
		return FALSE;
	}
	return TRUE;
}

BOOL SdpPathAbsToRel0(LPCTSTR path,LPCTSTR ref0,CString& res)
{
	char buf[_MAX_PATH];
	char ref1[_MAX_PATH],*ref=ref1;
	int n;

	if (path==NULL)
	{
		return FALSE;
	}
	if (PathIsRelative(path))
	{
		return TRUE;
	}
	if (ref0==NULL)
	{
		return FALSE;
	}
	if (PathIsRelative(ref0))
	{
		return FALSE;
	}

	strcpy(ref1,ref0);
	n=strlen(ref1);
	if ( n>0 && ref1[n-1]!='\\')
	{
		ref1[n]='\\';
		ref1[n+1]='\0';
	}

	if (!PathIsSameRoot(path,ref))
	{
		res=path;
		return FALSE;
	}

	if (strlen(path)>=2 && path[1]==':')
	{
		path+=2;
		ref+=2;
	}

	n=PathCommonPrefix(path,ref,buf);
	res=path+n+1;
	ref+=n+1;
	while (ref && *ref)
	{
		res="..\\"+res;
		ref=PathFindNextComponent(ref);
	}
	return TRUE;
}

BOOL SdpPathIsRelative(LPCTSTR path)
{
	return PathIsRelative(path);
}

BOOL SdpPathRelToAbs(LPCTSTR path,LPCTSTR ref0,CString& res)
{
	if (!SdpPathRelToAbs0(path,ref0,res))
	{
		CString str;
		str.Format("cannot convert to absolute path\n  %s\n  %s",path,ref0);
		AfxMessageBox(str);
		return FALSE;
	}
	return TRUE;
}

BOOL SdpPathRelToAbs0(LPCTSTR path,LPCTSTR ref0,CString& res)
{
	char buf[_MAX_PATH];
	char ref1[_MAX_PATH],*ref=ref1;

	if (path==NULL)
	{
		return FALSE;
	}
	if (!PathIsRelative(path))
	{
		return TRUE;
	}
	if (ref0==NULL)
	{
		return FALSE;
	}
	if (PathIsRelative(ref0))
	{
		return FALSE;
	}

	strcpy(ref1,ref0);
	
	if (!PathAppend(ref1,path))
	{
		return FALSE;
	}
	if (!PathCanonicalize(buf,ref1))
	{
		return FALSE;
	}
	res=buf;
	return TRUE;
}
