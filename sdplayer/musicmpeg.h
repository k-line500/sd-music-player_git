// MusicMPEG.h: CMusicMPEG クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUSICMPEG_H__F556CDC1_FABC_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_MUSICMPEG_H__F556CDC1_FABC_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MusicWave.h"
#include "lib\mp3dec.h"

class CMusicMPEG : public CMusicWave  
{
public:
	CMusicMPEG();
	virtual ~CMusicMPEG();

protected:
	DWORD m_data_size;
	DWORD m_data_pos;
	long m_next_frame;
	DWORD m_paddings;
	DWORD m_no_paddings;
	DWORD m_sample_per_frame;
	LPSTR m_frame_buf;
	LPSTR m_output_buf;

	MPEG_DECODE_INFO m_decode_info0;
	MPEG_DECODE_INFO m_decode_info;
	long f_GetNextFrame(LPSTR frame_buf);
	int f_Rewind();

// オーバーライド
	virtual LPCSTR f_Wave_OpenFile() ;			// MPEG ファイルオープン
	virtual void f_Wave_CloseFile();			// MPEG ファイルクローズ
	virtual DWORD f_Wave_ReadData(LPSTR buf, DWORD from, DWORD len);
	virtual int f_Wave_Rewind() ;
	virtual void f_SetMusicInfo();
};

#endif // !defined(AFX_MUSICMPEG_H__F556CDC1_FABC_11D5_BC30_009027BFCD6A__INCLUDED_)
