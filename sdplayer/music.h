#if !defined(AFX_MUSIC_H__CDF70E05_B419_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_MUSIC_H__CDF70E05_B419_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Music.h : ヘッダー ファイル
//

#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <math.h>

#include <windows.h>
#include <mmsystem.h>

/////////////////////////////////////////////////////////////////////////////
// CMusic ウィンドウ

class CMusic : public CWnd
{
// コンストラクション
public:
	CMusic();
	virtual ~CMusic();

// アトリビュート
protected:
	int m_object_type;
	DWORD m_header_size; // sec (wave) or kb (midi)
	int m_header_num;

// オペレーション
public:
	int f_GetObjectType();
	static CMusic *f_ObjectTypeChange(CMusic *music);
	int f_GetFileType();

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。

	//{{AFX_VIRTUAL(CMusic)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	// music database
	CString m_base_dir;		// directory in which database file is exist
	CString m_title;		// 0
	CString m_label;		// 1
	CString m_label_no;		// 2
	CString m_category;		// 3
	BOOL m_auto_repeat;		// 4
	CString	m_rep_start;	// 5
	CString	m_rep_end;		// 6
	int m_wait_time;		// 7
	double m_tempo_bpm;		// 8
	int m_speed;			// 9   delta speed
	int m_pitch;			// 10  delta pitch
	int m_tempo;			// 11  delta tempo
	int m_data_type;		// 12
	CString m_filename;		// 13  <-- 11 ver.0->1
//	double m_pitch_oct;		// 14
	double m_rep_mix;		// 14
	double m_tempo_org;		// 15
	int m_beats_per_rep;	// 16
	int m_data_num;			// 17
	CString m_flags;		// 18  frags
	CString m_cue_file;		// 19  cue sheet  file;prog;
	CString m_cue_prog;		// 19  cue sheet  file;prog;

	// database access function
	void f_InitData(void);
	void f_SetData(CStringArray *sa);
	void f_GetData(CStringArray *sa);
	void f_SetSpeedData(int speed);
	void f_SetPitchData(int pitch);
	void f_SetTempoData(int tempo);

	// buffer allocation

	// status display functon
	void f_SetStatusText(void);
	void f_SetStatusText(int no,const char *text);
	CString m_status_text0;
	CString m_status_text1;

	// status flag
	BOOL m_stat_load;
	BOOL m_stat_ready;
	BOOL m_stat_play;
	BOOL m_stat_playing;
	BOOL m_stat_repeat;
	BOOL m_stat_preparing;

	BOOL m_dsp_on;

	// dialog data
	CWnd *m_dialog;
	CString	m_time;
	CString	m_music_len;
	CString m_music_name;
	CString m_music_info;
	CString m_output_device;
	CString m_output_device_info;
	CString m_speed_control;
	CString m_pitch_control;
	CString m_tempo_control;
	BOOL m_play_on;
	double m_tempo_bpm0;
private:
	void f_SetDialogData();

public:
	// player control
	void OnLoad();
	void OnUnLoad();
	void OnPlay();
	void OnRewind();
	void OnFadeOut(int len);
	int m_wait;
	int m_nbuf;
	int m_fadeout;
	int m_fadeout_len; // 100ms unit
	DWORD  m_fadeout_vol;

	// music operation
	virtual int f_UnLoad(void)=0;
	virtual int f_Load(void)=0;
	virtual int f_Close(void)=0;
	virtual int f_Open(void)=0;
	virtual int f_Start(void)=0;
	virtual int f_Stop(void)=0;
	virtual int f_Reset(void)=0;
	virtual double f_GetTime(void)=0;
	virtual double f_GetLength(void)=0;
	virtual int f_SetStartTime(double start)=0;
	virtual int f_SetEndTime(double end)=0;
	int f_SetRepTime(void);
	virtual int f_SetRepTime(double start,double end)=0;
	virtual int f_ChangeSpeed()=0;
	virtual int f_ChangeTempo()=0;
	virtual int f_ChangePitch()=0;
	virtual void f_SetTempo(void);
	virtual void f_SetPitch();
	virtual void f_SetSpeed();
	virtual double f_GetTempoInFile();
	virtual void f_Activate();
	virtual void f_InActivate();
	virtual void f_OutProcDone(WPARAM wParam, LPARAM lParam)=0;
	virtual int f_GetVolume(DWORD *vol)=0;
	virtual int f_SetVolume(DWORD vol)=0;
protected:
	virtual void f_SetMusicInfo()=0;

	// inside music data
	double m_length;
	double m_start;
	double m_end;
	double m_return_mark;
	double m_repeat_mark;
	double m_paused_time;
	double m_speed_change;
	double m_pitch_change;
	double m_tempo_change;
//	double m_time_per_beat;
	DWORD  m_open_flag;

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CMusic)
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg LRESULT OnWaveOutDone(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMidiOutDone(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
	// 生成された OLE ディスパッチ マップ関数
	//{{AFX_DISPATCH(CMusic)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_MUSIC_H__CDF70E05_B419_11D5_BC30_009027BFCD6A__INCLUDED_)
