// DSP.cpp: CDSP クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "sdplayer.h"

#include "MusicWave.h"
#include "DSP.h"

#include <malloc.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

//#define OVL_BIT 9
#define OVL_LEN 512
#define INPUT_LEN 8000
#define MAXWIN_LEN 6000
#define STDWIN_LEN 4000
#define MINWIN_LEN 2000
#define MAX_OFFSET (999 + 124)
#define DIVIDER_BITS 15
/* Table with hierarchical offset seeking positions */

static const int s_scans[4][8]={
		{ 124, 249, 374, 499, 624, 749, 874, 999},
		{-100, -75, -50, -25,  25,  50,  75, 100},
		{ -20, -15, -10,  -5,   5,  10,  15,  20},
		{  -4,  -3,  -2,  -1,   1,   2,   3,   4}};

/*
static const int s_scans[4][8]={
		{   0, 125, 250, 375, 500, 625, 750, 875},
		{  25,  50,  75, 100, 125, 150, 175, 200},
		{   5,  10,  15,  20,  25,  30,  35,  40},
		{   1,   2,   3,   4,   5,   6,   7,   8}};
*/

CDSP::CDSP()
{
	m_inputbuf=NULL;
	m_inputbuf_len=INPUT_LEN;
	m_inputbuf_sample_num=0;

	m_outputbuf=NULL;
	m_outputbuf_len=m_inputbuf_len;
	m_outputbuf_sample_num=0;

	m_ovlbuf=NULL;
	m_ovlbuf_len=OVL_LEN;
	m_ovlbuf_sample_num=0;

	m_input_pos=0;
}

CDSP::~CDSP()
{
	f_Free();
}


int CDSP::f_Alloc(DWORD nch,DWORD bits)
{
	if (bits!=16 && bits!=8 )	return 0;
	m_nch=nch;
	m_sample_bytes=bits*nch/8;
	m_inputbuf =(LPSTR)calloc(1,m_inputbuf_len*m_sample_bytes);
	m_outputbuf=(LPSTR)calloc(1,m_outputbuf_len*m_sample_bytes);
	m_ovlbuf   =(LPSTR)calloc(1,m_ovlbuf_len*m_sample_bytes);
	if (m_inputbuf && m_outputbuf && m_ovlbuf)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void CDSP::f_Free()
{
	if (m_inputbuf)		free(m_inputbuf);
	m_inputbuf=NULL;
	if (m_outputbuf)	free(m_outputbuf);
	m_outputbuf=NULL;
	if (m_ovlbuf)		free(m_ovlbuf);
	m_ovlbuf=NULL;
}

int CDSP::f_Rewind()
{
	m_inputbuf_sample_num=0;
	m_outputbuf_sample_num=0;
	m_ovlbuf_sample_num=0;
	m_input_pos=0;
	return 1;
}

DWORD CDSP::f_ChangeTempo(LPSTR buf, DWORD from, DWORD len, double tempo, double play_rate, int mode)
{
	DWORD output_len;
	DWORD output_num;
	DWORD win_len;
	DWORD num;
	int offset;

	win_len=(DWORD)(STDWIN_LEN*play_rate);
	if (win_len>MAXWIN_LEN)	{win_len=MAXWIN_LEN;}
	if (win_len<MINWIN_LEN)	{win_len=MINWIN_LEN;}

	if ((DWORD)(from*tempo)>m_input_pos)
	{
		m_input_pos=(DWORD)(from*tempo);
	}

	output_num=0;
	do
	{
		num=m_outputbuf_sample_num;
		if (num>len)	{num=len;}
		if (num>0)
		{
			memcpy(buf+output_num * m_sample_bytes,
				m_outputbuf,num * m_sample_bytes);
			if (num<m_outputbuf_sample_num)
			{
				memcpy(m_outputbuf,
					m_outputbuf + num*m_sample_bytes,
					(m_outputbuf_sample_num - num ) * m_sample_bytes);
			}
			output_num+=num;
			len-=num;
			m_outputbuf_sample_num-=num;
		}
		if (len==0)
		{
			break;
		}

		m_outputbuf_sample_num=0;
		
		if (m_inputbuf_sample_num<win_len)
		{
			m_inputbuf_sample_num +=
				m_wave->f_Wave_ReadData(
					m_inputbuf+m_inputbuf_sample_num*m_sample_bytes,
					m_input_pos + m_inputbuf_sample_num,
					m_inputbuf_len - m_inputbuf_sample_num);
			if (m_inputbuf_sample_num<win_len)
			{
				win_len=m_inputbuf_sample_num;
			}
		}

		if (m_inputbuf_sample_num==0)
		{
			break;
		}
		else if (m_inputbuf_sample_num<m_ovlbuf_sample_num)
		{
			m_ovlbuf_sample_num=m_inputbuf_sample_num;
		}

		LPSTR ovlb=m_ovlbuf;
		LPSTR inputb=m_inputbuf;
		LPSTR outputb=m_outputbuf;
		offset=0;

		if (mode>0)
		{
			if (m_sample_bytes/m_nch==2) // 16bit sample 
			{
				if (mode>1 && m_inputbuf_sample_num>m_ovlbuf_sample_num+MAX_OFFSET)
				{
					offset=f_SeekOffset16(m_ovlbuf,m_inputbuf,m_ovlbuf_sample_num);
//					CString str;
//					str.Format("off=%d",offset);
//					m_wave->f_SetStatusText(1,str);
					inputb+=offset*m_sample_bytes;
				}
				int ovl,input,output;
				for (DWORD i=0 ; i<m_ovlbuf_sample_num*m_nch ; i++)
				{
					int c0=i/m_nch;
					int c1=m_ovlbuf_sample_num-c0;
					ovl=*(short*)ovlb;
					input=*(short*)inputb;
//					output = (ovl*c1+input*c0) >> OVL_BIT ;
					output = (ovl*c1+input*c0) / m_ovlbuf_sample_num ;
					*((short*)outputb) = (short)output ;
					ovlb+=2;
					inputb+=2;
					outputb+=2;
				}
				m_outputbuf_sample_num+=m_ovlbuf_sample_num;
			}
			else if (m_sample_bytes/m_nch==1) // 8bit sample
			{
				if (mode>1 && m_inputbuf_sample_num>m_ovlbuf_sample_num+MAX_OFFSET)
				{
					offset=f_SeekOffset8(m_ovlbuf,m_inputbuf,m_ovlbuf_sample_num);
					inputb+=offset*m_sample_bytes;
				}
				int ovl,input,output;
				for (DWORD i=0 ; i<m_ovlbuf_sample_num*m_nch ; i++)
				{
					int c0=i/m_nch;
					int c1=m_ovlbuf_sample_num-c0;
					ovl=*(char*)ovlb;
					input=*(char*)inputb;
//					output = (ovl*c1+input*c0) >> OVL_BIT ;
					output = (ovl*c1+input*c0) / m_ovlbuf_sample_num ;
					*((char*)outputb) = (char)output ;
					ovlb+=1;
					inputb+=1;
					outputb+=1;
				}
				m_outputbuf_sample_num+=m_ovlbuf_sample_num;
			}
		}

		num=win_len - m_outputbuf_sample_num - offset;
		if (num>(DWORD)OVL_LEN)
		{
			num-=OVL_LEN;
			memcpy(outputb,inputb,num*m_sample_bytes);
			inputb+=num*m_sample_bytes;
			m_outputbuf_sample_num+=num;

			m_ovlbuf_sample_num=OVL_LEN;
		}
		else
		{
			m_ovlbuf_sample_num=num;
		}
	
		memcpy(m_ovlbuf,inputb,m_ovlbuf_sample_num*m_sample_bytes);

		output_len=(DWORD)(m_outputbuf_sample_num*tempo);
		
		if (output_len<m_inputbuf_sample_num)
		{
			m_inputbuf_sample_num=m_inputbuf_sample_num-output_len;
			memcpy(m_inputbuf,
				m_inputbuf+output_len*m_sample_bytes,
				m_inputbuf_sample_num*m_sample_bytes);
		}
		else
		{
			m_inputbuf_sample_num=0;
		}
		m_input_pos+=output_len;
	}while(1);

	return output_num;
}

int CDSP::f_SeekOffset16(LPSTR ovlbuf, LPSTR inputbuf, DWORD ovl_num)
{
	if (ovl_num<OVL_LEN)
	{
		return 0;
	}
	short *ovlptr = (short*)ovlbuf;
	short *inputptr = (short*)inputbuf;

	DWORD bestcorr = 0xFFFFFFFF;
	int bestoffs = s_scans[0][0]*m_nch;
	int corr_offset = 0;
	int temp_offset = 0;
	for (int scancount = 0;scancount < 4; scancount ++)
	{
		for (int offsetcnt = 0 ; offsetcnt<8 ; offsetcnt++)
		{
			temp_offset = corr_offset + s_scans[scancount][offsetcnt]*m_nch;

			DWORD corr = 0;
			for (DWORD i=0 ; i<ovl_num*m_nch ; i++)
			{
				int c0 = i/m_nch;
				c0=c0 * (ovl_num - c0);
				int c1= (inputptr[i+temp_offset] - ovlptr[i])*c0 >> DIVIDER_BITS;
				corr += c1*c1 >> 8;
			}

			if (corr < bestcorr)
			{
				bestoffs = temp_offset;
				bestcorr = corr;
			}
			else if (corr == bestcorr && temp_offset<bestoffs)
			{
				bestoffs = temp_offset;
				bestcorr = corr;
			}
		}
		corr_offset = bestoffs;
	}

	return bestoffs/m_nch;
}

int CDSP::f_SeekOffset8(LPSTR ovlbuf, LPSTR inputbuf, DWORD ovl_num)
{
	if (ovl_num<OVL_LEN)
	{
		return 0;
	}
	char *ovlptr = (char*)ovlbuf;
	char *inputptr = (char*)inputbuf;

	DWORD bestcorr = 0xFFFFFFFF;
	int bestoffs = s_scans[0][0]*m_nch;
	int corr_offset = 0;
	int temp_offset = 0;
	for (int scancount = 0;scancount < 4; scancount ++)
	{
		for (int offsetcnt = 0 ; offsetcnt<8 ; offsetcnt++)
		{
			temp_offset = corr_offset + s_scans[scancount][offsetcnt]*m_nch;

			DWORD corr = 0;
			for (DWORD i=0 ; i<ovl_num*m_nch ; i++)
			{
				int c0 = i/m_nch;
				c0=c0 * (ovl_num - c0);
				int c1= (inputptr[i+temp_offset] - ovlptr[i])*c0 >> DIVIDER_BITS;
				corr += c1*c1 ; 
			}

			if (corr < bestcorr)
			{
				bestoffs = temp_offset;
				bestcorr = corr;
			}
			else if (corr == bestcorr && temp_offset<bestoffs)
			{
				bestoffs = temp_offset;
				bestcorr = corr;
			}
		}
		corr_offset = bestoffs;
	}

	return bestoffs/m_nch;
}
