// MusicMIDI.h: CMusicMIDI クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUSICMIDI_H__850FEA01_DF2E_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_MUSICMIDI_H__850FEA01_DF2E_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Music.h"

class CSmfTrack
{
public:
	CSmfTrack();
	virtual ~CSmfTrack();

	int   no;
	LPSTR buf;
	DWORD buf_size;
	DWORD final_tick;
	DWORD final_tick_time;

	DWORD next_tick;
	DWORD next_tick_time;
	LPSTR next_event_pos;

	int f_AllocBuffer(DWORD size) ;
	void f_FreeBuffer() ;
	LPCSTR f_GetVDWord(DWORD *dw); // get variable length data
	LPCSTR f_LoadEvent() ;	// get next event
	void f_Rewind() ;

	DWORD delta_time;
	union event_dword
	{
		DWORD dw;
		BYTE byte[4];
	} event;
	DWORD params;
	LPSTR param_pos;
	BYTE  running_status;

	CString name;
};

class CSmf
{
public:
	CSmf();
	virtual ~CSmf();

	CString name;
	struct smf_header
	{
		WORD format;
		WORD track_num;
		WORD division;
	} header;
	WORD  time_division;
	WORD  time_format;
	UINT  time_frame;
	DWORD last_tick;
	DWORD last_tick_time;
	DWORD next_tick_time;
	DWORD final_tick;
	DWORD final_tick_time;
	CSmfTrack **tracks;
	double sec_per_tick_time;
	double tempo_base;
	BOOL first_note_on;

	DWORD tempo_state;
	DWORD program_change_state[16];
	DWORD control_change_state[16][120];

	// SMF IO
	HMMIO hmmio ;
	LPCSTR f_Load(LPCSTR filename) ;	// Load SMF
	void f_UnLoad();				// UnLoad SMF
	void f_Rewind();				// rewind all tracks
	LPCSTR f_Check() ;				// SMF Format Check
	void f_GetInfo() ;
	CSmfTrack *f_GetNextEvent();
	int f_Seek(MIDIHDR *mh,DWORD seek_tick_time,int mode);
	int f_ReadEvent(MIDIHDR *mh,DWORD max_tick_time);
	int f_FindChunk(HMMIO hmmio,MMCKINFO *ckinfo);
private:
	int f_Seek1(MIDIHDR *mh,DWORD seek_tick_time);
	int f_Seek2(MIDIHDR *mh,DWORD seek_tick_time,int mode);
	int seek_status;
};

class CMusicMIDI : public CMusic  
{
public:
	CMusicMIDI();
	virtual ~CMusicMIDI();

	// for wave out API
//	int m_nmh;
	int f_setMidiHeader(MIDIHDR *mh);
private:
	CSmf *m_smf;
	HMIDISTRM m_hmo;
	MIDIHDR *m_mh;
	MMRESULT m_mmres;
	DWORD m_repeat_num;
	double m_repeat_mod;
	DWORD m_start_tick_time;
	DWORD m_stop_tick_time;
	DWORD m_rep_tick_time;
	DWORD m_ret_tick_time;
	DWORD m_seek_pos;
	int   m_seek_mode;

	BYTE m_tone_tune;
	BYTE m_fine_tune_MSB;
	BYTE m_fine_tune_LSB;


// オーバーライド
public:
	// data operation function 
	int f_UnLoad(void);
	int f_Load(void);
	int f_Close(void);
	int f_Open(void);
	int f_Start(void);
	int f_Stop(void);
	int f_Reset(void);
	double f_GetTime(void);
	double f_GetLength(void);
	int f_SetStartTime(double start);
	int f_SetEndTime(double end);
	int f_SetRepTime(double start,double end);
	int f_ChangeSpeed();
	int f_ChangeTempo();
	int f_ChangePitch();
	void f_SetSpeed();
	void f_SetPitch();
	void f_SetTempo(void);
	double f_GetTempoInFile();
	void f_OutProcDone(WPARAM wParam, LPARAM lParam);
	int f_GetVolume(DWORD *vol);
	int f_SetVolume(DWORD vol);
private:
	void f_SetMusicInfo();
};

// global call back function
void CALLBACK midiOutProc
( HMIDIOUT hwo,      
   UINT uMsg,         
   DWORD dwInstance,  
   DWORD dwParam1,    
   DWORD dwParam2
);

#endif // !defined(AFX_MUSICMIDI_H__850FEA01_DF2E_11D5_BC30_009027BFCD6A__INCLUDED_)
