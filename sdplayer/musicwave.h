// MusicWave.h: CMusicWave クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUSICWAVE_H__4D5FE3A4_BF01_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_MUSICWAVE_H__4D5FE3A4_BF01_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Music.h"

class CMusicWave : public CMusic  
{
	friend class CDSP;
public:
	CMusicWave();
	virtual ~CMusicWave();

public:
	// PCM wave format infomation
	DWORD m_sampling_rate;
	WORD  m_nch;
	WORD  m_sample_bits;
	WORD  m_sample_bytes;
	DWORD m_nsample;

	// for wave out API
//	int m_nwh;
	int f_setWaveHeader(WAVEHDR *wh);

protected:
	// data for WAVE buffer operation
	WAVEFORMATEX	wfmtex ;			// WAVE フォーマット構造体
	DWORD			m_HeaderSize ;		// WAVE ヘッダサイズ
//	DWORD			dwBufferSize ;		// WAVE バッファサイズ
//	LPVOID			pBuffer ;			// WAVE バッファポインタ
	HMMIO           hmmio ;
	DWORD           m_WaveDataSize ;

	HWAVEOUT m_hwo;
	WAVEHDR  *m_wh;
	MMRESULT m_mmres;
	DWORD m_repeat_num;
	double m_repeat_mod;
	DWORD m_next_pos;
	DWORD m_final_pos;
	DWORD m_rep_pos;
	DWORD m_ret_pos;
	long m_max_len;
	double m_play_rate;
	double m_tempo_change_rate;

	class CDSP *m_dsp;

	DWORD m_rep_mix_buf_size;
	LPSTR m_rep_mix_buf;
	DWORD m_rep_mix_len;
	DWORD m_rep_mix_pos;

	
	// WAVE buffer operation function
	virtual LPCSTR f_Wave_OpenFile() ;			// WAVE ファイルオープン
	virtual void f_Wave_CloseFile();			// WAVE ファイルクローズ
	virtual DWORD f_Wave_ReadData(LPSTR buf, DWORD from, DWORD len);
	virtual int f_Wave_Rewind() ;
	DWORD f_ReadData(LPSTR buf, DWORD from, DWORD len);
	DWORD f_Wave_MixData(LPSTR buf, DWORD from, DWORD len);

// オーバーライド
public:
	// data operation function 
	int f_UnLoad(void);
	int f_Load(void);
	int f_Close(void);
	int f_Open(void);
	int f_Start(void);
	int f_Stop(void);
	int f_Reset(void);
	double f_GetTime(void);
	double f_GetLength(void);
	int f_SetStartTime(double start);
	int f_SetEndTime(double end);
	int f_SetRepTime(double start,double end);
	int f_ChangeSpeed();
	int f_ChangePitch();
	int f_ChangeTempo();
	void f_SetTempo(void);
	void f_SetPitch();
	void f_SetSpeed();
	void f_OutProcDone(WPARAM wParam, LPARAM lParam);
	int f_GetVolume(DWORD *vol);
	int f_SetVolume(DWORD vol);
protected:
	virtual void f_SetMusicInfo();
};

// global call back function
void CALLBACK waveOutProc
( HWAVEOUT hwo,      
   UINT uMsg,         
   DWORD dwInstance,  
   DWORD dwParam1,    
   DWORD dwParam2
);

#endif // !defined(AFX_MUSICWAVE_H__4D5FE3A4_BF01_11D5_BC30_009027BFCD6A__INCLUDED_)
