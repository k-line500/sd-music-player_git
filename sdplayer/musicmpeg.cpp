// MusicMPEG.cpp: CMusicMPEG クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "sdplayer.h"
#include "MusicMPEG.h"
#include "Global.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

CMusicMPEG::CMusicMPEG()
{
	m_object_type=2;	// mpeg data
	m_frame_buf=NULL;
	m_output_buf=NULL;
}

CMusicMPEG::~CMusicMPEG()
{
	f_SetStatusText(0,"CMusicMPEG deleted");
//	Sleep(1000);
//	AfxMessageBox("CMusicMPEG deleted");
}

//////////////////////////////////////////////////////////////////////
// MPEG Audio file 読み込み
//////////////////////////////////////////////////////////////////////

LPCSTR CMusicMPEG::f_Wave_OpenFile()
{
    MMIOINFO                mmioinfo;
    MMCKINFO                ckRIFF;
    MMCKINFO                ckFMT;
    MMCKINFO                ckDATA;
	LPCSTR filename=m_filename;
	char frame_buf_0[156];
	char format_buf[32];
	DWORD frame_header;
	long frame_size;
	DWORD offset;
	char b;
    MMCKINFO ckinfo;
	char id_tag[3];
	char track_name[31];
	char artist_name[31];

    memset(&mmioinfo, 0, sizeof(MMIOINFO));
    hmmio = mmioOpen((LPSTR)filename, &mmioinfo, MMIO_READ|MMIO_ALLOCBUF);
    if (hmmio==(HMMIO)NULL)
    {
		return "file open error";
    }
	

	offset=0;

	if (3==mmioRead(hmmio,(HPSTR)&ckinfo.fccType, 3))
	{
		if ((ckinfo.fccType & 0x00FFFFFF)==mmioFOURCC('I','D','3',0))
		{
			if (1!=mmioRead(hmmio,&b,1)) // version
			{
				return "file format error";
			}
			if (1!=mmioRead(hmmio,&b,1)) // revision
			{
				return "file format error";
			}
			if (1!=mmioRead(hmmio,&b,1)) // frags
			{
				return "file format error";
			}

			ckinfo.cksize=0;
			for (int i=0 ; i<4 ; i++)
			{
				if (1!=mmioRead(hmmio,&b,1))
				{
					return "file format error";
				}
				ckinfo.cksize=(ckinfo.cksize<<7)|(b & 0x7F);
			}
			offset+=ckinfo.cksize+10;
		}
	}
	mmioSeek(hmmio,offset,SEEK_SET);

	for ( ; 1==mmioRead(hmmio,&b,1) && b==0 ; offset++);
	mmioSeek(hmmio,offset,SEEK_SET);

	memset(&ckRIFF, 0, sizeof(ckRIFF));
	if (4==mmioRead(hmmio,(HPSTR)&frame_header, 4) &&
		(frame_header & 0x0000E0FF) == 0x0000E0FF )
	{
		mmioSeek(hmmio,0,SEEK_SET);
		m_data_size=mmioSeek(hmmio,0,SEEK_END)-offset;
		m_HeaderSize=offset;
		mmioSeek(hmmio,0,SEEK_SET);
	}
	else
	{
		mmioSeek(hmmio,0,SEEK_SET);
		if (0 != mmioDescend(hmmio, &ckRIFF, NULL, MMIO_FINDRIFF))
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			return "file format error";
		}
		else if (ckRIFF.fccType == mmioFOURCC('W','A','V','E'))
		{
			ckFMT.ckid = mmioFOURCC('f','m','t',' ');
			if (0!=mmioDescend(hmmio, &ckFMT, &ckRIFF, MMIO_FINDCHUNK) ||
				ckFMT.cksize>32 )
			{
			    mmioClose(hmmio, 0);
				hmmio = (HMMIO)NULL;
				return "format chunk error";
			}

			if (ckFMT.cksize!=(DWORD)mmioRead(hmmio,format_buf, ckFMT.cksize))
			{
			    mmioClose(hmmio, 0);
				hmmio = (HMMIO)NULL;
				return "format chunk read error";
			}

			if ((WORD)*format_buf != 85 )
			{
			    mmioClose(hmmio, 0);
				hmmio = (HMMIO)NULL;
				return "not MPEG format";
			}
		}
		else if (ckRIFF.fccType == mmioFOURCC('R','M','P','3'))
		{
		}
		else
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			return "WAVE format error";
		}

		ckDATA.ckid = mmioFOURCC('d','a','t','a');
		if (0 != mmioDescend(hmmio, &ckDATA, &ckRIFF, MMIO_FINDCHUNK))
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			return "data chunk error";
		}
		for (offset=0 ; 1==mmioRead(hmmio,&b,1) && b==0 ; offset++);
		m_data_size=ckDATA.cksize-offset;
		m_HeaderSize=ckDATA.dwDataOffset+offset;
	}
	
	// remove ID3 tag
	mmioSeek(hmmio,m_HeaderSize+m_data_size-128,SEEK_SET);
	if (3==mmioRead(hmmio,id_tag, 3) &&
		id_tag[0]=='T' && id_tag[1]=='A' &&id_tag[2]=='G' )
	{
		m_data_size-=128;
		mmioRead(hmmio,track_name, 30);
		track_name[30]='\0';
		if (m_title.IsEmpty())
		{
			m_title=track_name;
			m_title.TrimRight(' ');
		}
		mmioRead(hmmio,artist_name, 30);
		artist_name[30]='\0';
		if (m_label.IsEmpty() || m_label_no.IsEmpty())
		{
			int n;
			m_label=artist_name;
			m_label.TrimRight(' ');
			if ((n=m_label.ReverseFind(' '))>0
			||  (n=m_label.ReverseFind('-'))>0 )
			{
				m_label_no=m_label.Right(m_label.GetLength()-n-1);
				m_label=m_label.Left(n);
			}
		}
	}

	// get decode info
	mmioSeek(hmmio,m_HeaderSize,SEEK_SET);
	frame_size=156 ;
	frame_size=mmioRead(hmmio, frame_buf_0, frame_size) ;
	if (frame_size<4)
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "1st frame size error";
	}

	mp3DecodeInit();
    memset(&m_decode_info0, 0, sizeof(m_decode_info0));
	if (0==mp3GetDecodeInfo(frame_buf_0,frame_size,&m_decode_info0,1))
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "1st frame error";
	}
	
	// wave fomart set
	wfmtex.wFormatTag      = WAVE_FORMAT_PCM;
	wfmtex.nChannels       = m_decode_info0.channels;
	wfmtex.nSamplesPerSec  = m_decode_info0.frequency;
	wfmtex.wBitsPerSample  = m_decode_info0.bitsPerSample;
	wfmtex.nBlockAlign     = wfmtex.nChannels * wfmtex.wBitsPerSample/8;
	wfmtex.nAvgBytesPerSec = wfmtex.nSamplesPerSec * wfmtex.nBlockAlign;
	wfmtex.cbSize          = 0;

	m_sample_per_frame=m_decode_info0.outputSize/wfmtex.nBlockAlign;
	if (NULL==(m_frame_buf=SdpAllocBuffer(m_decode_info0.maxInputSize)))
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "frame buffer alloc error";
	}
	if (NULL==(m_output_buf=SdpAllocBuffer(m_decode_info0.outputSize)))
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "output buffer alloc error";
	}

	f_Rewind();
	while((frame_size=f_GetNextFrame(m_frame_buf))>0);
	if (frame_size<0)
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "data error";
	}
	m_decode_info0.frames=m_next_frame;

//	if (m_decode_info0.bitRate==0)
//	{
//		m_decode_info0.frames--;
//	}

	m_WaveDataSize=m_sample_per_frame*m_decode_info0.frames*wfmtex.nBlockAlign;

/*
	static CString str;
	str.Format("br=%d size=%d frames=%d(%d %d)",
		m_decode_info0.bitRate/1000,
		m_data_size,
		m_decode_info0.frames,m_paddings,m_no_paddings);
	f_SetStatusText(1,str);
	m_status_text1=str;
//*/
//	str.Format("s/f=%d skip=%d",m_sample_per_frame,m_decode_info0.skipSize);
//	return str;

	// decoding start
	if (!f_Wave_Rewind())
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "decoding start error";
	}

//	return "success";
	return "";
}

void CMusicMPEG::f_Wave_CloseFile()
{
	m_data_size=0;
	m_data_pos=0;
	m_next_frame=0;
	SdpFreeBuffer(m_frame_buf);
	SdpFreeBuffer(m_output_buf);
	m_frame_buf=NULL;
	m_output_buf=NULL;
	CMusicWave::f_Wave_CloseFile();
	return;
}

DWORD CMusicMPEG::f_Wave_ReadData(LPSTR buf, DWORD from, DWORD len)
{
	if (len<=0)	return 0;
	DWORD total=0;
	long num,start_pos,end_pos;
	long start_frame=from/m_sample_per_frame;
	long start_offset=from%m_sample_per_frame;
	long end_frame=(from+len-1)/m_sample_per_frame;
	long end_offset=(from+len-1)%m_sample_per_frame;
	long frame_size;
	LPSTR buf0=buf;

	if (m_next_frame==start_frame+1)
	{
		start_pos=start_offset;
		end_pos=m_sample_per_frame-1;
		if (m_next_frame==end_frame+1)
		{
			end_pos=end_offset;
		}
		num=end_pos-start_pos+1;
		memcpy(buf,m_output_buf+start_pos*wfmtex.nBlockAlign,num*wfmtex.nBlockAlign);
		buf+=num*wfmtex.nBlockAlign;
		total+=num;
		if (total>len)
		{
			f_SetStatusText(1,"docode output length over");
		}
	}
	else if (m_next_frame>start_frame+1)
	{
		f_Rewind();
	}

	while(m_next_frame<=end_frame)
	{
		if ((frame_size=f_GetNextFrame(m_frame_buf))<=0)
		{
			f_SetMusicInfo();
			return f_Wave_MixData(buf0,from,total);
		}
		if (m_next_frame>start_frame-9)
		{
			MPEG_DECODE_PARAM dec_param;
			memset(&dec_param,0,sizeof(dec_param));
			dec_param.inputBuf=m_frame_buf;
			dec_param.inputSize=frame_size;
			dec_param.outputBuf=m_output_buf;
			// decode
			if (mp3DecodeFrame(&dec_param)==0)
			{
				f_SetStatusText(1,"docoding error");
			}
			if (dec_param.outputSize>m_decode_info0.outputSize)
			{
				f_SetStatusText(1,"docode output bufsize over");
			}
		}
		
		if (m_next_frame>start_frame)
		{
			start_pos=0;
			end_pos=m_sample_per_frame-1;
			if (m_next_frame==start_frame+1)
			{
				start_pos=start_offset;
			}
			if (m_next_frame==end_frame+1)
			{
				end_pos=end_offset;
			}
			num=end_pos-start_pos+1;

			memcpy(buf,m_output_buf+start_pos*wfmtex.nBlockAlign,num*wfmtex.nBlockAlign);
			buf+=num*wfmtex.nBlockAlign;
			total+=num;
			if (total>len)
			{
				f_SetStatusText(1,"docode output length over");
			}
		}
	}
	f_SetMusicInfo();
	return f_Wave_MixData(buf0,from,total);
}

int CMusicMPEG::f_Wave_Rewind()
{
	int res=1;
	long frame_size;
	CMusicWave::f_Wave_Rewind();
	
	f_Rewind();
	mp3DecodeInit();
	if (0==(frame_size=f_GetNextFrame(m_frame_buf)) ||
		0==mp3DecodeStart(m_frame_buf,frame_size))
	{
		res=0;
	}
	f_Rewind();
	f_SetMusicInfo();
	return res;
}

int CMusicMPEG::f_Rewind()
{
	mmioSeek(hmmio,m_HeaderSize+m_decode_info0.skipSize,SEEK_SET);
	m_data_pos=0;
	m_next_frame=0;
	m_paddings=0;
	m_no_paddings=0;
	m_decode_info=m_decode_info0;

	return 1;
}

long CMusicMPEG::f_GetNextFrame(LPSTR frame_buf)
{
	long size;
	size=m_decode_info0.maxInputSize;
	if (m_data_pos+size>m_data_size-m_decode_info0.skipSize)
	{
		size=m_data_size-m_decode_info0.skipSize-m_data_pos;
	}
	mmioSeek(hmmio,m_HeaderSize+m_decode_info0.skipSize+m_data_pos,SEEK_SET);
	size=mmioRead(hmmio,frame_buf,size);
	if (size<m_decode_info0.minInputSize)
	{
		return 0;
	}

    memset(&m_decode_info, 0, sizeof(m_decode_info));
	if (0==mp3GetDecodeInfo(frame_buf,size,&m_decode_info,1))
	{
/*		static CString str;
		str.Format("pos=%d/%d size=%d frame=%d error=%d",
			m_data_pos,m_data_size-m_decode_info0.skipSize,
			size,
			m_next_frame,
			mp3GetLastError());
		f_SetStatusText(1,str);
		m_status_text1=str;
*/
		return -1;
	}
	if (m_decode_info.header.padding)
	{
		size=m_decode_info.maxInputSize;
		m_paddings++;
	}
	else
	{
		size=m_decode_info.minInputSize;
		m_no_paddings++;
	}
	m_data_pos+=size;
	m_next_frame++;
	return size;
}

void CMusicMPEG::f_SetMusicInfo(void)
{
	CString br,ch;
	if (m_decode_info.bitRate)
	{
		br.Format("%dk",m_decode_info.bitRate/1000);
	}
	else
	{
		br="VBR";
	}

	if (m_nch==1)		{ch="mono";}
	else if (m_nch==2)	{ch="stereo";}
	else				{ch.Format("%d ch",m_nch);}

	m_music_info.Format(" -- M%dL%d (%s) %dHz %dbit %s --",
			m_decode_info.header.version,
			m_decode_info.header.layer,
			ch,
			m_sampling_rate,
			m_sample_bits,
			br);
}
