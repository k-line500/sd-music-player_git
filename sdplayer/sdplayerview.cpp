// SdPlayerView.cpp : CSdPlayerView クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "SdPlayer.h"

#include "SdPlayerDoc.h"
#include "SdPlayerView.h"
#include "MainFrm.h"

#include <afxole.h>

#include <stdlib.h>
#include <time.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView

IMPLEMENT_DYNCREATE(CSdPlayerView, CListView)

BEGIN_MESSAGE_MAP(CSdPlayerView, CListView)
	//{{AFX_MSG_MAP(CSdPlayerView)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_MoveUp, OnEDITMoveUp)
	ON_UPDATE_COMMAND_UI(ID_EDIT_MoveUp, OnUpdateEDITMoveUp)
	ON_COMMAND(ID_EDIT_MoveDown, OnEDITMoveDown)
	ON_UPDATE_COMMAND_UI(ID_EDIT_MoveDown, OnUpdateEDITMoveDown)
	ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
	ON_COMMAND(ID_RandomSelect, OnRandomSelect)
	ON_UPDATE_COMMAND_UI(ID_RandomSelect, OnUpdateRandomSelect)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_COMMAND(ID_FILE_ReOpen, OnFILEReOpen)
	ON_UPDATE_COMMAND_UI(ID_FILE_ReOpen, OnUpdateFILEReOpen)
	ON_COMMAND(ID_FILE_Add, OnFILEAdd)
	ON_WM_KEYUP()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_ClearAllCheck, OnClearAllCheck)
	ON_COMMAND(ID_PathAbsToRel, OnPathAbsToRel)
	ON_COMMAND(ID_PathRelToAbs, OnPathRelToAbs)
	ON_COMMAND(ID_ClearCheck, OnClearCheck)
	//}}AFX_MSG_MAP
	// 標準印刷コマンド
	ON_COMMAND(ID_FILE_PRINT, CListView::OnFilePrint)
//	ON_COMMAND(ID_FILE_PRINT_DIRECT, CListView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CListView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView クラスの構築/消滅

CSdPlayerView::CSdPlayerView()
{
	// TODO: この場所に構築用のコードを追加してください。
	m_char_width=1;
}

CSdPlayerView::~CSdPlayerView()
{
}

BOOL CSdPlayerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

//	cs.style|=LVS_REPORT | LVS_NOSORTHEADER ;
	cs.style|=LVS_REPORT ;
	return CListView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView クラスの描画

void CSdPlayerView::OnDraw(CDC* pDC)
{
	CListCtrl& refCtrl = GetListCtrl();
	CSdPlayerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: この場所にネイティブ データ用の描画コードを追加します。
	CString str;
	LVCOLUMN column;
	char buf[80];
	column.cchTextMax=80;
	column.mask=LVCF_TEXT;
	column.pszText=buf;
	int width,th,thmax,i,j,max;
	int col[8]={17,0,1,2,5,6,12,13};
	int cols=8;
	CRect rect;
	CFont font,*font0;

	font.CreatePointFont(100,"MS UI Gothic",pDC);
	font0=pDC->SelectObject(&font);

	width=0; 
	for (i=0 ; i<cols ; i++)
	{
		width+=refCtrl.GetColumnWidth(col[i]);
	}

	double ratio=(double)pDC->GetDeviceCaps(HORZRES)/(double)width;
	thmax=0;
	rect.SetRect(0,0,1,1);
	for (i=0 ; i<cols ; i++)
	{
		refCtrl.GetColumn(col[i],&column);
		str=buf;
		th=pDC->DrawText(str,&rect,DT_CALCRECT);
		if (thmax<th)	{thmax=th;}
		rect.right=rect.left+(int)(refCtrl.GetColumnWidth(col[i])*ratio);
		pDC->DrawText(str,&rect,DT_END_ELLIPSIS);
		rect.OffsetRect(rect.Width(),0);
	}
	
	for (j=0, max=refCtrl.GetItemCount() ; j<max ; j++)
	{
		rect.left=0;
		rect.OffsetRect(0,thmax);
		thmax=0;
		for (i=0 ; i<cols ; i++)
		{
			str=refCtrl.GetItemText(j,col[i]);
			th=pDC->DrawText(str,&rect,DT_CALCRECT);
			if (thmax<th)	{thmax=th;}
			rect.right=rect.left+(int)(refCtrl.GetColumnWidth(col[i])*ratio);
			if (col[i]==13)
			{
				pDC->DrawText(str,&rect,DT_PATH_ELLIPSIS);
			}
			else
			{
				pDC->DrawText(str,&rect,DT_END_ELLIPSIS);
			}
			rect.OffsetRect(rect.Width(),0);
		}
	}
	pDC->SelectObject(font0);
	font.DeleteObject();
}

void CSdPlayerView::OnInitialUpdate()
{
//	CListView::OnInitialUpdate();

	// TODO: GetListCtrl() メンバ関数の呼び出しを通して直接そのリスト コントロールに
	//  アクセスすることによって ListView をアイテムで固定できます。
	CListCtrl& refCtrl = GetListCtrl();
	CSdPlayerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	f_SetFont();
	GetParentFrame()->SetWindowText(pDoc->GetPathName());

	refCtrl.SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_CHECKBOXES|LVS_EX_HEADERDRAGDROP);

	refCtrl.DeleteAllItems();
	while(refCtrl.DeleteColumn(0));

	for (int ci=0,cmax=pDoc->m_subitems ; ci<cmax ; ci++)
	{
		CString cstr=pDoc->m_coltitle.GetAt(ci);
		if (cstr)
		{
			refCtrl.InsertColumn(ci,cstr,LVCFMT_LEFT,10,ci);
		}
		else
		{
			refCtrl.InsertColumn(ci,"empty",LVCFMT_LEFT,10,ci);
		}
	}

	f_SetColumnOrder();
	f_SetColumnWidth();
	CListView::OnInitialUpdate();
}

void CSdPlayerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください
	
	CListCtrl& refCtrl = GetListCtrl();
	CSdPlayerDoc* pDoc = GetDocument();
	CSdpProfile *profile=&(((CSdPlayerApp*)AfxGetApp())->m_profile);
	ASSERT_VALID(pDoc);

	GetParentFrame()->SetWindowText(pDoc->GetPathName());

	for (int i=lHint,imax=pDoc->m_music_list.GetSize() ; i<imax ; i++)
	{
		int items=refCtrl.GetItemCount();
		if ( i>=items ) refCtrl.InsertItem(items,"");
		if (CStringArray *sa=(CStringArray *)pDoc->m_music_list.GetAt(i))
		{
			for (int ci=0,cmax=sa->GetSize() ; ci<cmax ; ci++)
			{
				CString cstr=sa->GetAt(ci);

				if ((ci==5 || ci==6) && profile->opt_undisp_rep && sa->GetAt(4)=="OFF")
				{
					cstr="";
				}
	
				if (cstr)
				{
					refCtrl.SetItemText(i,ci,cstr);
				}
				else
				{
					refCtrl.SetItemText(i,ci,"empty");
				}
			}

			if (sa->GetAt(18).Find('a')>=0)
			{
				refCtrl.SetCheck(i,TRUE);
			}
			else
			{
				refCtrl.SetCheck(i,FALSE);
			}
		}
		else
		{
			refCtrl.SetItemText(i,0,"empty");
		}
	}

	while(i<refCtrl.GetItemCount())
	{
		refCtrl.DeleteItem(i);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView クラスの印刷

BOOL CSdPlayerView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// デフォルトの印刷準備
	return DoPreparePrinting(pInfo);
}

void CSdPlayerView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷前の特別な初期化処理を追加してください。
}

void CSdPlayerView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷後の後処理を追加してください。
}

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView クラスの診断

#ifdef _DEBUG
void CSdPlayerView::AssertValid() const
{
	CListView::AssertValid();
}

void CSdPlayerView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}

CSdPlayerDoc* CSdPlayerView::GetDocument() // 非デバッグ バージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSdPlayerDoc)));
	return (CSdPlayerDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerView クラスのメッセージ ハンドラ

void CSdPlayerView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください

	// make sure window is active
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);

	CMenu menu;
	if (menu.LoadMenu(IDR_SDPLAYTYPE))
	{
		CMenu* pPopup = menu.GetSubMenu(1);
		ASSERT(pPopup != NULL);

		pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,
            point.x, point.y,
			AfxGetMainWnd(),NULL); // use main window for cmds
	}
}

void CSdPlayerView::OnEditCut() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	CListCtrl& refCtrl = GetListCtrl();

	int is=refCtrl.GetSelectionMark();

	OnEditCopy();

	int ie=-1;
	for (int i=0 ; i<refCtrl.GetItemCount() ; )
	{
		if (refCtrl.GetItemState(i,LVIS_SELECTED))
		{
			if (ie<0)	ie=i;
			int max=refCtrl.GetItemCount()-1;
			for (int j=i ; j<max ; j++)
			{
				refCtrl.SetItemState(j,refCtrl.GetItemState(j+1,LVIS_SELECTED),LVIS_SELECTED);
			}
			pDoc->f_DelItem(i);
		}
		else
		{
			i++;
		}
	}

	if (ie>=0 && pDoc->m_stack)
	{
		pDoc->m_stack->RemoveAll();
		delete pDoc->m_stack;
		pDoc->m_stack=NULL;
		pDoc->m_undo_flag=0;
	}
	if (ie>=0)	is=ie;
	if (is>=refCtrl.GetItemCount())	is=refCtrl.GetItemCount()-1;
	refCtrl.SetSelectionMark(is);
	refCtrl.SetItemState(is,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
}

void CSdPlayerView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	if (refCtrl.GetSelectedCount()>0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

void CSdPlayerView::OnEditPaste() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	CListCtrl& refCtrl = GetListCtrl();
	CStringArray *sa;
	CString str;
	BOOL end,lf;

	int is=refCtrl.GetSelectionMark();

	if (is<0)
	{
		is=0;
	}

	COleDataObject data;
	data.AttachClipboard();
	if (!data.IsDataAvailable(CF_TEXT))	{return;}

	HGLOBAL hData=data.GetGlobalData(CF_TEXT);
	if (!hData)	{return;}
	LPVOID mem=GlobalLock(hData);

	int data_version;

	char *cp0=(char*)mem;
	char *cp1,*cp2;

	end=0;
	if (cp1=strchr(cp0,'\n'))
	{
		*cp1='\0';
		sscanf(cp0,"%d",&data_version);
		cp0=cp1+1;
	}
	else
	{
		end=1;
	}

	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		refCtrl.SetItemState(i,0,LVIS_SELECTED);
	}

	for (i=is ; !end && *cp0 ; i++)
	{
		cp2=strchr(cp0,'\n');
		if (cp2==NULL)
		{
			break;
		}
		sa = new CStringArray;
		sa->SetSize(pDoc->m_subitems);
		lf=0;
		for (int si=0 ; si<pDoc->m_subitems && !end && !lf ; si++)
		{
			if ((cp1=strchr(cp0,'\t')) && cp1<cp2 )
			{
				*cp1='\0';
				sa->SetAt(si,cp0);
				cp0=cp1+1;
			}
			else if ((cp1=strchr(cp0,'\n')) && cp1==cp2)
			{
				*cp1='\0';
				sa->SetAt(si,cp0);
				cp0=cp1+1;
				lf=1;
			}
			else
			{
				sa->SetAt(si,cp0);
				end=1;
			}
		}
		pDoc->f_AddItem(i,sa);
		refCtrl.SetItemState(i,LVIS_SELECTED,LVIS_SELECTED);
	}

	if (GlobalUnlock(hData)==0)	{GlobalFree(hData);}
	data.Release();

	if (i>is && pDoc->m_stack)
	{
		pDoc->m_stack->RemoveAll();
		delete pDoc->m_stack;
		pDoc->m_stack=NULL;
		pDoc->m_undo_flag=0;
	}

	if (--i<is)	i=is;
	if (i>=refCtrl.GetItemCount())	i=refCtrl.GetItemCount()-1;
	refCtrl.SetSelectionMark(i);
	refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
	refCtrl.EnsureVisible(i,TRUE);
}

void CSdPlayerView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	COleDataObject data;
	data.AttachClipboard();
	if (data.IsDataAvailable(CF_TEXT))
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
	data.Release();	
}

void CSdPlayerView::OnEditCopy() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	CListCtrl& refCtrl = GetListCtrl();
	CStringArray *sa;
	CString str;
	HANDLE hData;

//	int i=refCtrl.GetSelectionMark();
//
//	if (i<0)
//	{
//		return;
//	}

	str.Format("%d\n",pDoc->m_data_version);
	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		if (refCtrl.GetItemState(i,LVIS_SELECTED))
		{
			sa=(CStringArray*)pDoc->m_music_list.GetAt(i);
			
			if (sa->GetSize()<pDoc->m_subitems)
			{
				sa->SetSize(pDoc->m_subitems);
			}
			
			for (int si=0 ; si<pDoc->m_subitems ; si++)
			{
				str+=sa->GetAt(si);
				str+="\t";
			}
			str.SetAt(str.GetLength()-1,'\n');
		}
//		refCtrl.SetItemState(i,0,LVIS_SELECTED);
	}
	DWORD len=str.GetLength()+1;
	hData=GlobalAlloc(GMEM_FIXED,len);
	sprintf((char*)hData,"%s",str);

	// Create an OLE data source on the heap
	COleDataSource* pData = new COleDataSource;
	pData->CacheGlobalData( CF_TEXT, hData );
	pData->SetClipboard();
}

void CSdPlayerView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	if (refCtrl.GetSelectedCount()>0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}


void CSdPlayerView::OnEDITMoveUp() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	int i=refCtrl.GetSelectionMark();
	if (i<=0)	{return;}
	OnEditCut();
	i=refCtrl.GetSelectionMark();
	refCtrl.SetSelectionMark(i-1);
	OnEditPaste();
	refCtrl.EnsureVisible(i,TRUE);
	refCtrl.EnsureVisible(i-1,TRUE);
}

void CSdPlayerView::OnUpdateEDITMoveUp(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	if (refCtrl.GetSelectionMark()>=0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CSdPlayerView::OnEDITMoveDown() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	int i=refCtrl.GetSelectionMark();
	if (i<0 || i>=refCtrl.GetItemCount()-1)	{return;}
	OnEditCut();
	i=refCtrl.GetSelectionMark();
	if (i+1>=refCtrl.GetItemCount())
	{
		refCtrl.InsertItem(i+1,"");
	}
	refCtrl.SetSelectionMark(i+1);
	OnEditPaste();
	refCtrl.EnsureVisible(i+1,TRUE);
}

void CSdPlayerView::OnUpdateEDITMoveDown(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	if (refCtrl.GetSelectionMark()>=0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CSdPlayerView::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください

	CSdPlayerDoc* pDoc = GetDocument();
	int col=pNMListView->iSubItem;

	if (col<4 || col==13)
	{
		pDoc->f_SortItems(col);
	}
	
	*pResult = 0;
}

void CSdPlayerView::OnRandomSelect() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	static unsigned int seed=0;
	static int prev[50];
	static int prev_num=0;
	int i,prev_max;

	CListCtrl& refCtrl = GetListCtrl();
	if (seed==0)
	{
		seed=time(NULL);
		srand(seed);
	}
	prev_max=refCtrl.GetItemCount()/2;
	if (prev_max>50)	{prev_max=50;}
	if (prev_num>prev_max)	{prev_num=prev_max;};

	i=refCtrl.GetSelectionMark();
	refCtrl.SetItemState(i,0,LVIS_SELECTED|LVIS_FOCUSED);	

	do
	{
		i=rand()*refCtrl.GetItemCount()/(RAND_MAX+1);
		for (int j=0 ; j<prev_num ; j++)
		{
			if (prev[j]==i)
			{
				i=-1;
				break;
			}
		}
	}while(i<0);

	if (prev_num<prev_max)
	{
		prev[prev_num++]=i;
	}
	else
	{
		for (int j=0 ; j<prev_num-1 ; j++)
		{
			prev[j]=prev[j+1];
		}
		prev[j]=i;
	}

	refCtrl.SetSelectionMark(i);
	refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
	refCtrl.EnsureVisible(i,TRUE);
}

void CSdPlayerView::OnUpdateRandomSelect(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	if (refCtrl.GetItemCount()>0)
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CSdPlayerView::OnEditSelectAll() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();
	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		refCtrl.SetItemState(i,LVIS_SELECTED,LVIS_SELECTED);
	}

}

void CSdPlayerView::OnFILEReOpen() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	pDoc->f_ReOpenFile();
}

void CSdPlayerView::OnUpdateFILEReOpen(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	if (pDoc->GetPathName()&&*pDoc->GetPathName())
	{
		pCmdUI->Enable(TRUE);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}	
}

void CSdPlayerView::OnFILEAdd() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();

	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		refCtrl.SetItemState(i,0,LVIS_SELECTED);
	}

	CSdPlayerDoc* pDoc = GetDocument();
	pDoc->f_AddFile();

	if (--i<0)	i=0;
	refCtrl.SetSelectionMark(i);
	refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
	refCtrl.EnsureVisible(i,TRUE);
	if (i+1<refCtrl.GetItemCount())
	{
		refCtrl.EnsureVisible(i+1,TRUE);
	}
}

void CSdPlayerView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	if (nChar==VK_SPACE)
	{
		f_get_check();
	}
	CListView::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CSdPlayerView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	f_get_check();
	CListView::OnLButtonUp(nFlags, point);
}

void CSdPlayerView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	f_get_check();
	CListView::OnRButtonUp(nFlags, point);
}

void CSdPlayerView::f_get_check()
{
	CListCtrl& refCtrl = GetListCtrl();
	CSdPlayerDoc* pDoc = GetDocument();

	int modify=0;
	for (int i=0,imax=refCtrl.GetItemCount(); i<imax ; i++)
	{
		CStringArray *sa=(CStringArray *)pDoc->m_music_list.GetAt(i);
		CString str=sa->GetAt(18);
		if (refCtrl.GetCheck(i))
		{
			if (str.Find('a')<0)
			{
				sa->SetAt(18,str+"a");
				modify=1;
			}
		}
		else
		{
			if (str.Find('a')>=0)
			{
				str.Remove('a');
				sa->SetAt(18,str);
				modify=1;
			}
		}
	}
	if (modify==1)
	{
		pDoc->SetModifiedFlag(TRUE);
	}
}

void CSdPlayerView::f_set_check(int i)
{
	CListCtrl& refCtrl = GetListCtrl();
	refCtrl.SetCheck(i,TRUE);
	f_get_check();
}

void CSdPlayerView::OnClearAllCheck() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();

	for (int i=0,imax=refCtrl.GetItemCount(); i<imax ; i++)
	{
		refCtrl.SetCheck(i,FALSE);
	}
	f_get_check();
}

void CSdPlayerView::OnClearCheck() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CListCtrl& refCtrl = GetListCtrl();

	for (int i=0,imax=refCtrl.GetItemCount(); i<imax ; i++)
	{
		if (refCtrl.GetItemState(i,LVIS_SELECTED))
		{
			refCtrl.SetCheck(i,FALSE);
		}
	}
	f_get_check();
}

void CSdPlayerView::OnPathAbsToRel() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	CListCtrl& refCtrl = GetListCtrl();

	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		if (refCtrl.GetItemState(i,LVIS_SELECTED))
		{
			pDoc->f_PathAbsToRel(i);
		}
	}
}

void CSdPlayerView::OnPathRelToAbs() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerDoc* pDoc = GetDocument();
	CListCtrl& refCtrl = GetListCtrl();

	for (int i=0, max=refCtrl.GetItemCount() ; i<max ; i++)
	{
		if (refCtrl.GetItemState(i,LVIS_SELECTED))
		{
			pDoc->f_PathRelToAbs(i);
		}
	}
}

void CSdPlayerView::f_SetFont()
{
	TEXTMETRIC tm;
	CDC *dc=GetDC();
	CFont *font=((CMainFrame*)AfxGetMainWnd())->m_view_font;
	if (font!=NULL)
	{
		CFont *font0=dc->SelectObject(font);
		dc->GetTextMetrics(&tm);
		dc->SelectObject(font0);
		SetFont(font);
		OnUpdate(NULL,0,NULL);
	}
	else
	{
		dc->GetTextMetrics(&tm);
	}
	ReleaseDC(dc);
	m_char_width=tm.tmMaxCharWidth;

//	CString str;
//	str.Format("cw=%d",m_char_width);
//	::AfxMessageBox(str);
}

void CSdPlayerView::f_SetColumnWidth()
{
	CListCtrl& refCtrl = GetListCtrl();
	int *col_width=((CMainFrame*)AfxGetMainWnd())->m_view_column_width;
	int max=refCtrl.GetHeaderCtrl()->GetItemCount();
	for (int i=0 ; i<max ; i++)
	{
		int colw=col_width[i];
		colw*=m_char_width;
		colw/=10;
		refCtrl.SetColumnWidth(i,colw);
	}
}

void CSdPlayerView::f_GetColumnWidth(int *width)
{
	CListCtrl& refCtrl = GetListCtrl();
	int max=refCtrl.GetHeaderCtrl()->GetItemCount();
	for (int i=0 ; i<max ; i++)
	{
		int colw=refCtrl.GetColumnWidth(i);
		if (colw<=0)
		{
			width[i]=0;
		}
		else
		{
			colw*=10;
			colw/=m_char_width;
			if (colw==0)	{colw=1;}
			width[i]=colw;
		}
	}
}

void CSdPlayerView::f_GetColumnOrder(int *order)
{
	CListCtrl& refCtrl = GetListCtrl();
	refCtrl.GetColumnOrderArray(order);
}

void CSdPlayerView::f_SetColumnOrder(int *order)
{
	CListCtrl& refCtrl = GetListCtrl();
	
	int i=refCtrl.GetHeaderCtrl()->GetItemCount();
	refCtrl.SetColumnOrderArray(i,order);
}

void CSdPlayerView::f_SetColumnOrder()
{
	int *order=((CMainFrame*)AfxGetMainWnd())->m_view_column_order;
	f_SetColumnOrder(order);
}
