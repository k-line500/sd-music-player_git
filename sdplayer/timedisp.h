#if !defined(AFX_TIMEDISP_H__58ED0663_0791_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_TIMEDISP_H__58ED0663_0791_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeDisp.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CTimeDisp フレーム

class CTimeDisp : public CFrameWnd
{
	DECLARE_DYNCREATE(CTimeDisp)
protected:

public:
	CTimeDisp();           // 動的生成に使用されるプロテクト コンストラクタ。
// アトリビュート

// オペレーション
private:
	CString m_time_old;
	COLORREF m_back_color_old;
	void f_DispTimer(CDC *dc);
public:
	void f_DispTimer();
	CString m_time;
	COLORREF m_text_color;
	COLORREF m_back_color;
	BOOL m_back_mode;
	CFont m_font;
	BOOL m_repaint;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CTimeDisp)
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	virtual ~CTimeDisp();
	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CTimeDisp)
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_TIMEDISP_H__58ED0663_0791_11D7_BC30_009027BFCD6A__INCLUDED_)
