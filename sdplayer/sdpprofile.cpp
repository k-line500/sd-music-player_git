// SdpProfile.cpp: CSdpProfile クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "sdplayer.h"
#include "SdpProfile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

CSdpProfile::CSdpProfile()
{
	opt_undisp_rep=FALSE;
	opt_cb_items.SetSize(5);
	opt_cb_items.SetAt(0,"1 Title");
	opt_cb_items.SetAt(1,"0----------");
	opt_cb_items.SetAt(2,"2 Date");
	opt_cb_items.SetAt(3,"3 Time");
	opt_cb_items.SetAt(4,"4 User Text");
	opt_cb_usertext="";
	opt_cb_sel=2;
	opt_fadeout_len=1;
}

CSdpProfile::~CSdpProfile()
{

}

void CSdpProfile::f_write_profile()
{
	CString str;
	int i,max;

	AfxGetApp()->WriteProfileInt("option","undisp_rep",opt_undisp_rep);


	for (str="",i=0,max=opt_cb_items.GetSize() ; i<max ; i++)
	{
		str+=opt_cb_items.GetAt(i)+";";
	}
	AfxGetApp()->WriteProfileString("option","cb_items",str);
	AfxGetApp()->WriteProfileString("option","cb_usertext",opt_cb_usertext);

	AfxGetApp()->WriteProfileInt("option","cb_sel",opt_cb_sel);

	AfxGetApp()->WriteProfileInt("option","fadeout_len",opt_fadeout_len);

}

void CSdpProfile::f_read_profile()
{
	CString str;
	int i,n0,n1;

	opt_undisp_rep=AfxGetApp()->GetProfileInt("option","undisp_rep",opt_undisp_rep);

	str=AfxGetApp()->GetProfileString("option","cb_items","");
	if (str!="")
	{
//		opt_cb_items.RemoveAll();
		const char *cp=str;
		for (i=0,n0=0,n1=0 ; i<opt_cb_items.GetSize() ; n0=n1+1,i++)
		{
			n1=str.Find(';',n0);
			if (n1<0) {break;}
			opt_cb_items.SetAt(i,str.Mid(n0,n1-n0));
		}
	}
	opt_cb_usertext=AfxGetApp()->GetProfileString("option","cb_usertext",opt_cb_usertext);

	opt_cb_sel=AfxGetApp()->GetProfileInt("option","cb_sel",opt_cb_sel);

	opt_fadeout_len=AfxGetApp()->GetProfileInt("option","fadeout_len",opt_fadeout_len);

}
