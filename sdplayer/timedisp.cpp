// TimeDisp.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "TimeDisp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimeDisp

IMPLEMENT_DYNCREATE(CTimeDisp, CFrameWnd)

CTimeDisp::CTimeDisp()
{
	m_time="00:00";
	m_text_color=RGB(0,0,0);
//	m_back_color=RGB(255,255,255); // white
	m_back_color=RGB(192,192,192); // gray
//	m_back_color=RGB(128,255,255); // cyan
//	m_back_color=RGB(128,255,128); // green
//	m_back_color=RGB(255,255,128); // yellow
//	m_back_color=RGB(255,192,64);  // orange
//	m_back_color=RGB(255,128,128); // pink
	m_back_mode=1;

	m_time_old=m_time;
	m_back_color_old=m_back_color;
	m_repaint=TRUE;
}

CTimeDisp::~CTimeDisp()
{
}


BEGIN_MESSAGE_MAP(CTimeDisp, CFrameWnd)
	//{{AFX_MSG_MAP(CTimeDisp)
	ON_WM_PAINT()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeDisp メッセージ ハンドラ

void CTimeDisp::OnPaint() 
{
	CPaintDC dc(this); // 描画用のデバイス コンテキスト

	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください
	m_repaint=TRUE;
	f_DispTimer(&dc);
	// 描画用メッセージとして CFrameWnd::OnPaint() を呼び出してはいけません
}

void CTimeDisp::f_DispTimer()
{
	CDC *dc=GetDC();
	f_DispTimer(dc);
	ReleaseDC(dc);
}

void CTimeDisp::f_DispTimer(CDC *dc)
{
	CFont *font0=dc->SelectObject(&m_font);
	
	if (!m_back_mode)
	{
		dc->SetBkMode(TRANSPARENT);
	}

	if (m_back_color!=m_back_color_old)
	{
		m_repaint=TRUE;
		m_back_color_old=m_back_color;
	}
	dc->SetBkColor(m_back_color);
	dc->SetTextColor(m_text_color);
//	dc->GetTextFace(m_time);

	if (m_repaint)
	{
		dc->TextOut(0,0,m_time+" ");
	}
	else
	{
		for (int i=0 ; i<5 && m_time[i]==m_time_old[i] ; i++);
		if (i<5)
		{
			dc->TextOut(dc->GetTextExtent(m_time.Left(i)).cx,0,
						m_time.Right(5-i)+" ");
		}
	}
	m_time_old=m_time;
	m_repaint=FALSE;
	dc->SelectObject(font0);
}

void CTimeDisp::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください

	m_font.DeleteObject();
	m_font.CreateFont(cy,cx/5,
		0,
		0,
		FW_BOLD,
		0,
		0,
		0,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		FIXED_PITCH|FF_MODERN,
		NULL);
//	m_repaint=TRUE;
//	InvalidateRect(NULL);
}

