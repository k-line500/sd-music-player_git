// MainFrm.cpp : CMainFrame クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "SdPlayer.h"

#include "MainFrm.h"
#include "SdPlayerDoc.h"
#include "SdPlayerView.h"
#include "Music.h"
#include "SdpOptProp01.h"
#include "SdpOptProp02.h"

#include <Afxpriv.h> // for WM_SETMESSAGESTRING

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static UINT WM_FINDREPLACE = ::RegisterWindowMessage(FINDMSGSTRING);

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_EditData, OnEditData)
	ON_BN_CLICKED(IDC_AddData, OnAddData)
	ON_BN_CLICKED(IDC_RADIO_Patter, OnRADIOPatter)
	ON_BN_CLICKED(IDC_RADIO_Singing, OnRADIOSinging)
	ON_BN_CLICKED(IDC_Load, OnLoad)
	ON_BN_CLICKED(IDC_UnLoad, OnUnLoad)
	ON_BN_CLICKED(IDC_Rewind, OnRewind)
	ON_BN_CLICKED(IDC_Play, OnPlay)
	ON_BN_CLICKED(IDC_Repeat, OnRepeat)
	ON_BN_CLICKED(IDC_TipTimer, OnTipTimer)
	ON_COMMAND(ID_Play, OnPlayByKey)
	ON_COMMAND(ID_Repeat, OnRepeatByKey)
	ON_UPDATE_COMMAND_UI(ID_Load, OnUpdateLoad)
	ON_UPDATE_COMMAND_UI(ID_Play, OnUpdatePlay)
	ON_UPDATE_COMMAND_UI(ID_Repeat, OnUpdateRepeat)
	ON_UPDATE_COMMAND_UI(ID_Rewind, OnUpdateRewind)
	ON_UPDATE_COMMAND_UI(ID_UnLoad, OnUpdateUnLoad)
	ON_UPDATE_COMMAND_UI(ID_TipTimer, OnUpdateTipTimer)
	ON_COMMAND(ID_NextWindow, OnNextWindow)
	ON_COMMAND(ID_VIEW_PlayerControl, OnVIEWPlayerControl)
	ON_UPDATE_COMMAND_UI(ID_VIEW_PlayerControl, OnUpdateVIEWPlayerControl)
	ON_UPDATE_COMMAND_UI(ID_Patter, OnUpdatePatter)
	ON_UPDATE_COMMAND_UI(ID_Singing, OnUpdateSinging)
	ON_BN_CLICKED(IDC_Reset, OnReset)
	ON_UPDATE_COMMAND_UI(ID_Reset, OnUpdateReset)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_WM_CLOSE()
	ON_COMMAND(ID_Win_StdSize, OnWinStdSize)
	ON_COMMAND(ID_VIEW_TimeDisplay, OnVIEWTimeDisplay)
	ON_UPDATE_COMMAND_UI(ID_VIEW_TimeDisplay, OnUpdateVIEWTimeDisplay)
	ON_COMMAND(ID_SetFont, OnSetFont)
	ON_COMMAND(ID_Win_FlatSize, OnWinFlatSize)
	ON_COMMAND(ID_SaveColumnOrder, OnSaveColumnOrder)
	ON_COMMAND(ID_ResetColumnOrder, OnResetColumnOrder)
	ON_COMMAND(ID_SaveColumnWidth, OnSaveColumnWidth)
	ON_COMMAND(ID_ResetColumnWidth, OnResetColumnWidth)
	ON_BN_CLICKED(IDC_CueSheet, OnCueSheet)
	ON_UPDATE_COMMAND_UI(ID_CueSheet, OnUpdateCueSheet)
	ON_COMMAND(ID_SetOptions, OnSetOptions)
	ON_COMMAND(ID_AddData, OnAddData)
	ON_COMMAND(ID_EditData, OnEditData)
	ON_COMMAND(ID_Load, OnLoad)
	ON_COMMAND(ID_Rewind, OnRewind)
	ON_COMMAND(ID_TipTimer, OnTipTimer)
	ON_COMMAND(ID_UnLoad, OnUnLoad)
	ON_COMMAND(ID_Patter, OnRADIOPatter)
	ON_COMMAND(ID_Singing, OnRADIOSinging)
	ON_COMMAND(ID_Reset, OnReset)
	ON_COMMAND(ID_CueSheet, OnCueSheet)
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	//}}AFX_MSG_MAP
	// グローバル ヘルプ コマンド
	ON_COMMAND(ID_HELP_FINDER, CMDIFrameWnd::OnHelpFinder)
	ON_COMMAND(ID_HELP, CMDIFrameWnd::OnHelp)
	ON_COMMAND(ID_CONTEXT_HELP, CMDIFrameWnd::OnContextHelp)
	ON_COMMAND(ID_DEFAULT_HELP, CMDIFrameWnd::OnHelpFinder)
	//
	ON_MESSAGE(WM_SETMESSAGESTRING, OnSetMessageString)
	ON_COMMAND_RANGE(ID_CPanel0,ID_CPanel9, OnCPanel)
	ON_UPDATE_COMMAND_UI_RANGE(ID_CPanel0,ID_CPanel9, OnUpdateCPanel)
	//
	ON_REGISTERED_MESSAGE( WM_FINDREPLACE, OnFindReplace )
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // ステータス ライン インジケータ
	ID_SEPARATOR,           // file name etc.
//	ID_INDICATOR_KANA,
//	ID_INDICATOR_CAPS,
//	ID_INDICATOR_NUM,
//	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame クラスの構築/消滅

CMainFrame::CMainFrame()
{
	// TODO: この位置にメンバの初期化処理コードを追加してください。
	cx0=460;
	cy0=425;
	cx1=800;
	cy1=425;
	panel_no=0;
	m_view_font=NULL;
	for (int i=0 ; i<SDP_DATABASE_ITEMS ; i++)
	{
		m_view_column_order[i]=i;
	}
	f_InitColumnWidth();
	m_find_str="";
}

CMainFrame::~CMainFrame()
{
	if (m_view_font)
	{
		m_view_font->DeleteObject();
		delete m_view_font;
	}
}

void CMainFrame::f_InitColumnWidth()
{
	m_view_column_width[0] =150;//Title
	m_view_column_width[1] = 40;//Label
	m_view_column_width[2] = 40;//No.
	m_view_column_width[3] = 50;//Category
	m_view_column_width[4] = 30;//Rep
	m_view_column_width[5] = 50;//RepStart
	m_view_column_width[6] = 50;//RepEnd
	m_view_column_width[7] = 30;//Wait
	m_view_column_width[8] = 40;//Tempo
	m_view_column_width[9] = 60;//Speed ch
	m_view_column_width[10]= 60;//Pitch ch
	m_view_column_width[11]= 60;//Tempo ch
	m_view_column_width[12]= 50;//Type
	m_view_column_width[13]=140;//File
	m_view_column_width[14]= 40;//RepMix
	m_view_column_width[15]= 50;//TempoOrg
	m_view_column_width[16]= 40;//B/Rep
	m_view_column_width[17]= 30;//Num
	m_view_column_width[18]=  0;//flags
	m_view_column_width[19]= 30;//CueSheet
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // 作成に失敗
	}
	if (!m_wndDlgBar.Create(this, IDR_MAINFRAME, 
		CBRS_ALIGN_BOTTOM, AFX_IDW_DIALOGBAR))
	{
		TRACE0("Failed to create dialogbar\n");
		return -1;		// 作成に失敗
	}
	m_wndDlgBar.f_CreateSubWindow();

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // 作成に失敗
	}

	// TODO: もしツール チップスが必要ない場合、ここを削除してください。
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_GRIPPER | CBRS_SIZE_DYNAMIC );

	m_wndDlgBar.SetBarStyle(m_wndDlgBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_GRIPPER | CBRS_SIZE_DYNAMIC );


	// TODO: ツール バーをドッキング可能にしない場合は以下の行を削除
	//       してください。
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndDlgBar.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	DockControlBar(&m_wndDlgBar);

	// modify StatusBar width
	UINT nID,nStyle;
	int cxWidth;
	m_wndStatusBar.GetPaneInfo(0,nID,nStyle,cxWidth);
	cxWidth=(int)(cxWidth*0.7);
	m_wndStatusBar.SetPaneInfo(0,nID,nStyle,cxWidth);

	m_wndStatusBar.GetPaneInfo(1,nID,nStyle,cxWidth);
	cxWidth=(int)(cxWidth*0.9);
	m_wndStatusBar.SetPaneInfo(1,nID,nStyle,cxWidth);

	// initialize dialog bar controls
	m_wndDlgBar.f_InitDialog();
	f_ChangeCPanel(AfxGetApp()->GetProfileInt("CPanel","no",0));

	// set view font
	LOGFONT lf;
	CString str;
	if (m_view_font==NULL && (str=AfxGetApp()->GetProfileString("ViewFont","FaceName",""))!="")
	{
		strcpy(lf.lfFaceName,str);
		lf.lfHeight       =AfxGetApp()->GetProfileInt("ViewFont","Height",0);
		lf.lfWidth        =AfxGetApp()->GetProfileInt("ViewFont","Width",0);
		lf.lfEscapement   =AfxGetApp()->GetProfileInt("ViewFont","Escapement",0);
		lf.lfOrientation  =AfxGetApp()->GetProfileInt("ViewFont","Orientation",0);
		lf.lfWeight       =AfxGetApp()->GetProfileInt("ViewFont","Weight",FW_DONTCARE);
		lf.lfItalic       =AfxGetApp()->GetProfileInt("ViewFont","Italic",FALSE);
		lf.lfUnderline    =AfxGetApp()->GetProfileInt("ViewFont","Underline",FALSE);
		lf.lfStrikeOut    =AfxGetApp()->GetProfileInt("ViewFont","StrikeOut",FALSE);
		lf.lfCharSet      =AfxGetApp()->GetProfileInt("ViewFont","CharSet",DEFAULT_CHARSET);
		lf.lfOutPrecision =AfxGetApp()->GetProfileInt("ViewFont","OutPrecision",OUT_DEFAULT_PRECIS);
		lf.lfClipPrecision=AfxGetApp()->GetProfileInt("ViewFont","ClipPrecision",CLIP_DEFAULT_PRECIS);
		lf.lfQuality      =AfxGetApp()->GetProfileInt("ViewFont","Quality",DEFAULT_QUALITY);
		lf.lfPitchAndFamily=AfxGetApp()->GetProfileInt("ViewFont","PitchAndFamily",DEFAULT_PITCH|FF_DONTCARE);
		m_view_font=new CFont;
		m_view_font->CreateFontIndirect(&lf);
	}

	// set view column order
	str=AfxGetApp()->GetProfileString("ViewColumn","ColumnOrder","");
	if (str!="")
	{
		const char *cp=str;
		for (int i=0 ; i<SDP_DATABASE_ITEMS ; i++)
		{
			if (sscanf(cp,"%d",&m_view_column_order[i])<=0)	{break;}
			if ((cp=strchr(cp,','))==NULL)
			{
				i++;
				break;
			}
			cp++;
		}
		for ( ; i<SDP_DATABASE_ITEMS ; i++)
		{
			m_view_column_order[i]=i;
		}
	}

	// set view column width
	str=AfxGetApp()->GetProfileString("ViewColumn","ColumnWidth","");
	if (str!="")
	{
		const char *cp=str;
		for (int i=0 ; i<SDP_DATABASE_ITEMS ; i++)
		{
			if (sscanf(cp,"%d",&m_view_column_width[i])<=0)	{break;}
			if ((cp=strchr(cp,','))==NULL)
			{
				i++;
				break;
			}
			cp++;
		}
	}

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	WNDCLASS wndcls;
	
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: この位置で CREATESTRUCT cs を修正して、Window クラスやスタイルを
	//       修正してください。
	if (::GetClassInfo(AfxGetInstanceHandle(), cs.lpszClass, &wndcls))
	{
		::UnregisterClass("SD Music Player",AfxGetInstanceHandle());
		cs.lpszClass=wndcls.lpszClassName="SD Music Player";
		::AfxRegisterClass(&wndcls);
	}

	cs.x=AfxGetApp()->GetProfileInt("position","x",0);
	cs.y=AfxGetApp()->GetProfileInt("position","y",0);
//	cs.cx=460;
	cs.cx=AfxGetApp()->GetProfileInt("position","w",cx0);
//	cs.cy=425;
	cs.cy=AfxGetApp()->GetProfileInt("position","h",cy0);

	if (cs.cx>::GetSystemMetrics(SM_CXSCREEN))
	{
		cs.cx=::GetSystemMetrics(SM_CXSCREEN);
	}
	if (cs.cy>::GetSystemMetrics(SM_CYSCREEN))
	{
		cs.cy=::GetSystemMetrics(SM_CYSCREEN);
	}
	if (cs.x>::GetSystemMetrics(SM_CXSCREEN))
	{
		cs.x=(::GetSystemMetrics(SM_CXSCREEN)-cs.cx)/2;
	}
	if (cs.y>::GetSystemMetrics(SM_CYSCREEN))
	{
		cs.y=(::GetSystemMetrics(SM_CYSCREEN)-cs.cy)/2;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame クラスの診断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CMainFrame メンバ関数

void CMainFrame::f_SetStatusText(int no, const char* text)
{
	CStatusBar *wnd;
	if (wnd=(CStatusBar *)GetMessageBar())
	{
		if (no>=0 && no<wnd->m_nCount)
		{
			wnd->SetPaneText(no,_T(text));
		}
	}
}

CSdPlayerView *CMainFrame::f_GetView()
{
	// アクティブな MDI 子ウィンドウを取得する。 
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (!pChild)	{return NULL;}
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView* pView=(CSdPlayerView *)pChild->GetActiveView();
	return pView;
}

CSdPlayerDoc *CMainFrame::f_GetDocument()
{
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView *pView=f_GetView();
	// ドキュメントを取得
	if (!pView){return NULL;}
	CSdPlayerDoc *pDoc=pView->GetDocument();
	return pDoc;
}

CStringArray *CMainFrame::f_GetDocumentData()
{
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView *pView=f_GetView();
	if (!pView)	{return NULL;}
	// ドキュメントを取得
	CSdPlayerDoc* pDoc = f_GetDocument();
	if (!pDoc)	{return NULL;}
	ASSERT_VALID(pDoc);
	// List Controlを取得
	CListCtrl& refCtrl = pView->GetListCtrl();

	CStringArray *sa;

	int i=refCtrl.GetSelectionMark();

	if (i<0)
	{
		sa=NULL;
	}
	else
	{
		sa = new CStringArray;
		sa->Copy(*(CStringArray*)pDoc->m_music_list.GetAt(i));
		if (sa->GetSize()<pDoc->m_subitems)
		{
			sa->SetSize(pDoc->m_subitems);
		}
	}
	return sa;
}

void CMainFrame::f_SetDocumentData(CStringArray *sa)
{
	// アクティブな MDI 子ウィンドウを取得する。 
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (!pChild)	{return;}
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView* pView=(CSdPlayerView *)pChild->GetActiveView();
	if (!pView)	{return;}
	// ドキュメントを取得
	CSdPlayerDoc* pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	// List Controlを取得
	CListCtrl& refCtrl = pView->GetListCtrl();

	int num=refCtrl.GetSelectionMark()+1;
	if (sa)
	{
		sscanf(sa->GetAt(17),"%d",&num);
		if (pDoc->m_stack)
		{
			pDoc->m_stack->RemoveAll();
			delete pDoc->m_stack;
		}
		pDoc->m_stack=(CStringArray *)pDoc->m_music_list.GetAt(num-1);
		pDoc->m_music_list.SetAt(num-1,sa);
		pDoc->m_undo_flag=1; // replaced
		pDoc->SetModifiedFlag(TRUE);
	}
	pDoc->UpdateAllViews(NULL);
	refCtrl.SetSelectionMark(num-1);
	refCtrl.SetItemState(num-1,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	refCtrl.EnsureVisible(num-1,TRUE);
}

void CMainFrame::f_AddDocumentData(CStringArray *sa)
{
	// アクティブな MDI 子ウィンドウを取得する。 
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (!pChild)	{return;}
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView* pView=(CSdPlayerView *)pChild->GetActiveView();
	if (!pView)	{return;}
	// ドキュメントを取得
	CSdPlayerDoc* pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	// List Controlを取得
	CListCtrl& refCtrl = pView->GetListCtrl();

	if (sa->GetSize()<pDoc->m_subitems)
	{
		sa->SetSize(pDoc->m_subitems);
	}

	int i;
	i=refCtrl.GetSelectionMark();
	refCtrl.SetItemState(i,0,LVIS_SELECTED|LVIS_FOCUSED);	

	i=pDoc->m_music_list.GetSize();
	CString str;
	str.Format("%d",i+1);
	sa->SetAt(17,str);
	pDoc->m_music_list.Add(sa);
	if (pDoc->m_stack)
	{
		pDoc->m_stack->RemoveAll();
		delete pDoc->m_stack;
		pDoc->m_stack=NULL;
	}
	pDoc->m_undo_flag=2; // added;
	pDoc->SetModifiedFlag(TRUE);
	pDoc->UpdateAllViews(NULL);
	refCtrl.SetSelectionMark(i);
	refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);
	refCtrl.EnsureVisible(i,TRUE);
}

CString CMainFrame::f_GetDocumentDir()
{
	// アクティブな MDI 子ウィンドウを取得する。 
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (!pChild)	{return "";}
	// アクティブな MDI 子ウィンドウに結び付けられているアクティブなビューを取得する。
	CSdPlayerView* pView=(CSdPlayerView *)pChild->GetActiveView();
	if (!pView)	{return "";}
	// ドキュメントを取得
	CSdPlayerDoc* pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);

	return pDoc->f_GetCurDocDir();
}

void CMainFrame::f_NextWindow(int n)
{
	static int focus=0;

	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();

	CSdPlayerView* pView=NULL;
	if (pChild!=(CMDIChildWnd *)this)
	{
		pView=(CSdPlayerView *)pChild->GetActiveView();
	}

	if (GetFocus()==pView)	{focus=1;}

	if (++focus>3)	{focus=1;}

	if (n>0)	{focus=n;}

	switch (focus)
	{
	case 1:
		if (pView)
		{
			pView->SetFocus();
		}
		else if (pChild)
		{
			pChild->SetFocus();
		}
		break;
	case 2:
		m_wndDlgBar.SetFocus();
		break;
	case 3:
		m_wndToolBar.SetFocus();
		break;
	default:
		break;
	}
}

void CMainFrame::f_ChangeCPanel(int n)
{
	if (n==panel_no)
	{
		return;
	}

	CPoint p(0,0);
	ShowControlBar(&m_wndDlgBar, FALSE, FALSE);
	FloatControlBar(&m_wndDlgBar,p);
	m_wndDlgBar.DestroyWindow();

	switch (n)
	{
	default:
	case 0:
		m_wndDlgBar.Create(this, IDR_MAINFRAME,CBRS_ALIGN_BOTTOM, AFX_IDW_DIALOGBAR);
		panel_no=0;
		break;
	case 1:
		m_wndDlgBar.Create(this, IDD_CPanel1,CBRS_ALIGN_BOTTOM, AFX_IDW_DIALOGBAR);
		panel_no=1;
		break;
	}

	m_wndDlgBar.SetBarStyle(m_wndDlgBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_GRIPPER | CBRS_SIZE_DYNAMIC );
	m_wndDlgBar.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
	DockControlBar(&m_wndDlgBar);
	m_wndDlgBar.f_InitDialog();
	m_wndDlgBar.f_RestartDialog();
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame メッセージ ハンドラ


void CMainFrame::OnEditData() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnEditData();
}

void CMainFrame::OnAddData() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnAddData();
}

void CMainFrame::OnRADIOPatter() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnRADIOPatter();
}

void CMainFrame::OnRADIOSinging() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnRADIOSinging();
}

void CMainFrame::OnLoad() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnLoad();
}

void CMainFrame::OnUnLoad() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUnLoad();	
}

void CMainFrame::OnRewind() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnRewind();	
}

void CMainFrame::OnPlay() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnPlay();	
}

void CMainFrame::OnRepeat() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnRepeat();	
}

void CMainFrame::OnReset() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnReset();	
}

void CMainFrame::OnTipTimer() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnTipTimer();	
}

void CMainFrame::OnCueSheet() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	m_wndDlgBar.OnCueSheet();	
}

void CMainFrame::OnPlayByKey() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	m_wndDlgBar.OnPlayByKey();
}

void CMainFrame::OnRepeatByKey() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	m_wndDlgBar.OnRepeatByKey();
}

void CMainFrame::OnUpdatePatter(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdatePatter(pCmdUI);
}

void CMainFrame::OnUpdateSinging(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateSinging(pCmdUI);
}

void CMainFrame::OnUpdateLoad(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateLoad(pCmdUI);
}

void CMainFrame::OnUpdatePlay(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdatePlay(pCmdUI);
}

void CMainFrame::OnUpdateRepeat(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateRepeat(pCmdUI);
}

void CMainFrame::OnUpdateRewind(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateRewind(pCmdUI);
}

void CMainFrame::OnUpdateUnLoad(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateUnLoad(pCmdUI);
}

void CMainFrame::OnUpdateReset(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateReset(pCmdUI);	
}

void CMainFrame::OnUpdateTipTimer(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateTipTimer(pCmdUI);
}

void CMainFrame::OnUpdateCueSheet(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	m_wndDlgBar.OnUpdateCueSheet(pCmdUI);
}

void CMainFrame::OnNextWindow() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	f_NextWindow(0);
}

void CMainFrame::OnVIEWPlayerControl() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	BOOL bVisible = ((m_wndDlgBar.GetStyle() & WS_VISIBLE) != 0);
	ShowControlBar(&m_wndDlgBar, !bVisible, FALSE);	
}

void CMainFrame::OnUpdateVIEWPlayerControl(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	BOOL bVisible = ((m_wndDlgBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

void CMainFrame::OnVIEWTimeDisplay() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	BOOL bVisible = ((m_wndDlgBar.m_pwndTimeDisp->GetStyle() & WS_VISIBLE) != 0);
	if (bVisible)
	{
		m_wndDlgBar.m_pwndTimeDisp->ShowWindow(SW_HIDE);
	}
	else
	{
		m_wndDlgBar.m_pwndTimeDisp->ShowWindow(SW_SHOW);
	}
}

void CMainFrame::OnUpdateVIEWTimeDisplay(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if (m_wndDlgBar.m_pwndTimeDisp)
	{
		BOOL bVisible = ((m_wndDlgBar.m_pwndTimeDisp->GetStyle() & WS_VISIBLE) != 0);
		pCmdUI->SetCheck(bVisible);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

LRESULT CMainFrame::OnSetMessageString(WPARAM wParam, LPARAM lParam)
{
	if (wParam==AFX_IDS_IDLEMESSAGE)
	{
		wParam=0;
		LPCTSTR str;
		if (m_wndDlgBar.music==NULL)
		{
			str="music is null";
		}
		else
		{
			str=m_wndDlgBar.music->m_status_text0;
		}
/*
		else if (m_wndDlgBar.music->m_play_on)
		{
			if (m_wndDlgBar.music->m_wait>0)
			{
				str="playing (wait)";
			}
			else
			{
				str="playing";
			}
		}
		else if (m_wndDlgBar.music->m_stat_play)
		{
			str="pause";
		}
		else if (m_wndDlgBar.music->m_stat_ready)
		{
			if (m_wndDlgBar.music->f_GetTime()==0)
			{
				str="ready";
			}
			else
			{
				str="stop";
			}
		}
		else if (m_wndDlgBar.music->m_stat_load)
		{
			str="not ready";
		}
		else
		{
			str="not loaded";
		}
*/
		lParam=(LPARAM)str;
	}
	return CFrameWnd::OnSetMessageString(wParam, lParam);
}


void CMainFrame::OnEditUndo() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (!pChild)	{return;}
	CSdPlayerView* pView=(CSdPlayerView *)pChild->GetActiveView();
	if (!pView)	{return;}
	CSdPlayerDoc* pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);
	CListCtrl& refCtrl = pView->GetListCtrl();
	pDoc->f_Undo(refCtrl);
}

void CMainFrame::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	CMDIChildWnd *pChild;
	CSdPlayerView* pView;
	CSdPlayerDoc* pDoc;

	if (!(pChild=(CMDIChildWnd *)GetActiveFrame()))
	{
		pCmdUI->Enable(FALSE);
	}
	else if (!(pView=(CSdPlayerView *)pChild->GetActiveView()))
	{
		pCmdUI->Enable(FALSE);
	}
	else if (!(pDoc = pView->GetDocument()))
	{
		pCmdUI->Enable(FALSE);
	}
	else if (!pDoc->m_undo_flag)
	{
		pCmdUI->Enable(FALSE);
	}
	else
	{
		pCmdUI->Enable(TRUE);
	}
	
}

void CMainFrame::OnClose() 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください

	// 印刷プレビューをXで閉じる時この関数が呼ばれてしまう。
	// 下記のコードを入れておくとreturnする。
	if (m_lpfnCloseProc != NULL && !(*m_lpfnCloseProc)(this))
	{
		//AfxMessageBox("preview close");
		return;
	}

	CRect rect;
	WINDOWPLACEMENT wndpl;
	wndpl.length=sizeof(wndpl);

	// save position of mainframe
	GetWindowPlacement(&wndpl);
	AfxGetApp()->WriteProfileInt("position","x",wndpl.rcNormalPosition.left);
	AfxGetApp()->WriteProfileInt("position","y",wndpl.rcNormalPosition.top);
	AfxGetApp()->WriteProfileInt("position","w",wndpl.rcNormalPosition.right-wndpl.rcNormalPosition.left);
	AfxGetApp()->WriteProfileInt("position","h",wndpl.rcNormalPosition.bottom-wndpl.rcNormalPosition.top);

	// save position and state of time display window 
	m_wndDlgBar.m_pwndTimeDisp->GetWindowRect(&rect);
	AfxGetApp()->WriteProfileInt("TimeDisp","x",rect.left);
	AfxGetApp()->WriteProfileInt("TimeDisp","y",rect.top);
	AfxGetApp()->WriteProfileInt("TimeDisp","w",rect.Width());
	AfxGetApp()->WriteProfileInt("TimeDisp","h",rect.Height());

	int iVisible = ((m_wndDlgBar.m_pwndTimeDisp->GetStyle() & WS_VISIBLE) != 0);
	AfxGetApp()->WriteProfileInt("TimeDisp","on",iVisible);

	// save state of child frame
	CMDIChildWnd *pChild=(CMDIChildWnd *)GetActiveFrame();
	if (pChild!=(CMDIChildWnd *)this)
	{
		pChild->GetWindowPlacement(&wndpl);
		AfxGetApp()->WriteProfileInt("child","w",wndpl.rcNormalPosition.right-wndpl.rcNormalPosition.left);
		AfxGetApp()->WriteProfileInt("child","h",wndpl.rcNormalPosition.bottom-wndpl.rcNormalPosition.top);
		AfxGetApp()->WriteProfileInt("child","sw",wndpl.showCmd);
	}
	else
	{
		AfxGetApp()->WriteProfileInt("child","sw",SW_SHOWNORMAL);
	}

	// save control panel no
	AfxGetApp()->WriteProfileInt("CPanel","no",panel_no);

	// save view font
	LOGFONT lf;
	if (m_view_font)
	{
		m_view_font->GetLogFont(&lf);
		AfxGetApp()->WriteProfileInt("ViewFont","Height",lf.lfHeight);
		AfxGetApp()->WriteProfileInt("ViewFont","Width",lf.lfWidth);
		AfxGetApp()->WriteProfileInt("ViewFont","Escapement",lf.lfEscapement);
		AfxGetApp()->WriteProfileInt("ViewFont","Orientation",lf.lfOrientation);
		AfxGetApp()->WriteProfileInt("ViewFont","Weight",lf.lfWeight);
		AfxGetApp()->WriteProfileInt("ViewFont","Italic",lf.lfItalic);
		AfxGetApp()->WriteProfileInt("ViewFont","Underline",lf.lfUnderline);
		AfxGetApp()->WriteProfileInt("ViewFont","StrikeOut",lf.lfStrikeOut);
		AfxGetApp()->WriteProfileInt("ViewFont","CharSet",lf.lfCharSet);
		AfxGetApp()->WriteProfileInt("ViewFont","OutPrecision",lf.lfOutPrecision);
		AfxGetApp()->WriteProfileInt("ViewFont","ClipPrecision",lf.lfClipPrecision);
		AfxGetApp()->WriteProfileInt("ViewFont","Quality",lf.lfQuality);
		AfxGetApp()->WriteProfileInt("ViewFont","PitchAndFamily",lf.lfPitchAndFamily);
		AfxGetApp()->WriteProfileString("ViewFont","FaceName",lf.lfFaceName);
	}

	CString str;
	int i;
	// save view column order
	OnSaveColumnOrder();
	str="";
	for (i=0 ; i<SDP_DATABASE_ITEMS ; i++)
	{
		CString str1;
		str1.Format("%d",m_view_column_order[i]);
		if (str!="")	{str+=",";}
		str+=str1;
	}
	AfxGetApp()->WriteProfileString("ViewColumn","ColumnOrder",str);

	// save view column width
	OnSaveColumnWidth();
	str="";
	for (i=0 ; i<SDP_DATABASE_ITEMS ; i++)
	{
		CString str1;
		str1.Format("%d",m_view_column_width[i]);
		if (str!="")	{str+=",";}
		str+=str1;
	}
	AfxGetApp()->WriteProfileString("ViewColumn","ColumnWidth",str);

//	m_wndDlgBar.f_DestroySubWindow();
	CMDIFrameWnd::OnClose();
}

void CMainFrame::OnWinStdSize() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	WINDOWPLACEMENT wndpl;
	wndpl.length=sizeof(wndpl);

	// main frame
	GetWindowPlacement(&wndpl);
	ModifyStyle(WS_MAXIMIZE,0,0);
	SetWindowPos(&wndTop,wndpl.rcNormalPosition.left,wndpl.rcNormalPosition.top,cx0,cy0,SWP_NOZORDER);

	// time display
	m_wndDlgBar.m_pwndTimeDisp->SetWindowPos(&wndTopMost,0,0,80,48,SWP_NOMOVE|SWP_NOZORDER);

	// child frame
	CChildFrame *pChild,*pChild0;
	pChild0=(CChildFrame *)GetActiveFrame();
	int x=0,y=0;
	pChild=pChild0;
	if (pChild!=(CChildFrame *)this)
	{
		do
		{
			MDIRestore(pChild);
			pChild->SetWindowPos(&wndTop,x,y,pChild->cx0,pChild->cy0,SWP_NOZORDER);
			x+=20;
			y+=15;
			MDINext();
			pChild=(CChildFrame *)GetActiveFrame();
		}while(pChild!=pChild0 && pChild!=(CChildFrame *)this);
	}
	AfxGetApp()->WriteProfileInt("child","w",pChild->cx0);
	AfxGetApp()->WriteProfileInt("child","h",pChild->cy0);
}

void CMainFrame::OnWinFlatSize() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	WINDOWPLACEMENT wndpl;
	wndpl.length=sizeof(wndpl);

	// main frame
	GetWindowPlacement(&wndpl);
	ModifyStyle(WS_MAXIMIZE,0,0);
	SetWindowPos(&wndTop,wndpl.rcNormalPosition.left,wndpl.rcNormalPosition.top,cx1,cy1,SWP_NOZORDER);

	// time display
	m_wndDlgBar.m_pwndTimeDisp->SetWindowPos(&wndTopMost,0,0,80,48,SWP_NOMOVE|SWP_NOZORDER);

	// child frame
	CChildFrame *pChild,*pChild0;
	pChild0=(CChildFrame *)GetActiveFrame();
	int x=0,y=0;
	pChild=pChild0;
	if (pChild!=(CChildFrame *)this)
	{
		do
		{
			MDIRestore(pChild);
			pChild->SetWindowPos(&wndTop,x,y,pChild->cx1,pChild->cy1,SWP_NOZORDER);
			x+=20;
			y+=15;
			MDINext();
			pChild=(CChildFrame *)GetActiveFrame();
		}while(pChild!=pChild0 && pChild!=(CChildFrame *)this);
	}
	AfxGetApp()->WriteProfileInt("child","w",pChild->cx1);
	AfxGetApp()->WriteProfileInt("child","h",pChild->cy1);
}

void CMainFrame::OnCPanel(UINT nCommandID) 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	f_ChangeCPanel(nCommandID-ID_CPanel0);
}

void CMainFrame::OnUpdateCPanel(CCmdUI* pCmdUI) 
{
	// TODO: この位置に command update UI ハンドラ用のコードを追加してください
	if ((int)(pCmdUI->m_nID - ID_CPanel0) == panel_no)
	{
		pCmdUI->SetCheck(TRUE);
	}
	else
	{
		pCmdUI->SetCheck(FALSE);
	}
}

void CMainFrame::OnSetFont() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	LOGFONT lf;

	CSdPlayerView *current_view=f_GetView();
	if (current_view)
	{
		current_view->GetFont()->GetLogFont(&lf);
	}

	CFontDialog fd(&lf);
	fd.m_cf.Flags&=~CF_EFFECTS;
	if (fd.DoModal()!=IDOK)
	{
		return;
	}
	fd.GetCurrentFont(&lf);

	if (m_view_font==NULL)
	{
		m_view_font=new CFont;
	}
	else
	{
		m_view_font->DeleteObject();
	}

	m_view_font->CreateFontIndirect(&lf);
	
	CSdPlayerView *view=current_view;
	do
	{
		view->f_SetFont();
		view->f_SetColumnWidth();
		MDINext();
		view=f_GetView();
	}while(view!=current_view);
}

void CMainFrame::OnSaveColumnOrder() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerView *current_view=f_GetView();
	if (!current_view)	{return;}

	current_view->f_GetColumnOrder(m_view_column_order);
	CSdPlayerView *view=current_view;
	do
	{
		view->f_SetColumnOrder();
		MDINext();
		view=f_GetView();
	}while(view!=current_view);
}

void CMainFrame::OnResetColumnOrder() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	for (int i=0 ; i<SDP_DATABASE_ITEMS ; i++)
	{
		m_view_column_order[i]=i;
	}

	CSdPlayerView *current_view=f_GetView();
	if (!current_view)	{return;}

	CSdPlayerView *view=current_view;
	do
	{
		view->f_SetColumnOrder();
		MDINext();
		view=f_GetView();
	}while(view!=current_view);
}

void CMainFrame::OnSaveColumnWidth() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerView *current_view=f_GetView();
	if (!current_view)	{return;}

	current_view->f_GetColumnWidth(m_view_column_width);
	CSdPlayerView *view=current_view;
	do
	{
		view->f_SetColumnWidth();
		MDINext();
		view=f_GetView();
	}while(view!=current_view);
	
}

void CMainFrame::OnResetColumnWidth() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	f_InitColumnWidth();

	CSdPlayerView *current_view=f_GetView();
	if (!current_view)	{return;}

	CSdPlayerView *view=current_view;
	do
	{
		view->f_SetColumnWidth();
		MDINext();
		view=f_GetView();
	}while(view!=current_view);
}


void CMainFrame::OnSetOptions() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CSdPlayerApp *app=(CSdPlayerApp*)AfxGetApp();
	CPropertySheet ps;
	CSdpOptProp01 op01;
	CSdpOptProp02 op02;

	ps.AddPage(&op02);
	ps.AddPage(&op01);

	op01.profile=&(app->m_profile);
	op02.profile=&(app->m_profile);
	if (ps.DoModal()==IDOK)
	{
		f_GetDocument()->UpdateAllViews(NULL,0);
	}
}

void CMainFrame::OnEditFind() 
{
	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
	CFindReplaceDialog *frd=new CFindReplaceDialog();
	frd->Create(TRUE,m_find_str,NULL,FR_DOWN,this);
}

LONG CMainFrame::OnFindReplace(WPARAM wParam, LPARAM lParam)
{
	CFindReplaceDialog *frd=CFindReplaceDialog::GetNotifier(lParam);

	m_find_str=frd->GetFindString();
	if (frd->IsTerminating())
	{
		frd->DestroyWindow();
		return 0;
	}

	int n;
	if (frd->SearchDown())
	{
		n=1;
	}
	else
	{
		n=-1;
	}

	CSdPlayerView *pView=f_GetView();
	if (!pView)
	{
		return 0;
	}
	
	CListCtrl& refCtrl = pView->GetListCtrl();
	int i0=refCtrl.GetSelectionMark();
	int max=refCtrl.GetItemCount();
	int i;
	BOOL found=0;
	CString str0,str1;
	str0=m_find_str;
	if (!frd->MatchCase())
	{
		str0.MakeLower();
	}
	if (frd->MatchWholeWord())
	{
		str0.TrimLeft();
		str0.TrimRight();
		str0=" "+str0+" ";
	}

	if (i0<0)
	{
		if (n>0)	{i0=max-1;}
		else		{i0=0;}
	}

	for (i=i0+n ; ; i+=n)
	{
		if (i>=max)	{i=0;}
		if (i<0)	{i=max-1;}

		str1=refCtrl.GetItemText(i,0);
		if (!frd->MatchCase())
		{
			str1.MakeLower();
		}
		if (frd->MatchWholeWord())
		{
			str1.TrimLeft();
			str1.TrimRight();
			str1=" "+str1+" ";
		}

		if (str1.Find(str0)>=0)
		{
			found=1;
			break;
		}
		
		if (i==i0)
		{
			break;
		}
	}
	
	if (found)
	{
		refCtrl.SetItemState(i0,0,LVIS_SELECTED|LVIS_FOCUSED);	
		refCtrl.SetSelectionMark(i);
		refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
		refCtrl.EnsureVisible(i,TRUE);
		str1=refCtrl.GetItemText(i,0)+"\n  "+
			refCtrl.GetItemText(i,1)+" "+refCtrl.GetItemText(i,2)+"\n  "+
			refCtrl.GetItemText(i,3)
			+"\n\nFind Next ?";
		if (AfxMessageBox(str1,MB_ICONINFORMATION|MB_YESNO)==IDYES)
		{
			PostMessage(WM_FINDREPLACE,wParam,lParam);
		}
	}
	else
	{
		AfxMessageBox("*** not found ***");
	}
	return 0;
}
