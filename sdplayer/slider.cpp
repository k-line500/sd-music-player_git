// Slider.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "Slider.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSlider
IMPLEMENT_DYNAMIC(CSlider, CSliderCtrl)

CSlider::CSlider()
{
}

CSlider::~CSlider()
{
}


BEGIN_MESSAGE_MAP(CSlider, CSliderCtrl)
	//{{AFX_MSG_MAP(CSlider)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSlider メッセージ ハンドラ

void CSlider::OnPaint() 
{
	CSliderCtrl::OnPaint();
	if (GetFocus()==this)
	{
		CDC *dc=GetDC();
		RECT rect;
		CBrush br;
		br.CreateSolidBrush(RGB(255,0,0));
		GetClientRect(&rect);
		dc->FrameRect(&rect,&br);
		ReleaseDC(dc);
	}
	
//	CPaintDC dc(this); // 描画用のデバイス コンテキスト
	
	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください
	
	// 描画用メッセージとして CSliderCtrl::OnPaint() を呼び出してはいけません
}
