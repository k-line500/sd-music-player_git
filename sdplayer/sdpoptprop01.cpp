// SdpOptProp01.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "SdpOptProp01.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp01 プロパティ ページ

IMPLEMENT_DYNCREATE(CSdpOptProp01, CPropertyPage)

CSdpOptProp01::CSdpOptProp01() : CPropertyPage(CSdpOptProp01::IDD)
{
	//{{AFX_DATA_INIT(CSdpOptProp01)
	//}}AFX_DATA_INIT
}

CSdpOptProp01::~CSdpOptProp01()
{
}

void CSdpOptProp01::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSdpOptProp01)
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_UnDispRep, profile->opt_undisp_rep);

}


BEGIN_MESSAGE_MAP(CSdpOptProp01, CPropertyPage)
	//{{AFX_MSG_MAP(CSdpOptProp01)
		// メモ: ClassWizard はこの位置に DDX および DDV の呼び出しコードを追加します。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp01 メッセージ ハンドラ
