// ChildFrm.cpp : CChildFrame クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "SdPlayer.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnUpdateViewStyles)
	ON_COMMAND_RANGE(AFX_ID_VIEW_MINIMUM, AFX_ID_VIEW_MAXIMUM, OnViewStyle)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame クラスの構築/消滅

CChildFrame::CChildFrame()
{
	// TODO: メンバ初期化コードをこの位置に追加してください。
	cx0=430;
	cy0=130;
	cx1=770;
	cy1=220;
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs の設定を行って、Window クラスまたは
	//       スタイルを変更してください。

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.style = WS_CHILD | WS_VISIBLE | WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU
		/*| FWS_ADDTOTITLE*/ | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX
		/*| WS_MAXIMIZE*/ ;
//	cs.cx=430;
//	cs.cy=130;
//	cs.cx=cx0;
//	cs.cy=cy0;
	cs.cx=AfxGetApp()->GetProfileInt("child","w",cx0);
	cs.cy=AfxGetApp()->GetProfileInt("child","h",cy0);

	if (AfxGetApp()->GetProfileInt("child","sw",SW_SHOWNORMAL)==SW_SHOWMAXIMIZED)
	{
		cs.style|=WS_MAXIMIZE;
	}


	return TRUE;
}

void CChildFrame::ActivateFrame(int nCmdShow)
{
	// TODO: この関数を修正してフレームがアクティブにされる方法を変更してください。

//	nCmdShow = SW_SHOWMAXIMIZED;
	CMDIChildWnd::ActivateFrame(nCmdShow);
}


/////////////////////////////////////////////////////////////////////////////
// CChildFrame クラスの診断

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame クラスのメッセージハンドラ

void CChildFrame::OnUpdateViewStyles(CCmdUI* pCmdUI)
{
	// TODO: このコードを変更・拡張して、 View メニューの選択を処理する
	// ようにしてください。

	CView* pView = this->GetActiveView(); 

	// もし右側のペインが作成されないかビューではないなら、
	// この範囲でコマンドを無効にします

	if (pView == NULL)
		pCmdUI->Enable(FALSE);
	else
	{
		DWORD dwStyle = pView->GetStyle() & LVS_TYPEMASK;

		// もしコマンドが ID_VIEW_LINEUP ならば、 LVS_ICON or LVS_SMALLICON 
		// モードになった場合だけ、このコマンドを有効にします

//		if (pCmdUI->m_nID == ID_VIEW_LINEUP)
//		{
//			if (dwStyle == LVS_ICON || dwStyle == LVS_SMALLICON)
//				pCmdUI->Enable();
//			else
//				pCmdUI->Enable(FALSE);
//		}
//		else
//		{
			// そうでなければ、ドットを使ってビューのスタイルを反映します
			pCmdUI->Enable();
			BOOL bChecked = FALSE;

			switch (pCmdUI->m_nID)
			{
			case ID_VIEW_DETAILS:
				bChecked = (dwStyle == LVS_REPORT);
				break;
/*
			case ID_VIEW_SMALLICON:
				bChecked = (dwStyle == LVS_SMALLICON);
				break;

			case ID_VIEW_LARGEICON:
				bChecked = (dwStyle == LVS_ICON);
				break;
*/
			case ID_VIEW_LIST:
				bChecked = (dwStyle == LVS_LIST);
				break;

			default:
				bChecked = FALSE;
				break;
			}

			pCmdUI->SetRadio(bChecked ? 1 : 0);
		}
//	}
}


void CChildFrame::OnViewStyle(UINT nCommandID)
{
	// TODO: このコードを変更・拡張して、 View メニューの選択を処理する
	// ようにしてください。
	CView* pView = this->GetActiveView(); 

	// もし右側のペインが作成されて CSdPlayerView ならば 、
	// メニュー コマンドを処理します
	if (pView != NULL)
	{
		DWORD dwStyle = -1;

		switch (nCommandID)
		{
/*		case ID_VIEW_LINEUP:
			{
				// リスト コントロールのアイコンをグリッド位置に合わせます
				CListCtrl& refListCtrl = pView->GetListCtrl();
				refListCtrl.Arrange(LVA_SNAPTOGRID);
			}
			break;
*/
		// 他のコマンドはリスト コントロールのスタイルを変更します
		case ID_VIEW_DETAILS:
			dwStyle = LVS_REPORT;
			break;
/*
		case ID_VIEW_SMALLICON:
			dwStyle = LVS_SMALLICON;
			break;

		case ID_VIEW_LARGEICON:
			dwStyle = LVS_ICON;
			break;
*/
		case ID_VIEW_LIST:
			dwStyle = LVS_LIST;
			break;
		}

		// スタイル変更; ウィンドウは自動的に再描画します
		if (dwStyle != -1)
			pView->ModifyStyle(LVS_TYPEMASK, dwStyle);
	}
}

//DEL void CChildFrame::OnWinStdSize() 
//DEL {
//DEL 	// TODO: この位置にコマンド ハンドラ用のコードを追加してください
//DEL 	SetWindowPos(&wndTop,0,0,cx0,cy0,SWP_NOMOVE|SWP_NOZORDER);
//DEL }
