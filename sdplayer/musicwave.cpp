// MusicWave.cpp: CMusicWave クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SdPlayer.h"
#include "MusicWave.h"
#include "Global.h"
#include "Dsp.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

CMusicWave::CMusicWave()
{
	m_object_type=0;	// wave data
	m_header_num=6;		// must be >=2
	m_header_size=1;  // 1 sec
	m_wh=NULL;
	m_dsp=new CDSP;
	m_dsp->m_wave=this;
	m_play_rate=1.0;
	m_tempo_change_rate=1.0;
	m_rep_mix_buf_size=2; // 2 sec
	m_rep_mix_buf=NULL;
	m_rep_mix_len=0;
	m_rep_mix_pos=0xFFFFFFFF;

//	m_open_flag=CALLBACK_WINDOW;
}

CMusicWave::~CMusicWave()
{
	f_UnLoad();
	delete m_dsp;
	f_SetStatusText(0,"CMusicWave deleted");
//	Sleep(1000);
//	AfxMessageBox("CMusicWave deleted");
}

//////////////////////////////////////////////////////////////////////
// WAVE file 読み込み
//////////////////////////////////////////////////////////////////////

LPCSTR CMusicWave::f_Wave_OpenFile()
{
    MMIOINFO                mmioinfo;
    MMCKINFO                ckRIFF;
    MMCKINFO                ckFMT;
    MMCKINFO                ckDATA;
	LPCSTR filename=m_filename;
 
    memset(&mmioinfo, 0, sizeof(MMIOINFO));
    hmmio = mmioOpen((LPSTR)filename, &mmioinfo, MMIO_READ|MMIO_ALLOCBUF);
    if (hmmio==(HMMIO)NULL)
    {
		return "file open error";
    }
	
	memset(&ckRIFF, 0, sizeof(ckRIFF));
	if (0 != mmioDescend(hmmio, &ckRIFF, NULL, MMIO_FINDRIFF) ||
		ckRIFF.fccType != mmioFOURCC('W','A','V','E'))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "WAVE format error";
	}

	ckFMT.ckid = mmioFOURCC('f','m','t',' ');
	if (0 != mmioDescend(hmmio, &ckFMT, &ckRIFF, MMIO_FINDCHUNK))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "format chunk error";
	}

	if ( ckFMT.cksize > sizeof(WAVEFORMATEX) )
	{
		// cksize = 16 or 18,  sizeof WAVEFORMATEX = 18
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "format chunk size error";
	}

	memset(&wfmtex, 0, sizeof(WAVEFORMATEX));
	if (ckFMT.cksize != (DWORD)mmioRead(hmmio, (HPSTR)&wfmtex, ckFMT.cksize))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "format chunk read error";
	}

	if ( wfmtex.wFormatTag != WAVE_FORMAT_PCM )
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "not PCM format";
	}

	ckDATA.ckid = mmioFOURCC('d','a','t','a');
	if (0 != mmioDescend(hmmio, &ckDATA, &ckRIFF, MMIO_FINDCHUNK))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "data chunk error";
	}

	m_WaveDataSize=ckDATA.cksize;
	m_HeaderSize=ckDATA.dwDataOffset;
	wfmtex.cbSize = 0 ;

//	return "success";
	return "";
}

void CMusicWave::f_Wave_CloseFile()
{
	mmioClose(hmmio,0);
	hmmio = (HMMIO)NULL;
	m_WaveDataSize=0;
	m_HeaderSize=0;
	memset( &wfmtex , 0, sizeof(WAVEFORMATEX) );
}

DWORD CMusicWave::f_ReadData(LPSTR buf, DWORD from, DWORD len)
{
	int mode;
	if (!m_stat_playing)	{mode=2;}
	else if (m_nbuf>=2)		{mode=2;}
	else if (m_nbuf==1)		{mode=1;}
	else					{mode=0;}

//	return m_dsp->f_ChangeTempo(buf,from,len,m_tempo_change_rate,m_play_rate,mode);

	if ( m_dsp_on &&
		(m_tempo_change_rate>1.0005 || m_tempo_change_rate<0.9995))
	{
		return m_dsp->f_ChangeTempo(buf,from,len,m_tempo_change_rate,m_play_rate,mode);
	}
	return f_Wave_ReadData(buf, from, len);
}

DWORD CMusicWave::f_Wave_ReadData(LPSTR buf, DWORD from, DWORD len)
{
	mmioSeek(hmmio, m_HeaderSize+from*m_sample_bytes, SEEK_SET);
	len=(DWORD)mmioRead(hmmio, buf, len*m_sample_bytes)/m_sample_bytes;
	return f_Wave_MixData(buf,from,len);
}

int CMusicWave::f_Wave_Rewind()
{
//	mmioSeek(hmmio, m_HeaderSize, SEEK_SET);
	m_dsp->f_Rewind();
	return 1;
}

DWORD CMusicWave::f_Wave_MixData(LPSTR buf, DWORD from, DWORD len)
{
	int pos=from-m_rep_mix_pos;
	if (m_stat_repeat && m_rep_mix_len>0 && pos>=0)
	{
		LPSTR mix_buf=m_rep_mix_buf+m_sample_bytes*pos;
		for (int i=0 ; i<(int)len && i<(int)(m_rep_mix_len-pos) ; i++)
		{
			double c0=pos + i;
			double c1=m_rep_mix_len - c0 ;
			if (m_sample_bytes/m_nch==2) // 16bit sample
			{
				for (int j=0 ; j<m_nch ; j++)
				{
					double input=*((short*)buf);
					double mix=*((short*)mix_buf);
					double output=(c0*mix+c1*input) / m_rep_mix_len;
					*((short*)buf)=(short)output;
					buf+=2;
					mix_buf+=2;
				}
			}
			else if (m_sample_bytes/m_nch==1) // 8bit sample
			{
				for (int j=0 ; j<m_nch ; j++)
				{
					double input=*((char*)buf);
					double mix=*((char*)mix_buf);
					double output=(c0*mix+c1*input) / m_rep_mix_len;
					*((char*)buf)=(char)output;
					buf+=1;
					mix_buf+=1;
				}
			}
		}
	}
	return len;
}

//////////////////////////////////////////////////////////////////////
// WAVE データ演奏
//////////////////////////////////////////////////////////////////////
int CMusicWave::f_UnLoad()
{
	// close wave output device if open
	f_Close();

	// free repeat mix buffer
	SdpFreeBuffer(m_rep_mix_buf);
	m_rep_mix_buf=NULL;
	m_rep_mix_len=0;
	m_rep_mix_pos=0xFFFFFFFF;

	// free wave buffer
	if (m_wh!=NULL)
	{
		for (int i=0 ; i<m_header_num ; i++)
		{
			SdpFreeBuffer(m_wh[i].lpData);
		}
		SdpFreeBuffer((LPSTR)m_wh);
		m_wh=NULL;
	}
	m_nbuf=0;
	
	m_dsp->f_Free();
	m_dsp_on=0;

	f_Wave_CloseFile();

	// initialize variable
	m_sampling_rate=0;//44100;
	m_nch=0;//2;
	m_sample_bits=0; //16;
	m_sample_bytes=0;//4;
	m_nsample=0;

	m_length=0.0;
	m_start=0.0;
	m_end=m_length;
	m_return_mark=m_start;
	m_repeat_mark=m_end;
	m_play_rate=1.0;
	m_tempo_change_rate=1.0;

	m_music_info=_T("");

	m_stat_load=0;
	m_status_text0="not loaded";
	m_status_text1="";
	f_SetStatusText();
	return 1;
}

int CMusicWave::f_Load()
{
	f_UnLoad();

	// alloc wave buffer and load new wave file
	m_status_text0="loading ...";
	m_status_text1=m_filename;
	f_SetStatusText();

	CString cstr=f_Wave_OpenFile();
	if (cstr!="")
	{
		cstr="loading failed ("+cstr+")";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	m_sample_bits=wfmtex.wBitsPerSample;
	m_sample_bytes=wfmtex.nBlockAlign;
	m_nsample=m_WaveDataSize/m_sample_bytes;
	m_sampling_rate=wfmtex.nSamplesPerSec;
	m_nch=wfmtex.nChannels;

	// alloc buffer
	if (0==m_dsp->f_Alloc(m_nch,m_sample_bits))
	{
		cstr="loading failed (dsp alloc error)";
		f_SetStatusText(0,cstr);
		m_dsp_on=0;
	}
	else
	{
		m_dsp_on=1;
	}

	if (NULL==(m_wh=(WAVEHDR*)SdpAllocBuffer(sizeof(WAVEHDR)*m_header_num)))
	{
		cstr="loading failed (wave header alloc error)";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	m_max_len=m_sampling_rate*m_header_size;
	for (int i=0 ; i<m_header_num ; i++)
	{
		m_wh[i].dwBufferLength=0;
		m_wh[i].dwBytesRecorded=0;
		m_wh[i].dwFlags=0;
		m_wh[i].dwLoops=0;
		m_wh[i].dwUser=0;
		if (NULL==(m_wh[i].lpData=SdpAllocBuffer(m_max_len*m_sample_bytes)))
		{
			while(i>0)
			{
				SdpFreeBuffer(m_wh[--i].lpData);
			}
			SdpFreeBuffer((LPSTR)m_wh);
			m_wh=NULL;
			cstr="loading failed (stream buffer alloc error)";
			f_SetStatusText(0,cstr);
			m_status_text0="not loaded";
			return 0;
		}
	}
	m_nbuf=0;

	// alloc mix buffer
	if (NULL==(m_rep_mix_buf=SdpAllocBuffer(m_sampling_rate*m_rep_mix_buf_size*m_sample_bytes)))
	{
		cstr="loading failed (repeat mix buffer alloc error)";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	m_length=(double)m_nsample/(double)m_sampling_rate;
	m_start=0.0;
	m_end=m_length;
	if (m_rep_start=="")
	{
		m_return_mark=m_start;
	}
	else
	{
		m_return_mark=SdpSetStringToTime(m_rep_start,2)/1000.0;
	}

	if (m_rep_end=="")
	{
		m_repeat_mark=m_end;
	}
	else
	{
		m_repeat_mark=SdpSetStringToTime(m_rep_end,2)/1000.0;
	}

	m_stat_load=1;
	f_SetSpeed();
	f_SetTempo();
	f_SetPitch();
	m_tempo_change_rate=m_tempo_change;
	m_play_rate=m_speed_change;
	f_SetStartTime(m_start-m_wait_time);
	f_SetEndTime(m_end);
	f_SetRepTime(m_return_mark,m_repeat_mark);
	f_SetMusicInfo();

	m_status_text0="loading OK";
	f_SetStatusText();
	return 1;
}

void CMusicWave::f_SetMusicInfo(void)
{
	if (m_nch=1)
	{
		m_music_info.Format(" -- PCM (mono) %dHz %dbit --",m_sampling_rate,m_sample_bits);
	}
	else if (m_nch=2)
	{
		m_music_info.Format(" -- PCM (stereo) %dHz %dbit --",m_sampling_rate,m_sample_bits);
	}
	else
	{
		m_music_info.Format(" -- PCM (%d ch) %dHz %dbit --",m_nch,m_sampling_rate,m_sample_bits);
	}
}

int CMusicWave::f_Close(void)
{
	if (m_stat_ready)
	{
		f_Reset();
		if (m_fadeout_vol>0)
		{
			f_SetVolume(m_fadeout_vol);
			m_fadeout_vol=0;
		}
		waveOutClose(m_hwo);
		m_stat_ready=0;
		f_SetStatusText(0,"wave closed");
		m_status_text0="not ready";
		m_output_device=_T("");
//		Sleep(1000);
	}
	// clear reapeat status
	m_repeat_num=0;
	m_repeat_mod=0.0;

	return 1;
}

int CMusicWave::f_Open(void)
{
	if (!m_stat_load) 
	{
		f_SetStatusText(0,"not loaded");
		return 0;
	}

	// close wave output device if open
	f_Close();
	m_play_rate=m_speed_change;
	m_tempo_change_rate=m_tempo_change;
	f_SetStartTime(m_start);
	f_SetEndTime(m_end);
	f_SetRepTime(m_return_mark,m_repeat_mark);

	// open wave output device
	switch (m_open_flag)
	{
	  case CALLBACK_FUNCTION:
		m_mmres=waveOutOpen(&m_hwo,WAVE_MAPPER,&wfmtex,(DWORD)waveOutProc,(DWORD)this,CALLBACK_FUNCTION);
		break;
	  case CALLBACK_WINDOW:
		m_mmres=waveOutOpen(&m_hwo,WAVE_MAPPER,&wfmtex,(DWORD)this->m_hWnd,(DWORD)this,CALLBACK_WINDOW);
		break;
	  default:
		f_SetStatusText(0,"waveOutOpen flag is invalid");
		return 0;
		break;
	}
	if (m_mmres!=MMSYSERR_NOERROR )
	{
		f_SetStatusText(0,"waveOutOpen failed");
		return 0;
	}

	UINT id;
	WAVEOUTCAPS caps;
	waveOutGetID(m_hwo,&id);
	waveOutGetDevCaps(id,&caps,sizeof(WAVEOUTCAPS));
	m_output_device=caps.szPname;
	m_output_device_info=" "+m_output_device;

	m_stat_ready=1;
	m_status_text0="ready";
	f_SetStatusText();
	return 1;
}

int CMusicWave::f_Start(void)
{
	if (m_stat_play)
	{
		if (!m_stat_playing) waveOutRestart(m_hwo);
		m_stat_playing=1;
		m_status_text0="playing";
		f_SetStatusText();
		return 1;
	}
	else if (m_stat_ready)
	{
		// reset wave device
		f_Reset();

		int i;
//		int nh;
		DWORD max_len=m_max_len;
		m_stat_playing=0;
		waveOutPause(m_hwo);
		while(m_stat_preparing)	{Sleep(0);}
		m_stat_preparing=1;
		for (i=0,m_nbuf=0 /*nh=0*/ ; i<m_header_num ; i++)
		{
//			PostMessage(MM_WOM_DONE,(WPARAM)m_hwo,(LPARAM)&m_wh[i]);
			if (i<m_header_num-1)	{m_max_len=10;}
			else					{m_max_len=max_len;}

			if (f_setWaveHeader(&m_wh[i]))
			{
				if (MMSYSERR_NOERROR!=waveOutPrepareHeader(m_hwo,&m_wh[i],sizeof(WAVEHDR)))
				{
					f_SetStatusText(0,"waveOutPrepareHeader failed");
				}
				else if (MMSYSERR_NOERROR!=waveOutWrite(m_hwo,&m_wh[i],sizeof(WAVEHDR)))
				{
					f_SetStatusText(0,"waveOutWrite failed");
				}
				else
				{
					m_nbuf++; // nh++;
				}
			}
		}
		m_stat_preparing=0;

		if (m_nbuf>0)
		{
			m_stat_play=1;
			m_stat_playing=1;
			waveOutRestart(m_hwo);
			m_status_text0="playing";
			f_SetStatusText();
			return 1;
		}
		else
		{
			return 0;
		}

/*		for ( ; i<m_header_num ; i++)
		{
			while(m_stat_preparing)	{Sleep(0);}
			m_stat_preparing=1;
			if (f_setWaveHeader(&m_wh[i]))
			{
				m_mmres=waveOutPrepareHeader(m_hwo,&m_wh[i],sizeof(WAVEHDR));
				if (m_mmres==MMSYSERR_NOERROR )
				{
					m_mmres=waveOutWrite(m_hwo,&m_wh[i],sizeof(WAVEHDR));
					if (m_mmres==MMSYSERR_NOERROR )
					{
						m_nbuf++;
					}
				}
			}
			m_stat_preparing=0;
		}
*/
/*		waveOutPause(m_hwo);
		for (i=0 ; i<nwh ; i++)
		{
			m_mmres=waveOutWrite(m_hwo,&m_wh[i],sizeof(WAVEHDR));
			if (m_mmres!=MMSYSERR_NOERROR )
			{
				f_SetStatusText(0,"waveOutWrite failed");
				f_Reset();
				return 0;
			}
			m_nbuf++;
		}
*/		
/*		if (!m_stat_playing && m_nbuf>0)
		{
			m_stat_play=1;
			m_stat_playing=1;
			waveOutRestart(m_hwo);
			m_status_text0="playing";
			f_SetStatusText();
		}
		else if (m_nbuf==0)
		{
			return 0;
		}
*/
	}
//  else
	f_SetStatusText(0,"not ready");
	return 0;
}

int CMusicWave::f_Stop(void)
{
	if (m_stat_playing)
	{
		waveOutPause(m_hwo);
		m_stat_playing=0;
		m_status_text0="pause";
		f_SetStatusText();
		return 1;
	}
	else if (m_stat_ready)
	{
		if (f_GetTime()==0.0)
		{
			m_status_text0="ready";
			f_SetStatusText();
		}
		else
		{
			m_status_text0="stop";
			f_SetStatusText();
		}
		return 1;
	}
//	else
	f_SetStatusText(0,"not ready");
	return 0;
}

int CMusicWave::f_Reset(void)
{
	m_repeat_num=0;
	m_repeat_mod=0.0;
	if (m_stat_ready)
	{
		m_stat_play=0;
		m_stat_playing=0;
		while(m_stat_preparing)	{Sleep(0);}
		waveOutReset(m_hwo);

		switch (m_open_flag)
		{
		  case CALLBACK_FUNCTION:
			while(m_nbuf)	{Sleep(0);}
			break;
		  case CALLBACK_WINDOW:
			m_nbuf=0;
			break;
		  default:
			m_nbuf=0;
			break;
		}

		for (int i=0 ; i<m_header_num ; i++)
		{
			waveOutUnprepareHeader(m_hwo,&m_wh[i],sizeof(WAVEHDR));
			m_wh[i].dwUser=0;
		}
		m_status_text0="ready";
		f_SetStatusText();
		return 1;
	}
//	else
	f_SetStatusText(0,"not ready");
	return 0;
}

double CMusicWave::f_GetLength()
{
	return m_length;
}

double CMusicWave::f_GetTime()
{
	MMTIME mmt;
	mmt.wType=TIME_SAMPLES;

	if (m_stat_ready)
	{
		waveOutGetPosition(m_hwo,&mmt,sizeof(MMTIME));
	}
	else
	{
		return 0.0;
	}

	if (mmt.wType==TIME_SAMPLES)
	{
		double time;
		time=(double)mmt.u.sample/(double)m_sampling_rate*m_tempo_change_rate-(m_repeat_mark-m_return_mark)*m_repeat_num - m_repeat_mod + m_start;
		if (mmt.u.sample > 4000000000)
		{
			if ( m_stat_play && m_stat_playing)
			{
				f_Reset();
				f_SetStartTime(time);
				f_Start();
			}
			else
			{
				f_Reset();
				f_SetStartTime(time);
			}
		}
		return time;
	}
	else
	{
		return 0.0;
	}
}

int CMusicWave::f_SetStartTime(double start)
{
	if (m_stat_play)
	{
		return 0;
	}
	else if (m_stat_load)
	{
		SdpSetTimeToString(start,m_time,1);
		if (start<0.0)				{start=0.0;}
		else if (start>m_length)	{start=m_length;}
		m_next_pos=(DWORD)(m_sampling_rate*start/m_tempo_change_rate);
		m_start=start;
		f_Wave_Rewind();
		return 1;
	}
	return 0;
}

int CMusicWave::f_SetEndTime(double end)
{
	if (m_stat_load)
	{
		if (end<0.0)			{end=0.0;}
		else if (end>m_length)	{end=m_length;}
		m_final_pos=(DWORD)(m_sampling_rate*end/m_tempo_change_rate);
		m_end=end;
		return 1;
	}
	return 0;
}

int CMusicWave::f_SetRepTime(double start,double end)
{
	if (m_stat_load)
	{
		double min=1.0;

		if (start>m_length-min)		{start=m_length-min;}
		if (start<0.0)				{start=0.0;}
		if (end<start+min)			{end=start+min;}
		if (end>m_length)			{end=m_length;}

		m_ret_pos=(DWORD)(m_sampling_rate*start/m_tempo_change_rate);
		m_rep_pos=(DWORD)(m_sampling_rate*end/m_tempo_change_rate);
		m_return_mark=start;
		m_repeat_mark=end;

		double rep_mix=m_rep_mix;
		if (rep_mix>m_rep_mix_buf_size) {rep_mix=m_rep_mix_buf_size;}
		m_rep_mix_len=0;
		m_rep_mix_pos=0xFFFFFFFF;
		if (m_rep_mix_buf)
		{
			double v=start-rep_mix;
			if (v<0) {v=0.0;}
			m_rep_mix_len=f_Wave_ReadData(m_rep_mix_buf,(DWORD)(m_sampling_rate*v),(DWORD)(m_sampling_rate*(start-v)));
		}
		m_rep_mix_pos=(DWORD)(m_sampling_rate*end)-m_rep_mix_len;

		return 1;
	}
	return 0;
}

int CMusicWave::f_ChangeSpeed()
{
	double time;
	if (m_stat_ready)
	{
		if ( m_stat_play && m_stat_playing)
		{
			f_Stop();
			time=f_GetTime();
			if (!f_Open())	{return 0;}
			f_SetStartTime(time);
			return f_Start();
		}
		else
		{
			f_Stop();
			time=f_GetTime();
			if (!f_Open())	{return 0;}
			f_SetStartTime(time);
		}
	}
	return 1;
}

int CMusicWave::f_ChangeTempo()
{
	return f_ChangeSpeed();
}

int CMusicWave::f_ChangePitch()
{
	return f_ChangeTempo();
}

void CMusicWave::f_SetSpeed()
{
	if (!m_dsp_on)
	{
		m_tempo_change=1.0;
		m_pitch_change=1.0;
	}
	m_speed_change=(1.0+m_speed/1000.0)*m_pitch_change;

	if (m_speed_change<0.1)			{m_speed_change=0.1;}
	else if (m_speed_change>10.0)	{m_speed_change=10.0;}

	if (m_stat_load)
	{
		wfmtex.nSamplesPerSec= (DWORD)(m_sampling_rate*m_speed_change+0.5);
		m_speed_change=(double)wfmtex.nSamplesPerSec/(double)m_sampling_rate;
		wfmtex.nAvgBytesPerSec=wfmtex.nSamplesPerSec*wfmtex.nBlockAlign;
	}
	m_tempo_bpm=m_tempo_bpm0*m_speed_change*m_tempo_change;
}

void CMusicWave::f_SetTempo()
{
	if (m_dsp_on)
	{
		m_tempo_change=(1.0+m_tempo/1000.0)/m_pitch_change;
		m_tempo_bpm=m_tempo_bpm0*m_speed_change*m_tempo_change;
	}
	else
	{
		m_tempo_change=1.0;
	}
}

void CMusicWave::f_SetPitch()
{
	if (m_dsp_on)
	{
		double rate=m_speed_change/m_pitch_change;
		m_pitch_change=exp(m_pitch/60.0*log(2.0));
		if (m_stat_load)
		{
			m_pitch_change=(DWORD)(m_sampling_rate*rate*m_pitch_change+0.5)/(double)(m_sampling_rate*rate);
		}
		f_SetSpeed();
		f_SetTempo();
	}
	else
	{
		m_pitch_change=1.0;
	}
}

int CMusicWave::f_GetVolume(DWORD *vol)
{
	return waveOutGetVolume(m_hwo,vol);
}

int CMusicWave::f_SetVolume(DWORD vol)
{
	return waveOutSetVolume(m_hwo,vol);
}

//////////////////////////////////////////////////////////////////////
// wave header operation
//////////////////////////////////////////////////////////////////////
int CMusicWave::f_setWaveHeader(WAVEHDR *wh)
{
	
	DWORD end_pos;
	long next_len;

	if (wh->dwUser & 0x1)
	{
		m_repeat_num++;
		long mod=wh->dwUser>>2;
		if (wh->dwUser & 0x2)
		{
			mod=-mod;
		}
		m_repeat_mod+=(double)mod/(double)m_sampling_rate*m_tempo_change_rate;
	}
	wh->dwUser=0;

	if (m_stat_repeat)
	{
		end_pos=m_rep_pos;
	}
	else
	{
		end_pos=m_final_pos;
	}
	
	if (end_pos>(DWORD)(m_nsample/m_tempo_change_rate))
	{
		end_pos=(DWORD)(m_nsample/m_tempo_change_rate);
	}

	if (end_pos <= m_next_pos && m_stat_repeat)
	{
		next_len=(long)(m_sampling_rate*0.5);  // 0.5 sec
	}
	else if (end_pos < m_next_pos)
	{
		next_len=0;
		m_next_pos=end_pos;
	}
	else
	{
		next_len=end_pos - m_next_pos;
	}

	if (next_len>m_max_len)
	{
		next_len=m_max_len;
	}
	
	if (next_len>0)
	{
		next_len=f_ReadData(wh->lpData,m_next_pos,next_len);
		m_next_pos+=next_len;
	}

	wh->dwBufferLength=next_len*m_sample_bytes;
	wh->dwFlags=0;
	wh->dwUser=0;
	wh->dwBytesRecorded=0;

	if (m_stat_repeat)
	{
		if (m_next_pos>=m_rep_pos || next_len==0)
		{
			if (m_next_pos>=m_rep_pos)
			{
				wh->dwUser=(m_next_pos - m_rep_pos)*4+1;
			}
			else
			{
				wh->dwUser=(m_rep_pos - m_next_pos)*4+3;
			}

			if (next_len==0)
			{
				memset(wh->lpData,0,m_sample_bytes);
				wh->dwBufferLength=m_sample_bytes;
			}

			m_next_pos=m_ret_pos;
			if (m_final_pos<m_rep_pos)
			{
				m_stat_repeat=0;
			}
			f_Wave_Rewind();
		}
	}
	else if (next_len==0)
	{
	//	CString str;
	//	str.Format("done(hd%d %d) %d -> %d",wh-m_wh,next_len,m_next_pos-next_len,m_next_pos);
	//	f_SetStatusText(1,str);
	//	Sleep(1000);
		return 0;
	}
//	CString str;
//	str.Format("read(hd%d %d) %d -> %d",wh-m_wh,next_len,m_next_pos-next_len,m_next_pos);
//	f_SetStatusText(1,str);
//	Sleep(1000);
	return 1;
}

/*
int CMusicWave::f_setWaveHeader(WAVEHDR *wh)
{
	DWORD end_pos;
	int len,next_len;
	static int rep;
	static DWORD repm;

	if (rep)
	{
		m_repeat_num++;
		m_repeat_mod+=(double)(repm/m_sample_bytes)/(double)m_sampling_rate;
	}
	rep=0;

	if (m_stat_repeat)
	{
		end_pos=m_rep_pos;
	}
	else
	{
		end_pos=m_final_pos;
	}

	if (end_pos>m_DataSize)
	{
		end_pos=m_DataSize;
	}

	if ( m_next_pos >= end_pos )
	{
		if (m_stat_repeat)
		{
			rep=1;
			repm=m_next_pos - end_pos ;
			m_next_pos=m_ret_pos;
		}
		else
		{
			return 0;
		}
	}
	else if ( m_prev_pos < m_final_pos && m_next_pos >= m_final_pos )
	{
		return 0;
	}

	len=end_pos - m_next_pos;
	if ( len <=0 )
	{
		rep=0;
		return 0;
	}
	else if ( len <= m_std_len )
	{
		next_len= len ;
	}
	else if ( len <= 2*m_std_len )
	{
		next_len= len - m_std_len ;
	}
	else if ( len <= 3*m_std_len )
	{
		next_len= len - m_std_len - m_half_len ;
	}
	else
	{
		next_len=m_std_len ;
	}

	next_len=f_ReadData(wh->lpData,m_next_pos,next_len);
	wh->dwBufferLength=next_len;
	wh->dwFlags=0;
	wh->dwLoops=0;
	wh->dwUser=0;
	m_prev_pos=m_next_pos;
	m_next_pos+=next_len;
	return 1;
}
*/

//////////////////////////////////////////////////////////////////////
// waveOutProc
//////////////////////////////////////////////////////////////////////
void CMusicWave::f_OutProcDone(WPARAM wParam, LPARAM lParam)
{
	HWAVEOUT hwo=(HWAVEOUT)wParam;
	WAVEHDR *wh=(WAVEHDR*)lParam;
//	CString str;
//	str.Format("done(hd%d) %d",wh - m_wh,m_nbuf);
//	str.Format("playing (buf %d)",m_nbuf);
//	f_SetStatusText(0,str);

//	waveOutProc(hwo,WOM_DONE,(DWORD)this,(DWORD)lParam,0);
//	Sleep(1000);

	while(m_stat_preparing)	{Sleep(0);}
	m_stat_preparing=1;
	if ( m_stat_play )
	{
		waveOutUnprepareHeader(hwo,wh,sizeof(WAVEHDR));
		if ( f_setWaveHeader(wh) )
		{
			waveOutPrepareHeader(hwo,wh,sizeof(WAVEHDR));
			if (MMSYSERR_NOERROR==waveOutWrite(hwo,wh,sizeof(WAVEHDR)))
			{
				m_nbuf++;
			}
		}

		if (m_nbuf<=0)
		{
			m_stat_play=0;
			m_stat_playing=0;
		}
	}
	
	if (m_nbuf<0)
	{
		m_nbuf=0;
	}

//	str.Format("playing (buf %d)",m_nbuf);
//	f_SetStatusText(0,str);

	m_stat_preparing=0;
}

void CALLBACK waveOutProc
( HWAVEOUT hwo,      
   UINT uMsg,         
   DWORD dwInstance,  
   DWORD dwParam1,    
   DWORD dwParam2   )
{
//	WAVEHDR *wh;
	CMusicWave *wave;

 	switch (uMsg)
	{
		case WOM_OPEN :
			break;
		case WOM_CLOSE :
			break;
		case WOM_DONE :
//			wh=(WAVEHDR *)dwParam1;
			wave=(CMusicWave*)dwInstance;
			wave->m_nbuf--;
			if ( wave->m_stat_play )
			{
				wave->f_OutProcDone((WPARAM)hwo,(LPARAM)dwParam1);
				::PostMessage(wave->m_hWnd,MM_WOM_DONE,(WPARAM)hwo,(LPARAM)dwParam1);
			}

/*			while(wave->m_stat_preparing)	{Sleep(0);}
			wave->m_stat_preparing=1;
			if ( wave->m_stat_play )
			{
				waveOutUnprepareHeader(hwo,wh,sizeof(WAVEHDR));
				if ( wave->f_setWaveHeader(wh) )
				{
					waveOutPrepareHeader(hwo,wh,sizeof(WAVEHDR));
					if (MMSYSERR_NOERROR==waveOutWrite(hwo,wh,sizeof(WAVEHDR)))
					{
						wave->m_nbuf++;
					}
				}

				if (wave->m_nbuf<=0)
				{
					wave->m_stat_play=0;
					wave->m_stat_playing=0;
				}
			}
*/
			if (wave->m_nbuf<0)
			{
				wave->m_nbuf=0;
			}
//			wave->m_stat_preparing=0;
			break;
	  default:
			break;
	}
	return;
}
