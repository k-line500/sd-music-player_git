// SdPlayerDoc.h : CSdPlayerDoc クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_SDPLAYERDOC_H__D0499572_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPLAYERDOC_H__D0499572_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CSdPlayerDoc : public CDocument
{
protected: // シリアライズ機能のみから作成します。
	CSdPlayerDoc();
	DECLARE_DYNCREATE(CSdPlayerDoc)

// アトリビュート
public:

// オペレーション
public:

//オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSdPlayerDoc)
	public:
	virtual void Serialize(CArchive& ar);
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	void f_Undo(CListCtrl& refCtrl);
	void f_AddFile();
	void f_ReOpenFile();
	virtual ~CSdPlayerDoc();
	CString &f_GetCurDocDir();
	void f_AddItem(int i,CStringArray *sa);
	void f_DelItem(int i);
	void f_PathAbsToRel(int i);
	void f_PathRelToAbs(int i);
	void f_SetItemNo();
	void f_SortItems(int col);
	CDWordArray m_colwidth; // m_view_columnwidth of CMainFrame is used now instead of this
	CStringArray m_coltitle;
	int m_data_version;
	int m_subitems;
	CObArray m_music_list;
	CStringArray *m_stack;
	int m_undo_flag;
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	CString m_current_docdir;
	void f_SetColumns(void);
	//+++++ 03.02.27 kawamura add start
	BOOL f_CheckExt(CArchive& ar,char *ext);
	void f_WriteCsvFile(CArchive& ar);
	int f_ReadCsvFile(CArchive& ar);
	int f_scanf(char buff[], char data[][256]);
	BOOL DoSave(LPCTSTR lpszPathName,BOOL bReplace);
	//+++++ 03.02.27 kawamura add end
	

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CSdPlayerDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	// 生成された OLE ディスパッチ マップ関数
	//{{AFX_DISPATCH(CSdPlayerDoc)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPLAYERDOC_H__D0499572_B2AC_11D5_BC30_009027BFCD6A__INCLUDED_)
