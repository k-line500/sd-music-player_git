// SdpOptProp02.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "SdpOptProp02.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp02 プロパティ ページ

IMPLEMENT_DYNCREATE(CSdpOptProp02, CPropertyPage)

CSdpOptProp02::CSdpOptProp02() : CPropertyPage(CSdpOptProp02::IDD)
{
	//{{AFX_DATA_INIT(CSdpOptProp02)
	//}}AFX_DATA_INIT
}

CSdpOptProp02::~CSdpOptProp02()
{
}

void CSdpOptProp02::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSdpOptProp02)
	DDX_Control(pDX, IDC_SPIN1, m_spin_fadeout_len);
	DDX_Control(pDX, IDC_LIST1, m_list1);
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_RADIO1, profile->opt_cb_sel);
	DDX_Text(pDX, IDC_EDIT2, profile->opt_cb_usertext);

	DDX_Text(pDX, IDC_EDIT1, profile->opt_fadeout_len);
	DDV_MinMaxInt(pDX, profile->opt_fadeout_len, 0, 10);
}


BEGIN_MESSAGE_MAP(CSdpOptProp02, CPropertyPage)
	//{{AFX_MSG_MAP(CSdpOptProp02)
	ON_BN_CLICKED(IDC_BUTTON1, OnItemUp)
	ON_BN_CLICKED(IDC_BUTTON2, OnItemDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp02 メッセージ ハンドラ

BOOL CSdpOptProp02::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// TODO: この位置に初期化の補足処理を追加してください
	for (int i=0,max=profile->opt_cb_items.GetSize() ; i<max ; i++ )
	{
		m_list1.AddString(LPCTSTR(profile->opt_cb_items.GetAt(i))+2);
	}

	m_spin_fadeout_len.SetRange(0,10);

	UpdateData(FALSE);

	return TRUE;  // コントロールにフォーカスを設定しないとき、戻り値は TRUE となります
	              // 例外: OCX プロパティ ページの戻り値は FALSE となります
}

void CSdpOptProp02::OnItemUp() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	int pos=m_list1.GetCurSel();
	if (pos==LB_ERR || pos<=0) {return;}
	CString str,str2;
	m_list1.GetText(pos,str);
	str2=profile->opt_cb_items.GetAt(pos);
	profile->opt_cb_items.RemoveAt(pos);
	m_list1.DeleteString(pos);
	pos--;
	pos=m_list1.InsertString(pos,str);
	profile->opt_cb_items.InsertAt(pos,str2);
	m_list1.SetCurSel(pos);
}

void CSdpOptProp02::OnItemDown() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	int pos=m_list1.GetCurSel();
	if (pos==LB_ERR) {return;}
	CString str,str2;
	m_list1.GetText(pos,str);
	str2=profile->opt_cb_items.GetAt(pos);
	profile->opt_cb_items.RemoveAt(pos);
	int max=m_list1.DeleteString(pos);
	pos++;
	if (pos>max){pos=max;}
	pos=m_list1.InsertString(pos,str);
	profile->opt_cb_items.InsertAt(pos,str2);
	m_list1.SetCurSel(pos);

}
