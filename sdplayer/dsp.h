// DSP.h: CDSP クラスのインターフェイス
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DSP_H__F3D81721_0A8D_11D6_BC30_009027BFCD6A__INCLUDED_)
#define AFX_DSP_H__F3D81721_0A8D_11D6_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDSP  
{
public:
	CDSP();
	virtual ~CDSP();
	
	class CMusicWave *m_wave;

	DWORD m_sample_bytes;
	DWORD m_nch;
	DWORD m_input_pos;
	LPSTR m_inputbuf;
	DWORD m_inputbuf_sample_num;
	DWORD m_inputbuf_len;
	LPSTR m_outputbuf;
	DWORD m_outputbuf_sample_num;
	DWORD m_outputbuf_len;
	LPSTR m_ovlbuf;
	DWORD m_ovlbuf_len;
	DWORD m_ovlbuf_sample_num;

	int f_Alloc(DWORD nch, DWORD bits);
	void f_Free();
	int f_Rewind();
	DWORD f_ChangeTempo(LPSTR buf, DWORD from, DWORD len, double tempo, double play_rate, int mode);
	int f_SeekOffset16(LPSTR ovlbuf, LPSTR inputbuf, DWORD ovl_num);
	int f_SeekOffset8(LPSTR ovlbuf, LPSTR inputbuf, DWORD ovl_num);
};

#endif // !defined(AFX_DSP_H__F3D81721_0A8D_11D6_BC30_009027BFCD6A__INCLUDED_)
