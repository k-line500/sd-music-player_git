#if !defined(AFX_SDPOPTPROP01_H__38E81081_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPOPTPROP01_H__38E81081_91C0_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SdpOptProp01.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp01 ダイアログ

class CSdpOptProp01 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSdpOptProp01)

// コンストラクション
public:
	CSdpOptProp01();
	~CSdpOptProp01();

// ダイアログ データ
	//{{AFX_DATA(CSdpOptProp01)
	enum { IDD = IDD_OptProp01 };
	//}}AFX_DATA

	CSdpProfile *profile;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。

	//{{AFX_VIRTUAL(CSdpOptProp01)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CSdpOptProp01)
		// メモ: ClassWizard はこの位置にメンバ関数を追加します。
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPOPTPROP01_H__38E81081_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
