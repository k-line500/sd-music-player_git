// SdpDataBox.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "SdPlayer.h"
#include "MainFrm.h"
#include "SdpDataBox.h"
#include "SdpDataProp01.h"
#include "Music.h"
#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdpDataBox ダイアログ


CSdpDataBox::CSdpDataBox(CWnd* pParent /*=NULL*/)
	: CDialog(CSdpDataBox::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSdpDataBox)
	m_tempo_in_file = _T("");
	m_relpath = FALSE;
	m_rep_mix = 0;
	//}}AFX_DATA_INIT
}

BOOL CSdpDataBox::OnInitDialog()
{
	CDialog::OnInitDialog();

//	UpdateData(TRUE);

	m_beatsperrep_updown.SetRange(0,10000);
	m_tempo_org_updown.SetRange(-1000,1000);
	m_waittime_updown.SetRange(-20,20);

	m_speed_slider.SetRange(-50,50,TRUE);
	m_pitch_slider.SetRange(-60,60,TRUE);
	m_tempo_slider.SetRange(-50,50,TRUE);
	m_speed_slider.SetPos(music->m_speed);
	m_pitch_slider.SetPos(music->m_pitch);
	m_tempo_slider.SetPos(music->m_tempo);

	m_repstart_updown.SetRange(-500,500);
	m_repstart_updown.SetPos(0);
	m_repstart_slider.SetRange(-500,500,TRUE);
	m_repstart_slider.SetPos(0);
	m_repstart_slider.SetTicFreq(50);
	m_repeat_start=SdpSetStringToTime(music->m_rep_start,2);

	m_repend_updown.SetRange(-500,500);
	m_repend_updown.SetPos(0);
	m_repend_slider.SetRange(-500,500,TRUE);
	m_repend_slider.SetPos(0);
	m_repend_slider.SetTicFreq(50);
	m_repeat_end=SdpSetStringToTime(music->m_rep_end,2);

	m_repboth_updown.SetRange(-500,500);
	m_repboth_updown.SetPos(0);
	m_repboth_slider.SetRange(-500,500,TRUE);
	m_repboth_slider.SetPos(0);
	m_repboth_slider.SetTicFreq(50);

	m_repmix_updown.SetRange(0,2000);

	m_time_slider.SetRange(0,180,TRUE);
	m_time_slider.SetTicFreq(10);

	if (music->m_dsp_on)
	{
		m_pitch_slider.EnableWindow(TRUE);
		m_tempo_slider.EnableWindow(TRUE);
	}
	else
	{
		m_pitch_slider.EnableWindow(FALSE);
		m_tempo_slider.EnableWindow(FALSE);
	}

	m_relpath=SdpPathIsRelative(music->m_filename);

	music->SendMessage(WM_TIMER,0);
 
	UpdateData(FALSE);

	if (music->m_filename!="")
	{
		OnLoad();
	}
	return TRUE;
}

void CSdpDataBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	if (!pDX->m_bSaveAndValidate)
	{
		m_rep_mix=(int)(music->m_rep_mix*1000);
	}

	//{{AFX_DATA_MAP(CSdpDataBox)
	DDX_Control(pDX, IDC_SPIN_RepMix, m_repmix_updown);
	DDX_Control(pDX, IDC_SPIN_RepBoth, m_repboth_updown);
	DDX_Control(pDX, IDC_SLIDER_RepBoth, m_repboth_slider);
	DDX_Control(pDX, IDC_SPIN_TempOrg, m_tempo_org_updown);
	DDX_Control(pDX, IDC_SPIN_WaitTime, m_waittime_updown);
	DDX_Control(pDX, IDC_SPIN_BeatsPerRep, m_beatsperrep_updown);
	DDX_Control(pDX, IDC_SPIN_RepEnd, m_repend_updown);
	DDX_Control(pDX, IDC_SLIDER_RepEnd, m_repend_slider);
	DDX_Control(pDX, IDC_SPIN_RepStart, m_repstart_updown);
	DDX_Control(pDX, IDC_SLIDER_RepStart, m_repstart_slider);
	DDX_Control(pDX, IDC_SLIDER_Time, m_time_slider);
	DDX_Control(pDX, IDC_SLIDER_Speed, m_speed_slider);
	DDX_Control(pDX, IDC_SLIDER_Pitch, m_pitch_slider);
	DDX_Control(pDX, IDC_SLIDER_Tempo, m_tempo_slider);
	DDX_Text(pDX, IDC_EDIT_TempoOrg2, m_tempo_in_file);
	DDX_Check(pDX, IDC_CHECK_RelPath, m_relpath);
	DDX_Text(pDX, IDC_EDIT_RepMix, m_rep_mix);
	DDV_MinMaxInt(pDX, m_rep_mix, 0, 2000);
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT_Title, music->m_title);
	DDX_Text(pDX, IDC_EDIT_File, music->m_filename);
	DDX_Text(pDX, IDC_EDIT_Category, music->m_category);
	DDX_Text(pDX, IDC_EDIT_Label, music->m_label);
	DDX_Text(pDX, IDC_EDIT_No, music->m_label_no);
	DDX_Radio(pDX, IDC_RADIO_Wave, music->m_data_type);

	DDX_Check(pDX, IDC_RepCheck, music->m_auto_repeat);
	DDX_Check(pDX, IDC_Repeat, music->m_stat_repeat);

	DDX_Text(pDX, IDC_EDIT_WaitTime, music->m_wait_time);
//	DDV_MinMaxInt(pDX, music->m_wait_time, -20, 20);

	DDX_Text(pDX, IDC_EDIT_Tempo2, music->m_tempo_bpm);
	DDX_Text(pDX, IDC_EDIT_Speed, music->m_speed_control);
	DDX_Text(pDX, IDC_EDIT_Pitch, music->m_pitch_control);
	DDX_Text(pDX, IDC_EDIT_Tempo, music->m_tempo_control);

	DDX_Text(pDX, IDC_EDIT_TempoOrg, music->m_tempo_org);
//	DDV_MinMaxDouble(pDX, music->m_tempo_org, 0, 1000);
	DDX_Text(pDX, IDC_EDIT_BeatsPerRep, music->m_beats_per_rep);
//	DDV_MinMaxInt(pDX, music->m_beats_per_rep, 0, 10000);

	DDX_Check(pDX, IDC_Play, music->m_play_on);
	DDX_Text(pDX, IDC_EDIT_Time, music->m_time);
	DDX_Text(pDX, IDC_EDIT_Length, music->m_music_len);

	DDX_Text(pDX, IDC_EDIT_RepStart, music->m_rep_start);
	DDX_Text(pDX, IDC_EDIT_RepEnd, music->m_rep_end);

	if (pDX->m_bSaveAndValidate)
	{
		music->m_rep_mix=(double)m_rep_mix/1000.0;
	}
}


BEGIN_MESSAGE_MAP(CSdpDataBox, CDialog)
	//{{AFX_MSG_MAP(CSdpDataBox)
	ON_BN_CLICKED(IDC_Add, OnAdd)
	ON_BN_CLICKED(IDC_Load, OnLoad)
	ON_BN_CLICKED(IDC_Play, OnPlay)
	ON_BN_CLICKED(IDC_Rewind, OnRewind)
	ON_BN_CLICKED(IDC_Repeat, OnRepeat)
	ON_BN_CLICKED(IDC_RepCheck, OnRepCheck)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_Set_RepStart, OnSetRepStart)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RepStart, OnDeltaposSPINRepStart)
	ON_BN_CLICKED(IDC_Test_RepStart, OnTestRepStart)
	ON_BN_CLICKED(IDC_Test_RepStart2, OnTestRepStart2)
	ON_BN_CLICKED(IDC_Set_RepEnd, OnSetRepEnd)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RepEnd, OnDeltaposSPINRepEnd)
	ON_BN_CLICKED(IDC_Test_RepEnd, OnTestRepEnd)
	ON_BN_CLICKED(IDC_Test_Loop, OnTestLoop)
	ON_BN_CLICKED(IDC_Set_BeatsPerRep, OnSetBeatsPerRep)
	ON_BN_CLICKED(IDC_BUTTON_File, OnBUTTONFile)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_Test_RepEnd2, OnTestRepEnd2)
	ON_BN_CLICKED(IDC_RADIO_Wave, OnRADIOWave)
	ON_BN_CLICKED(IDC_RADIO_MIDI, OnRadioMidi)
	ON_BN_CLICKED(IDC_RADIO_MPEG, OnRadioMpeg)
	ON_BN_CLICKED(IDC_Set_OrigBPM, OnSetOrigBPM)
	ON_BN_CLICKED(IDC_Set_RepLength, OnSetRepLength)
	ON_EN_CHANGE(IDC_EDIT_TempoOrg, OnChangeEDITTempoOrg)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_TempOrg, OnDeltaposSPINTempOrg)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_RepBoth, OnDeltaposSPINRepBoth)
	ON_BN_CLICKED(IDC_CHECK_RelPath, OnCHECKRelPath)
	ON_BN_CLICKED(IDC_RepClear, OnRepClear)
	ON_BN_CLICKED(IDC_More, OnMore)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdpDataBox メッセージ ハンドラ

void CSdpDataBox::OnAdd() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	EndDialog(IDC_Add);
}

void CSdpDataBox::OnLoad() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	music->OnRewind();
	music->m_data_type=music->f_GetFileType();
	UpdateData(FALSE);
	if (music->m_data_type<0)	{return;}

	if (music->f_GetObjectType()!=music->m_data_type)
	{
		music=CMusic::f_ObjectTypeChange(music);
	}

	music->OnLoad();
	if (music->m_stat_load)
	{
		m_time_slider.SetRange(0,(int)(music->f_GetLength())+1,TRUE);
	}
	else
	{
		m_time_slider.SetRange(0,180,TRUE);
	}

	if (music->m_dsp_on)
	{
		m_pitch_slider.EnableWindow(TRUE);
		m_tempo_slider.EnableWindow(TRUE);
	}
	else
	{
		m_pitch_slider.EnableWindow(FALSE);
		m_tempo_slider.EnableWindow(FALSE);
//		music->f_SetPitchData(0);
//		music->f_SetTempoData(0);
	}
	m_speed_slider.SetPos(music->m_speed);
	m_pitch_slider.SetPos(music->m_pitch);
	m_tempo_slider.SetPos(music->m_tempo);

	if (music->f_GetTempoInFile()>0.0)
	{
		m_tempo_in_file.Format("( %.2f )",music->f_GetTempoInFile());
	}

	UpdateData(FALSE);
}

void CSdpDataBox::OnPlay() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
//	music->f_SetStartTime(music->f_GetTime() - music->m_wait);
	music->f_SetStartTime(music->f_GetTime());
	music->f_SetEndTime(music->f_GetLength());
	music->OnPlay();
}

void CSdpDataBox::OnRewind() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	music->OnRewind();
}

void CSdpDataBox::OnRepCheck() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	music->m_stat_repeat=music->m_auto_repeat;
	UpdateData(FALSE);
}

void CSdpDataBox::OnRepeat() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	if (music->m_stat_repeat)
	{
		double time=music->f_GetTime();
		music->f_Reset();
		music->f_SetStartTime(time);
		music->OnPlay();
	}
}

void CSdpDataBox::OnTimer(UINT nIDEvent) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
 	CDataExchange dx(this,FALSE);
 
 	if (nIDEvent==1)
 	{
 		if (!music->m_play_on)
 			DDX_Check(&dx, IDC_Play, music->m_play_on);
 		if (!music->m_stat_repeat)
 			DDX_Check(&dx, IDC_Repeat, music->m_stat_repeat);
	}

	DDX_Text(&dx, IDC_EDIT_Time, music->m_time);
	int time=SdpSetStringToTime(music->m_time,1);
	if (time<0)	{time=0;}
	m_time_slider.SetPos(time);
//	DDX_Control(&dx, IDC_SLIDER_Time, m_time_slider);

//	CDialog::OnTimer(nIDEvent);
}

void CSdpDataBox::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください

	CDataExchange dx(this,FALSE);

	if ((CSliderCtrl*)pScrollBar==&m_time_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				if (music->m_play_on) {music->KillTimer(1);}
				music->m_time.Format("%02d:%02d",(m_time_slider.GetPos()/60)%100,m_time_slider.GetPos()%60);
				DDX_Text(&dx, IDC_EDIT_Time, music->m_time);
				break;
			case TB_ENDTRACK:
				music->f_Reset();
				music->m_wait=0;
				music->f_SetStartTime(m_time_slider.GetPos());
				music->SendMessage(WM_TIMER,0);
				music->OnPlay();
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_repstart_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
				SdpSetTimeToString((m_repeat_start+m_repstart_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_start,2);
//				m_repstart_updown.SetPos(m_repstart_slider.GetPos());
				DDX_Text(&dx, IDC_EDIT_RepStart, music->m_rep_start);
				if (m_repstart_slider.GetPos()>=m_repstart_slider.GetRangeMax())
				{
					m_repstart_slider.SetRange(m_repstart_slider.GetRangeMin()+1,m_repstart_slider.GetRangeMax()+1,TRUE);
				}
				if (m_repstart_slider.GetPos()<=m_repstart_slider.GetRangeMin())
				{
					m_repstart_slider.SetRange(m_repstart_slider.GetRangeMin()-1,m_repstart_slider.GetRangeMax()-1,TRUE);
				}
				break;
			case TB_TOP:
			case TB_BOTTOM:
				m_repeat_start=SdpSetStringToTime(music->m_rep_start,2)-m_repboth_slider.GetPos();
				m_repstart_slider.SetRange(-500,500,TRUE);
				m_repstart_slider.SetPos(0);
				break;
			case TB_ENDTRACK:
				music->f_SetRepTime();
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_repend_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
				SdpSetTimeToString((m_repeat_end+m_repend_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_end,2);
//				m_repend_updown.SetPos(m_repend_slider.GetPos());
				DDX_Text(&dx, IDC_EDIT_RepEnd, music->m_rep_end);
				if (m_repend_slider.GetPos()>=m_repend_slider.GetRangeMax())
				{
					m_repend_slider.SetRange(m_repend_slider.GetRangeMin()+1,m_repend_slider.GetRangeMax()+1,TRUE);
				}
				if (m_repend_slider.GetPos()<=m_repend_slider.GetRangeMin())
				{
					m_repend_slider.SetRange(m_repend_slider.GetRangeMin()-1,m_repend_slider.GetRangeMax()-1,TRUE);
				}
				break;
			case TB_TOP:
			case TB_BOTTOM:
				m_repeat_end=SdpSetStringToTime(music->m_rep_end,2)-m_repboth_slider.GetPos();
				m_repend_slider.SetRange(-500,500,TRUE);
				m_repend_slider.SetPos(0);
				break;
			case TB_ENDTRACK:
				music->f_SetRepTime();
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_repboth_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
				SdpSetTimeToString((m_repeat_start+m_repstart_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_start,2);
				SdpSetTimeToString((m_repeat_end+m_repend_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_end,2);
				DDX_Text(&dx, IDC_EDIT_RepStart, music->m_rep_start);
				DDX_Text(&dx, IDC_EDIT_RepEnd, music->m_rep_end);
				if (m_repboth_slider.GetPos()>=m_repboth_slider.GetRangeMax())
				{
					m_repboth_slider.SetRange(m_repboth_slider.GetRangeMin()+1,m_repboth_slider.GetRangeMax()+1,TRUE);
				}
				if (m_repboth_slider.GetPos()<=m_repboth_slider.GetRangeMin())
				{
					m_repboth_slider.SetRange(m_repboth_slider.GetRangeMin()-1,m_repboth_slider.GetRangeMax()-1,TRUE);
				}
				break;
			case TB_TOP:
			case TB_BOTTOM:
				m_repeat_start=SdpSetStringToTime(music->m_rep_start,2)-m_repstart_slider.GetPos();
				m_repeat_end=SdpSetStringToTime(music->m_rep_end,2)-m_repend_slider.GetPos();
				m_repboth_slider.SetRange(-500,500,TRUE);
				m_repboth_slider.SetPos(0);
				break;
			case TB_ENDTRACK:
				music->f_SetRepTime();
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_speed_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetSpeedData(m_speed_slider.GetPos());
//				m_pitch_slider.SetPos(music->m_pitch);
//				m_tempo_slider.SetPos(music->m_tempo);
				if (music->m_speed >=m_speed_slider.GetRangeMax())
				{
					m_speed_slider.SetRange(m_speed_slider.GetRangeMin()+1,m_speed_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_speed <=m_speed_slider.GetRangeMin())
				{
					m_speed_slider.SetRange(m_speed_slider.GetRangeMin()-1,m_speed_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangeSpeed();
				m_speed_slider.SetPos(music->m_speed);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_pitch_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetPitchData(m_pitch_slider.GetPos());
				if (music->m_pitch >=m_pitch_slider.GetRangeMax())
				{
					m_pitch_slider.SetRange(m_pitch_slider.GetRangeMin()+1,m_pitch_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_pitch <=m_pitch_slider.GetRangeMin())
				{
					m_pitch_slider.SetRange(m_pitch_slider.GetRangeMin()-1,m_pitch_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangePitch();
				m_pitch_slider.SetPos(music->m_pitch);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}
	else if ((CSliderCtrl*)pScrollBar==&m_tempo_slider)
	{
		switch (nSBCode)
		{
			case TB_THUMBTRACK:
			case TB_BOTTOM:
			case TB_LINEDOWN:
			case TB_LINEUP:
			case TB_PAGEDOWN:
			case TB_PAGEUP:
			case TB_TOP:
				music->f_SetTempoData(m_tempo_slider.GetPos());
//				m_speed_slider.SetPos(music->m_speed);
				if (music->m_tempo >=m_tempo_slider.GetRangeMax())
				{
					m_tempo_slider.SetRange(m_tempo_slider.GetRangeMin()+1,m_tempo_slider.GetRangeMax()+1,TRUE);
				}
				if (music->m_tempo <=m_tempo_slider.GetRangeMin())
				{
					m_tempo_slider.SetRange(m_tempo_slider.GetRangeMin()-1,m_tempo_slider.GetRangeMax()-1,TRUE);
				}
				UpdateData(FALSE);
				break;
			case TB_ENDTRACK:
				music->f_ChangeTempo();
				m_tempo_slider.SetPos(music->m_tempo);
				UpdateData(FALSE);
				break;
			default:
				break;
		}
	}

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CSdpDataBox::OnSetRepStart() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	double time;

	music->m_play_on=0;
	music->OnPlay();
	time=music->f_GetTime();

	m_repeat_start=SdpSetTimeToString(time,music->m_rep_start,2)
				- m_repboth_slider.GetPos();
	m_repstart_slider.SetRange(-500,500,TRUE);
	m_repstart_slider.SetPos(0);
//	m_repstart_updown.SetPos(0);
	music->f_SetRepTime();

	UpdateData(FALSE);
}

void CSdpDataBox::OnDeltaposSPINRepStart(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CDataExchange dx(this,FALSE);

	if (m_repstart_slider.GetPos()+pNMUpDown->iDelta>=m_repstart_slider.GetRangeMax())
	{
		m_repstart_slider.SetRange(m_repstart_slider.GetRangeMin()+pNMUpDown->iDelta,m_repstart_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	if (m_repstart_slider.GetPos()+pNMUpDown->iDelta<=m_repstart_slider.GetRangeMin())
	{
		m_repstart_slider.SetRange(m_repstart_slider.GetRangeMin()+pNMUpDown->iDelta,m_repstart_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	m_repstart_slider.SetPos(m_repstart_slider.GetPos()+pNMUpDown->iDelta);
	SdpSetTimeToString((m_repeat_start+m_repstart_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_start,2);
	DDX_Text(&dx, IDC_EDIT_RepStart, music->m_rep_start);
	music->f_SetRepTime();
	*pResult = 1;
/*	
	int min,max;
	m_repstart_updown.GetRange(min,max);

	if ((pNMUpDown->iPos+pNMUpDown->iDelta) >= min && (pNMUpDown->iPos+pNMUpDown->iDelta) <= max )
	{
		m_repstart_slider.SetPos(m_repstart_slider.GetPos()+pNMUpDown->iDelta);
		SdpSetTimeToString((m_repeat_start+m_repstart_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_start,2);
		DDX_Text(&dx, IDC_EDIT_RepStart, music->m_rep_start);
		music->f_SetRepTime();
		*pResult = 0;
	}
	else
	{
		*pResult = 1;
	}
*/
}

void CSdpDataBox::OnTestRepStart() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	double pos=SdpSetStringToTime(music->m_rep_start,2)/1000.0;
	music->f_Reset();
	music->f_SetEndTime(pos);
	music->f_SetStartTime(pos-5.0);
	music->m_stat_repeat=0;
	music->m_wait=0;
	music->m_play_on=1;
	music->OnPlay();

	UpdateData(FALSE);
}

void CSdpDataBox::OnTestRepStart2() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	double pos=SdpSetStringToTime(music->m_rep_start,2)/1000.0;
	music->f_Reset();
	music->f_SetStartTime(pos);
	music->f_SetEndTime(pos+3.0);
	music->m_stat_repeat=0;
	music->m_wait=0;
	music->m_play_on=1;
	music->OnPlay();

	UpdateData(FALSE);
}

void CSdpDataBox::OnSetRepEnd() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	double time;

	music->m_play_on=0;
	music->OnPlay();
	time=music->f_GetTime();

	m_repeat_end=SdpSetTimeToString(time,music->m_rep_end,2)
				- m_repboth_slider.GetPos();
	m_repend_slider.SetRange(-500,500,TRUE);
	m_repend_slider.SetPos(0);
//	m_repend_updown.SetPos(0);
	music->f_SetRepTime();

	UpdateData(FALSE);
}

void CSdpDataBox::OnDeltaposSPINRepEnd(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CDataExchange dx(this,FALSE);

	if (m_repend_slider.GetPos()+pNMUpDown->iDelta>=m_repend_slider.GetRangeMax())
	{
		m_repend_slider.SetRange(m_repend_slider.GetRangeMin()+pNMUpDown->iDelta,m_repend_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	if (m_repend_slider.GetPos()+pNMUpDown->iDelta<=m_repend_slider.GetRangeMin())
	{
		m_repend_slider.SetRange(m_repend_slider.GetRangeMin()+pNMUpDown->iDelta,m_repend_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	m_repend_slider.SetPos(m_repend_slider.GetPos()+pNMUpDown->iDelta);
	SdpSetTimeToString((m_repeat_end+m_repend_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_end,2);
	DDX_Text(&dx, IDC_EDIT_RepEnd, music->m_rep_end);
	music->f_SetRepTime();
	*pResult = 1;
/*
	int min,max;
	m_repend_updown.GetRange(min,max);

	if ((pNMUpDown->iPos+pNMUpDown->iDelta) >= min && (pNMUpDown->iPos+pNMUpDown->iDelta) <= max )
	{
		m_repend_slider.SetPos(m_repend_slider.GetPos()+pNMUpDown->iDelta);
		SdpSetTimeToString((m_repeat_end+m_repend_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_end,2);
		DDX_Text(&dx, IDC_EDIT_RepEnd, music->m_rep_end);
		music->f_SetRepTime();
		*pResult = 0;
	}
	else
	{
		*pResult = 1;
	}
*/
}

void CSdpDataBox::OnDeltaposSPINRepBoth(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CDataExchange dx(this,FALSE);

	if (m_repboth_slider.GetPos()+pNMUpDown->iDelta>=m_repboth_slider.GetRangeMax())
	{
		m_repboth_slider.SetRange(m_repboth_slider.GetRangeMin()+pNMUpDown->iDelta,m_repboth_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	if (m_repboth_slider.GetPos()+pNMUpDown->iDelta<=m_repboth_slider.GetRangeMin())
	{
		m_repboth_slider.SetRange(m_repboth_slider.GetRangeMin()+pNMUpDown->iDelta,m_repboth_slider.GetRangeMax()+pNMUpDown->iDelta,TRUE);
	}
	m_repboth_slider.SetPos(m_repboth_slider.GetPos()+pNMUpDown->iDelta);
	SdpSetTimeToString((m_repeat_start+m_repstart_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_start,2);
	SdpSetTimeToString((m_repeat_end+m_repend_slider.GetPos()+m_repboth_slider.GetPos())/1000.0,music->m_rep_end,2);
	DDX_Text(&dx, IDC_EDIT_RepStart, music->m_rep_start);
	DDX_Text(&dx, IDC_EDIT_RepEnd, music->m_rep_end);
	music->f_SetRepTime();
	*pResult = 1;
}

void CSdpDataBox::OnTestRepEnd() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	double pos=SdpSetStringToTime(music->m_rep_end,2)/1000.0;
	music->f_Reset();
	music->f_SetStartTime(pos-5.0-music->m_rep_mix);
	music->f_SetEndTime(pos);
	music->m_stat_repeat=0;
	music->m_wait=0;
	music->m_play_on=1;
	music->OnPlay();

	UpdateData(FALSE);
}

void CSdpDataBox::OnTestRepEnd2() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	double pos=SdpSetStringToTime(music->m_rep_end,2)/1000.0;
	music->f_Reset();
	music->f_SetStartTime(pos);
	music->f_SetEndTime(pos+3.0);
	music->m_stat_repeat=0;
	music->m_wait=0;
	music->m_play_on=1;
	music->OnPlay();

	UpdateData(FALSE);
	
}

void CSdpDataBox::OnTestLoop() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	double rep_start=SdpSetStringToTime(music->m_rep_start,2)/1000.0;
	double rep_end=SdpSetStringToTime(music->m_rep_end,2)/1000.0;
	music->f_Reset();
	music->f_SetRepTime(rep_start,rep_end);
	music->f_SetEndTime(rep_start+3.0);
	music->f_SetStartTime(rep_end-5.0-music->m_rep_mix);
	music->m_stat_repeat=1;
	music->m_wait=0;
	music->m_play_on=1;
	music->OnPlay();

	UpdateData(FALSE);
}

void CSdpDataBox::OnSetBeatsPerRep() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	long rep_start=SdpSetStringToTime(music->m_rep_start,2);
	long rep_end=SdpSetStringToTime(music->m_rep_end,2);

	if (music->m_tempo_org<1.0)
	{
		music->m_beats_per_rep=0;
	}
	else if (rep_end-rep_start>1000)
	{
		music->m_beats_per_rep=(int)(music->m_tempo_org*(rep_end-rep_start)/60000.0+0.5);
	}
	else
	{
		music->m_beats_per_rep=0;
	}

	UpdateData(FALSE);
}

void CSdpDataBox::OnChangeEDITTempoOrg() 
{
	// TODO: これが RICHEDIT コントロールの場合、コントロールは、 lParam マスク
	// 内での論理和の ENM_CHANGE フラグ付きで CRichEditCrtl().SetEventMask()
	// メッセージをコントロールへ送るために CDialog::OnInitDialog() 関数をオーバー
	// ライドしない限りこの通知を送りません。
	
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	if (music->m_tempo_org<1.0)
	{
		music->m_tempo_org=0.0;
		music->m_tempo_bpm0=128.0;
	}
	else
	{
		music->m_tempo_bpm0=music->m_tempo_org;
	}
	
	music->f_SetTempo();

	UpdateData(FALSE);
}

void CSdpDataBox::OnDeltaposSPINTempOrg(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CDataExchange dx(this,FALSE);

	int min,max;
	m_tempo_org_updown.GetRange(min,max);

	if ((pNMUpDown->iPos+pNMUpDown->iDelta) >= min && (pNMUpDown->iPos+pNMUpDown->iDelta) <= max )
	{
		music->m_tempo_org+=pNMUpDown->iDelta;
		if (music->m_tempo_org<0)	{music->m_tempo_org=0.0;}
		DDX_Text(&dx, IDC_EDIT_TempoOrg, music->m_tempo_org);
		OnChangeEDITTempoOrg();
		*pResult = 0;
	}
	else
	{
		*pResult = 1;
	}
}

void CSdpDataBox::OnSetOrigBPM() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	long rep_start=SdpSetStringToTime(music->m_rep_start,2);
	long rep_end=SdpSetStringToTime(music->m_rep_end,2);

	if (rep_end-rep_start>1000)
	{
		music->m_tempo_org=60000.0*music->m_beats_per_rep/(rep_end-rep_start);
		CString str;
		str.Format("%.4f",music->m_tempo_org);
		sscanf(str,"%lg",&music->m_tempo_org);
	}
	else
	{
		music->m_tempo_org=0.0;
	}
//	m_tempo_org_updown.SetPos((int)music->m_tempo_org);
	m_tempo_org_updown.SetPos(0);

	UpdateData(FALSE);
	OnChangeEDITTempoOrg();
}

void CSdpDataBox::OnSetRepLength() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	long rep_start=SdpSetStringToTime(music->m_rep_start,2);

	if (music->m_tempo_org<1.0)
	{
		return;
	}

	m_repeat_end=SdpSetTimeToString(rep_start/1000.0+music->m_beats_per_rep*60.0/music->m_tempo_org,music->m_rep_end,2)
				- m_repboth_slider.GetPos();
	m_repend_slider.SetPos(0);
	m_repend_updown.SetPos(0);
	music->f_SetRepTime();

	UpdateData(FALSE);
}


void CSdpDataBox::OnBUTTONFile() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CFileDialog *fd;
	UpdateData(TRUE);
	fd=new CFileDialog(TRUE,NULL,NULL,OFN_HIDEREADONLY,_T("Wave(*.wav) MIDI(*.mid) MPEG(*.mp3 *.rmp)|*.wav;*.mid;*.mp3;*.rmp|All Files (*.*)|*.*||"));
	fd->m_ofn.lpstrInitialDir=music->m_base_dir;
	if (fd->DoModal()==IDOK)
	{
		music->m_filename=fd->GetPathName();
		if (m_relpath)
		{
			m_relpath=SdpPathAbsToRel(music->m_filename,music->m_base_dir,music->m_filename);
		}
	}
	delete fd;
	UpdateData(FALSE);
}


BOOL CSdpDataBox::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください
	CWnd *wnd;
    switch (pMsg->message)
    {
    case WM_KEYDOWN:
        switch (pMsg->wParam)
        {
        case VK_RETURN:
			if (wnd=GetFocus())
			{
				switch (wnd->GetDlgCtrlID())
				{
				case IDOK:
				case IDCANCEL:
				case IDC_Add:
				case IDC_BUTTON_File:
				case IDC_Load:
				case IDC_Rewind:
				case IDC_Set_RepStart:
				case IDC_Set_RepEnd:
				case IDC_Test_RepStart:
				case IDC_Test_RepStart2:
				case IDC_Test_RepEnd:
				case IDC_Test_RepEnd2:
				case IDC_Test_Loop:
				case IDC_Set_BeatsPerRep:
				case IDC_Set_OrigBPM:
				case IDC_Set_RepLength:
					break;
				case IDC_Play:
				case IDC_Repeat:
				case IDC_RepCheck:
					pMsg->wParam=VK_SPACE;
					break;
				default:
					pMsg->wParam=VK_TAB;
//					if (wnd=GetNextDlgTabItem(wnd,GetKeyState(VK_SHIFT)&~1))
//					{
//						wnd->SetFocus();
//					}
//			        return TRUE;
					break;
				}
			}
            break;
        case 'O':
//			if (GetKeyState(VK_CONTROL)&0x80)
			if (GetKeyState(VK_MENU)&0x80)
			{
				PostMessage(WM_COMMAND,IDOK);
				return TRUE;
			}
            break;
        case 'C':
//			if (GetKeyState(VK_CONTROL)&0x80)
			if (GetKeyState(VK_MENU)&0x80)
			{
				PostMessage(WM_COMMAND,IDCANCEL);
				return TRUE;
			}
            break;
        case 'A':
//			if (GetKeyState(VK_CONTROL)&0x80)
			if (GetKeyState(VK_MENU)&0x80)
			{
				PostMessage(WM_COMMAND,IDC_Add);
				return TRUE;
			}
            break;
		default:
			break;
        }
		break;
    case WM_KEYUP:
        switch (pMsg->wParam)
        {
        case VK_RETURN:
			if (wnd=GetFocus())
			{
				switch (wnd->GetDlgCtrlID())
				{
				case IDC_Play:
				case IDC_Repeat:
				case IDC_RepCheck:
					pMsg->wParam=VK_SPACE;
					break;
				default:
					break;
				}
			}
		default:
			break;
		}
	default:
		break;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CSdpDataBox::OnRADIOWave() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
}

void CSdpDataBox::OnRadioMidi() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
}

void CSdpDataBox::OnRadioMpeg() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
}

void CSdpDataBox::OnCHECKRelPath() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);
	if (m_relpath)
	{
		m_relpath=SdpPathAbsToRel(music->m_filename,music->m_base_dir,music->m_filename);
	}
	else
	{
		m_relpath=!SdpPathRelToAbs(music->m_filename,music->m_base_dir,music->m_filename);
	}
	UpdateData(FALSE);
}

void CSdpDataBox::OnRepClear() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	UpdateData(TRUE);

	music->m_play_on=0;
	music->OnPlay();

	m_repeat_start=0;
	m_repstart_slider.SetRange(-500,500,TRUE);
	m_repstart_slider.SetPos(0);
	music->m_rep_start="";

	m_repeat_end=0;
	m_repend_slider.SetRange(-500,500,TRUE);
	m_repend_slider.SetPos(0);
	music->m_rep_end="";

	m_repboth_slider.SetRange(-500,500,TRUE);
	m_repboth_slider.SetPos(0);

	music->f_SetRepTime();

	music->m_stat_repeat=music->m_auto_repeat=0;

	UpdateData(FALSE);
}

void CSdpDataBox::OnMore() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CPropertySheet ps;
	CSdpDataProp01 dp01;

	ps.AddPage(&dp01);
	ps.m_psh.pszCaption=music->m_title;

	dp01.music=music;
	int res=ps.DoModal();

	switch (res)
	{
		case IDOK:
			music->m_cue_file.TrimLeft();
			music->m_cue_prog.TrimLeft();
			break;
		default:
			break;
	}
}
