// SdPlayerDoc.cpp : CSdPlayerDoc クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "SdPlayer.h"
#include "SdPlayerDoc.h"
#include "Global.h"

#include "shlwapi.h"

//UINT AFXAPI AfxGetFileName(LPCTSTR lpszPathName, LPTSTR lpszTitle, UINT nMax);

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerDoc

IMPLEMENT_DYNCREATE(CSdPlayerDoc, CDocument)

BEGIN_MESSAGE_MAP(CSdPlayerDoc, CDocument)
	//{{AFX_MSG_MAP(CSdPlayerDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CSdPlayerDoc, CDocument)
	//{{AFX_DISPATCH_MAP(CSdPlayerDoc)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// メモ: VBA からバインドされているタイプ セーフをサポートするために IID_ISdPlayer  のサポートを追加します。
//       この IID は、.ODL ファイルの中のディスプインターフェイスにアタッチされている GUID にマッチしていな 
//       ければなりません。 

// {D0499566-B2AC-11D5-BC30-009027BFCD6A}
static const IID IID_ISdPlayer =
{ 0xd0499566, 0xb2ac, 0x11d5, { 0xbc, 0x30, 0x0, 0x90, 0x27, 0xbf, 0xcd, 0x6a } };

BEGIN_INTERFACE_MAP(CSdPlayerDoc, CDocument)
	INTERFACE_PART(CSdPlayerDoc, IID_ISdPlayer, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerDoc クラスの構築/消滅

CSdPlayerDoc::CSdPlayerDoc()
{
	// TODO: この位置に１度だけ呼ばれる構築用のコードを追加してください。

	EnableAutomation();

	AfxOleLockApp();

	m_data_version=1;
	m_subitems=SDP_DATABASE_ITEMS;  //defined in SdPlayer.h
	m_stack=NULL;
	m_undo_flag=0;
	f_SetColumns();
}

CSdPlayerDoc::~CSdPlayerDoc()
{
	for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
	{

		CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
		if (sa!=NULL)
		{
			sa->RemoveAll();
			delete sa;
		}
	}
	m_music_list.RemoveAll();
	m_colwidth.RemoveAll();
	m_coltitle.RemoveAll();
	if (m_stack!=NULL)
	{
		m_stack->RemoveAll();
		delete m_stack;
	}
	AfxOleUnlockApp();
}

/*
BOOL CSdPlayerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: この位置に再初期化処理を追加してください。
	// (SDI ドキュメントはこのドキュメントを再利用します。)

	return TRUE;
}
*/

void CSdPlayerDoc::DeleteContents() 
{
	// TODO: この位置に固有の処理を追加するか、または基本クラスを呼び出してください

	for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
	{

		CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
		if (sa!=NULL)
		{
			sa->RemoveAll();
			delete sa;
		}
	}
	m_music_list.RemoveAll();
	
	CDocument::DeleteContents();
}

void CSdPlayerDoc::f_SetColumns()
{
	m_coltitle.SetSize(m_subitems);   m_colwidth.SetSize(m_subitems);
	m_coltitle.SetAt(0,"Title");      m_colwidth.SetAt(0,150);//200
	m_coltitle.SetAt(1,"Label");      m_colwidth.SetAt(1,40);//50
	m_coltitle.SetAt(2,"No.");        m_colwidth.SetAt(2,40);//50
	m_coltitle.SetAt(3,"Category");   m_colwidth.SetAt(3,50);//60
	m_coltitle.SetAt(4,"Rep");        m_colwidth.SetAt(4,30);//40
	m_coltitle.SetAt(5,"RepStart");   m_colwidth.SetAt(5,50);//64
	m_coltitle.SetAt(6,"RepEnd");     m_colwidth.SetAt(6,50);//64
	m_coltitle.SetAt(7,"Wait");       m_colwidth.SetAt(7,30);//40
	m_coltitle.SetAt(8,"Tempo");      m_colwidth.SetAt(8,40);//60
	m_coltitle.SetAt(9,"Speed(0.1%)");  m_colwidth.SetAt(9,60);//80
	m_coltitle.SetAt(10,"Pitch(tone)"); m_colwidth.SetAt(10,60);//80
	m_coltitle.SetAt(11,"Tempo(0.1%)"); m_colwidth.SetAt(11,60);//80
	m_coltitle.SetAt(12,"Type");      m_colwidth.SetAt(12,50);//60
	m_coltitle.SetAt(13,"File");      m_colwidth.SetAt(13,140);//180
	m_coltitle.SetAt(14,"RepMix");    m_colwidth.SetAt(14,40);
	m_coltitle.SetAt(15,"TempoOrg");  m_colwidth.SetAt(15,50);//64
	m_coltitle.SetAt(16,"B/Rep");     m_colwidth.SetAt(16,40);//60
	m_coltitle.SetAt(17,"Num");       m_colwidth.SetAt(17,30);//40
	m_coltitle.SetAt(18,"flags");     m_colwidth.SetAt(18,0);
	m_coltitle.SetAt(19,"CueSheet");  m_colwidth.SetAt(19,30);
}


/////////////////////////////////////////////////////////////////////////////
// CSdPlayerDoc シリアライゼーション

void CSdPlayerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: この位置に保存用のコードを追加してください。
		for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
		{
			CString str;
			CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
			if (sa->GetSize()<m_subitems)
			{
				sa->SetSize(m_subitems);
			}

			str.Format("%d",i+1);
			sa->SetAt(17,str);
		}

		//+++++ 03.02.27 kawamura add start
		if (f_CheckExt(ar,"csv"))
		{// CSV
			f_WriteCsvFile(ar);
		}
		//+++++ 03.02.27 kawamura add end
		else
		{// Binary
			ar << m_data_version;

//			ar << m_subitems;
//			m_colwidth.Serialize(ar);
//			m_coltitle.Serialize(ar);
			m_music_list.Serialize(ar);
		}
	}
	else
	{
		// TODO: この位置に読み込み用のコードを追加してください。

		//+++++ 03.02.27 kawamura add start
		if (f_CheckExt(ar,"csv"))
		{// CSV
			f_ReadCsvFile(ar);
		}
		//+++++ 03.02.27 kawamura add end
		else
		{// Binary
			ar >> m_data_version;
			if (m_data_version<=1)
			{
	//			int subitems;
	//			ar >> subitems;
	//			if (subitems>=m_subitems)	{m_subitems=subitems;}
	//			m_colwidth.Serialize(ar);
	//			m_coltitle.Serialize(ar);
				m_music_list.Serialize(ar);
			}
		}

		if (m_data_version>1)
		{
			CString str;
			str.Format("data version (%d) must be 1 or lower",m_data_version);
			AfxMessageBox(str);
			m_data_version=1;
		}

		if (m_data_version==0)
		{
			for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
			{
				CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
				sa->SetAt(13,sa->GetAt(11)); // filename 11->13
				sa->SetAt(11,""); // tempo new
				sa->SetAt(10,""); // pitch new
			}
			m_data_version=1;
		}

		for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
		{
			CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
			int j=sa->GetSize();
			if (j<m_subitems)
			{
				sa->SetSize(m_subitems);
				for ( ; j<m_subitems ; j++)
				{
					sa->SetAt(j,"");
				}
			}
		}
	}
	UpdateAllViews(NULL);
}

//+++++++++++++ 03.02.27 kawamura add

// 拡張子チェック
BOOL CSdPlayerDoc::f_CheckExt(CArchive& ar,char *ext)
{
	CString buff, file_name, file_ext;
	int find;
	CFile *fp = ar.GetFile();
	file_name = fp->GetFileName();
	find = file_name.ReverseFind('.');
	if (find > 0)
	{
		file_ext = file_name.Mid(find+1);
		if (file_ext.CompareNoCase(ext) == 0)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void CSdPlayerDoc::f_WriteCsvFile(CArchive& ar)
{
	int i, j, i_max, j_max;
	CString buff, str, kind;
	CStringArray *str_ary;

	// バージョン情報書込
	buff.Format("%d\n", m_data_version);
	ar.WriteString(buff);

	// Musicリスト書込
	i_max = m_music_list.GetSize();
	for (i = 0; i < i_max; i++)
	{
		buff = "";
		str_ary = (CStringArray *)m_music_list.GetAt(i);
		j_max = str_ary->GetSize();
		for (j = 0; j < j_max; j++)
		{
			if (j > 0)
			{	// カンマ区切り
				buff += ",";
			}
			str = str_ary->GetAt(j);
			if (str.Find(',') > 0)
			{	// 文字中にカンマがあったらダブルクォーテーションで囲む
				str = "\"" + str + "\"";
			}
			buff += str;
		}
		buff += "\n";
		ar.WriteString(buff);
	}
}

int CSdPlayerDoc::f_ReadCsvFile(CArchive& ar)
{
	int i;
	CString buff, str, kind;
	CStringArray *str_ary;
	int ret;
	char data[50][256], data_buff[256];
	int data_cnt;

	// バージョン情報取得
	ret = ar.ReadString(buff);
	if (ret == FALSE)
		return FALSE;
	strcpy(data_buff, LPCTSTR(buff));
	data_cnt = f_scanf(data_buff, data);
	if (data_cnt > 0)
		m_data_version = atoi(data[0]);
	else
		return FALSE;

	// バージョン情報チェック
	if (m_data_version>1)
	{
		return FALSE;
	}

	// Musicリスト取得
	while (1)
	{
		ret = ar.ReadString(buff);
		if (ret == FALSE)
			break;	// 読込終了
		strcpy(data_buff, LPCTSTR(buff));
		data_cnt = f_scanf(data_buff, data);
		if (data_cnt > 0)
		{
			str_ary = new CStringArray;
			for (i = 0; i < data_cnt; i++)
			{
				str_ary->Add(data[i]);
			}
			m_music_list.Add(str_ary);
		}
		else
			break;	// 読込終了
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////
//文字列からのデータ抽出
//　カンマを区切りとしてデータを取り出す。
//　ただし、例外としてダブルクォーテーションで囲まれたデータは
//　１つとみなす。
//char buff[]       ：一行のデータ
//char data[][256]　：一項目のデータ
//戻り値 (int)      ：抽出したデータの数
int CSdPlayerDoc::f_scanf(char buff[], char data[][256])
{
	int i, n, datano, d_quot;

	i = 0;
	datano = 0;
	d_quot = 0;
	while (buff[i] != '\n' && buff[i] != '\r' && buff[i] != '\0')
	{
		while (buff[i] == ',')
		{
			data[datano][0] = '\0';	// データ無し
			i++;
			datano++;
		}
		n = 0;
		while (buff[i] != '\n'	// 改行コード(CR)
		  && buff[i] != '\r'	// 改行コード(LF)
		  && buff[i] != '\0'	// 文字列終了コード
		  && (buff[i] != ','	||  d_quot  != 0)) // カンマ
		{
			if (d_quot == 0 && buff[i] == '\"')
			{
				d_quot = 1;
				i++;
				continue;
			}
			else if (d_quot != 0 && buff[i] == '\"')
			{
				d_quot = 0;
				i++;
				continue;
			}
			data[datano][n++] = buff[i++];
		}
		if (n > 0)
		{
			data[datano][n] = '\0';
			datano++;
			if (buff[i] == ',')
				i++;
		}
	}
	if (i > 0)
	{	// 最後がカンマで終わっていたらデータ数+1
		if (buff[i-1] == ',')
		{
			data[datano][0] = '\0';
			datano++;
		}
	}

	return datano;
}

// [保存],[上書き保存]処理のカスタマイズ
BOOL CSdPlayerDoc::DoSave(LPCTSTR lpszPathName,BOOL bReplace)
{
	CString save_file = lpszPathName;
	
	if (save_file.IsEmpty())
	{
		CString old_file=PathFindFileName(GetPathName());
		CFileDialog save_dlg(FALSE, "", old_file,
			OFN_OVERWRITEPROMPT | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY,
			"SdPlayer Binary Database (*.sdm)|*.sdm|SdPlayer Text Database (*.csv)|*.csv|すべてのファイル (*.*)|*.*||",
			NULL);
		
		save_dlg.m_ofn.lpstrInitialDir=f_GetCurDocDir();

		if (save_dlg.DoModal() != IDOK)
		{
			return FALSE;
		}
		save_file = save_dlg.GetPathName();
	}

	if (bReplace == TRUE)
		SetPathName(save_file);

	OnSaveDocument(save_file);

	return TRUE;
}
//+++++++++++++ 03.02.27 kawamura end

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerDoc クラスの診断

#ifdef _DEBUG
void CSdPlayerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSdPlayerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSdPlayerDoc コマンド

void CSdPlayerDoc::f_DelItem(int i)
{
	CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
	if (sa!=NULL)
	{
		sa->RemoveAll();
		delete sa;
	}
	m_music_list.RemoveAt(i,1);
	f_SetItemNo();
	SetModifiedFlag(TRUE);
	UpdateAllViews(NULL,i);
}

void CSdPlayerDoc::f_AddItem(int i,CStringArray *sa)
{
	m_music_list.InsertAt(i,sa);
	f_SetItemNo();
	SetModifiedFlag(TRUE);
	UpdateAllViews(NULL,i);
}

void CSdPlayerDoc::f_PathAbsToRel(int i)
{
	CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
	if (sa!=NULL)
	{
		CString path=sa->GetAt(13);
		CString base_dir=f_GetCurDocDir();
		if (SdpPathAbsToRel(path,base_dir,path))
		{
			sa->SetAt(13,path);
			SetModifiedFlag(TRUE);
			UpdateAllViews(NULL,i);
		}
	}
}

void CSdPlayerDoc::f_PathRelToAbs(int i)
{
	CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
	if (sa!=NULL)
	{
		CString path=sa->GetAt(13);
		CString base_dir=f_GetCurDocDir();
		if (SdpPathRelToAbs(path,base_dir,path))
		{
			sa->SetAt(13,path);
			SetModifiedFlag(TRUE);
			UpdateAllViews(NULL,i);
		}
	}
}

CString &CSdPlayerDoc::f_GetCurDocDir()
{
	char buf[_MAX_PATH];
	strcpy(buf,GetPathName());
	PathRemoveFileSpec(buf);
	m_current_docdir=buf;
//	m_current_docdir=GetPathName().Left(GetPathName().GetLength()-AfxGetFileName(GetPathName(),NULL,0));
	return m_current_docdir;
}

void CSdPlayerDoc::f_SetItemNo()
{
	for (int i=0,max=m_music_list.GetSize() ; i<max ; i++)
	{
		CString str;
		CStringArray *sa=(CStringArray *)m_music_list.GetAt(i);
		str.Format("%d",i+1);
		sa->SetAt(17,str);
	}
}

static int s_col;
static int f_compare( const void *saa1, const void *saa2 )
{
	CStringArray *sa1=*(CStringArray **)saa1;
	CStringArray *sa2=*(CStringArray **)saa2;

	CString str1=sa1->GetAt(s_col);
	CString str2=sa2->GetAt(s_col);

	int n=str1.CollateNoCase(str2);
	if (n==0)
	{
		int num1=atoi(sa1->GetAt(17));
		int num2=atoi(sa2->GetAt(17));
		n=num1-num2;
	}
	return n;
}

void CSdPlayerDoc::f_SortItems(int col)
{
	CStringArray **saa;
	int i,max;

	s_col=col;
	max=m_music_list.GetSize();
	saa=(CStringArray **)calloc(max,sizeof(CStringArray *));
	if (saa==NULL)
	{
		AfxMessageBox("memory allocation error");
		return;
	}

	for (i=0 ; i<max ; i++)
	{
		saa[i]=(CStringArray *)m_music_list.GetAt(i);
	}

	qsort((void*)saa,max,sizeof(CStringArray *),f_compare);

	for (i=0 ; i<max ; i++)
	{
		m_music_list.SetAt(i,saa[i]);
	}
	free(saa);

	f_SetItemNo();
	SetModifiedFlag(TRUE);
	UpdateAllViews(NULL,0);
}

void CSdPlayerDoc::f_ReOpenFile()
{
	CFile file;
	LPCTSTR path=GetPathName();

	if (IsModified())
	{
		if (AfxMessageBox("Modified data will be erased. OK?",MB_OKCANCEL)!=IDOK)
		{
			return;
		}
	}

	if (path && file.Open(path, CFile::modeRead))
	{
		CArchive ar(&file, CArchive::load);
		DeleteContents();
		Serialize(ar);
		SetModifiedFlag(FALSE);
		ar.Close();
		file.Close();
	}
	else
	{
		AfxMessageBox("file open failed");
	}
}

void CSdPlayerDoc::f_AddFile()
{
	CFile file;
	CString path;
	CObArray list2;
	int data_version;

	if (!AfxGetApp()->DoPromptFileName(path, AFX_IDS_OPENFILE,
		OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, TRUE, NULL))
	{
		return;
	}

	if (path && file.Open(path, CFile::modeRead))
	{
		CArchive ar(&file, CArchive::load);
		ar >> data_version;
		if (data_version==m_data_version)
		{
			list2.Serialize(ar);
			for (int i=0, max=list2.GetSize() ; i<max ; i++)
			{
				CStringArray *sa=(CStringArray *)list2.GetAt(i);
				int j=sa->GetSize();
				if (j<m_subitems)
				{
					sa->SetSize(m_subitems);
					for ( ; j<m_subitems ; j++)
					{
						sa->SetAt(j,"");
					}
				}
				m_music_list.Add(sa);
			}
			f_SetItemNo();
			SetModifiedFlag(TRUE);
			UpdateAllViews(NULL,m_music_list.GetSize()-max);
			list2.RemoveAll();
		}
		else
		{
			AfxMessageBox("data version is different");
		}

		ar.Close();
		file.Close();
	}
	else
	{
		AfxMessageBox("file open failed");
	}
}

void CSdPlayerDoc::f_Undo(CListCtrl& refCtrl)
{
	int num=0,i=0;
	CStringArray *sa;
	switch(m_undo_flag)
	{
	case 1: // replaced
		sscanf(m_stack->GetAt(17),"%d",&num);
		i=num-1;
		sa=(CStringArray *)m_music_list.GetAt(i);
		m_music_list.SetAt(i,m_stack);
		m_stack=sa;
		m_undo_flag=1; // replaced
		break;
	case 2: // added
		num=m_music_list.GetSize();
		i=num-1;
		if (m_stack!=NULL)
		{
			m_stack->RemoveAll();
			delete m_stack;
		}
		m_stack=(CStringArray *)m_music_list.GetAt(i);
		m_music_list.RemoveAt(i);
		m_undo_flag=3; // deleted
		break;
	case 3: // deleted
		sscanf(m_stack->GetAt(17),"%d",&num);
		i=num-1;
		m_music_list.InsertAt(i,m_stack);
		m_stack=NULL;
		m_undo_flag=2; // added
		break;
	default:
		return;
		break;
	}
	f_SetItemNo();
	SetModifiedFlag(TRUE);
	UpdateAllViews(NULL,0);
	for (int j=0, max=refCtrl.GetItemCount() ; j<max ; j++)
	{
		refCtrl.SetItemState(j,0,LVIS_SELECTED);	
	}
	if (i>=max)
	{
		i=max-1;
	}
	refCtrl.SetSelectionMark(i);
	refCtrl.SetItemState(i,LVIS_SELECTED|LVIS_FOCUSED,LVIS_SELECTED|LVIS_FOCUSED);	
	refCtrl.EnsureVisible(i,TRUE);
}
