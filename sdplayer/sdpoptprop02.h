#if !defined(AFX_SDPOPTPROP02_H__38E81085_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SDPOPTPROP02_H__38E81085_91C0_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SdpOptProp02.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSdpOptProp02 ダイアログ

class CSdpOptProp02 : public CPropertyPage
{
	DECLARE_DYNCREATE(CSdpOptProp02)

// コンストラクション
public:
	CSdpOptProp02();
	~CSdpOptProp02();

// ダイアログ データ
	//{{AFX_DATA(CSdpOptProp02)
	enum { IDD = IDD_OptProp02 };
	CSpinButtonCtrl	m_spin_fadeout_len;
	CListBox	m_list1;
	//}}AFX_DATA

	CSdpProfile *profile;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。

	//{{AFX_VIRTUAL(CSdpOptProp02)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CSdpOptProp02)
	virtual BOOL OnInitDialog();
	afx_msg void OnItemUp();
	afx_msg void OnItemDown();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SDPOPTPROP02_H__38E81085_91C0_11D7_BC30_009027BFCD6A__INCLUDED_)
