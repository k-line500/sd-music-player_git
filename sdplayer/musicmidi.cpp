// MusicMIDI.cpp: CMusicMIDI クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "sdplayer.h"
#include "MusicMIDI.h"
#include "Global.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////

#define DWORDSWAP(dw) \
    ((((dw)>>24)&0x000000FFL)|\
    (((dw)>>8)&0x0000FF00L)|\
    (((dw)<<8)&0x00FF0000L)|\
    (((dw)<<24)&0xFF000000L))

#define WORDSWAP(w) \
    ((((w)>>8)&0x00FF)|\
    (((w)<<8)&0xFF00))

// CSmfTrack
//////////////////////////////////////////////////////////////////////
// 構築/消滅/メンバ関数
//////////////////////////////////////////////////////////////////////
CSmfTrack::CSmfTrack()
{
	no=0;
	buf=NULL;
	buf_size=0;
	final_tick=0;
	final_tick_time=0;
	next_tick=0;
	next_tick_time=0;
	next_event_pos=NULL;

	delta_time=0;
	event.dw=0;
	params=0;
	param_pos=NULL;
	running_status=0;
	name="";
}

CSmfTrack::~CSmfTrack()
{
	f_FreeBuffer();
}

int CSmfTrack::f_AllocBuffer(DWORD size)
{
	buf=(LPSTR)SdpAllocBuffer(size);
	next_event_pos=buf;
	if (buf==NULL)
	{
		buf_size=0;
		return 0;
	}
	else
	{
		buf_size=size;
	}
	final_tick=0;
	final_tick_time=0;
	next_tick=0;
	next_tick_time=0;
	next_event_pos=buf;
	return 1;
}

void CSmfTrack::f_FreeBuffer()
{
	SdpFreeBuffer(buf);
	buf=NULL;
	buf_size=0;
}

void CSmfTrack::f_Rewind()
{
	next_tick=0;
	next_tick_time=0;
	next_event_pos=buf;
	delta_time=0;
	param_pos=NULL;
	params=0;
	event.dw=0;
	running_status=0;
	f_LoadEvent();
}

LPCSTR CSmfTrack::f_LoadEvent()
{
	static CString str;
	BYTE byte;
	LPSTR bufend=buf+buf_size;
	
	// get delta time
	if ((str=f_GetVDWord(&delta_time))!="")
	{
		return str;
	}

    if ( next_event_pos+3 > bufend)
	{
		str.Format("track(%d) format error",no);
		return str;
	}
	byte=*next_event_pos++;

	if ((BYTE)0xF0 > byte) // short message
	{
		if ((BYTE)0x80 > byte)
		{
			if (running_status==0)
			{
				str.Format("track(%d) status error",no);
				return str;
			}
		}
		else
		{
			running_status=byte;
			byte=*next_event_pos++;
		}
		event.byte[3]=MEVT_SHORTMSG;
		event.byte[1]=byte;
		event.byte[0]=running_status;
		switch ((running_status >> 4) & 0x0F)
		{
		case 0x0C:
		case 0x0D:
			event.byte[2]=0;
			break;
		default:
			event.byte[2]=*next_event_pos++;
			break;
		}
	}
	else
	{
		running_status=0;
		if (byte==(BYTE)0xFF && *next_event_pos==(BYTE)0x51) // TEMPO
		{
			next_event_pos++;
			event.byte[3]=MEVT_TEMPO;
			if (*next_event_pos++!=(BYTE)0x03)
			{
				str.Format("track(%d) tempo error",no);
				return str;
			}
			event.byte[2]=*next_event_pos++;
			event.byte[1]=*next_event_pos++;
			event.byte[0]=*next_event_pos++;
		}
		else if (byte==(BYTE)0xFF) // other Meta event
		{
			event.byte[0]=byte;
			byte=*next_event_pos++;
			event.byte[3]=MEVT_NOP;
			event.byte[2]=0;
			event.byte[1]=byte;
			if ((str=f_GetVDWord(&params))!="")
			{
				return str;
			}
			if (params & 0xFF000000)
			{
				str.Format("track(%d) length is too long",no);
				return str;
			}
			param_pos=next_event_pos;
			next_event_pos+=params;
		}
		else if (byte==(BYTE)0xF0 || byte==(BYTE)0xF7) // SysEx
		{
			event.byte[0]=byte;
			event.byte[3]=MEVT_LONGMSG;
			event.byte[2]=0;
			event.byte[1]=0;
			if ((str=f_GetVDWord(&params))!="")
			{
				return str;
			}
			if (params & 0xFF000000)
			{
				str.Format("track(%d) length is too long",no);
				return str;
			}
			param_pos=next_event_pos;
			next_event_pos+=params;
		}
		else
		{
			str.Format("track(%d) event error",no);
			return str;
		}
	}
	
	next_tick++;
	next_tick_time+=delta_time;
	return "";
}

LPCSTR CSmfTrack::f_GetVDWord(DWORD *dw)
{
	static CString str;
	BYTE byte,byte0;
	int n;
	LPSTR bufend=buf+buf_size;
	
	*dw=0;
	n=0;
    do
    {
		if ( next_event_pos>= bufend)
		{
			str.Format("track(%d) vlength error",no);
			return str;
		}

		byte=*next_event_pos++;
		if (n++==0)	{byte0=byte;}
        *dw = (*dw << 7) | (byte & 0x7F);
    } while (byte & 0x80);
	
	if (n>6 || (n==5 && (byte0 & 0xF0)))
	{
		str.Format("track(%d) length is too long",no);
        return str;
	}
	return "";
}

// CSmf
//////////////////////////////////////////////////////////////////////
// 構築/消滅/メンバ関数
//////////////////////////////////////////////////////////////////////
CSmf::CSmf()
{
	name="";
	header.format=0;
	header.track_num=0;
	header.division=0;
	last_tick=0;
	last_tick_time=0;
	next_tick_time=0;
	final_tick=0;
	final_tick_time=0;
	tracks=NULL;
	tempo_base=0.0;
	first_note_on=0;
	seek_status=0;
	sec_per_tick_time=0.0;
	hmmio = (HMMIO)NULL;
}

CSmf::~CSmf()
{
	f_UnLoad();
}

void CSmf::f_UnLoad()
{

	if (tracks)
	{
		for (int i=0 ; i<header.track_num ; i++)
		{
			delete tracks[i];
		}
	}
	SdpFreeBuffer((LPSTR)tracks);
	tracks=NULL;
	name="";
	header.format=0;
	header.track_num=0;
	header.division=0;
	last_tick=0;
	last_tick_time=0;
	next_tick_time=0;
	final_tick=0;
	final_tick_time=0;
	tempo_base=0.0;
	sec_per_tick_time=0.0;
	first_note_on=0;
	seek_status=0;
}

int CSmf::f_FindChunk(HMMIO hmmio,MMCKINFO *ckinfo)
{
	int res=-1;
	do
	{
		ckinfo->fccType=0;
		if (4==mmioRead(hmmio,(HPSTR)&ckinfo->fccType, 4))
		{
			if (ckinfo->ckid==ckinfo->fccType)
			{
				res=0;
			}
		}
		else
		{
			res=1;
		}
	}while(res==-1);

	if (res==0)
	{
		if (4==mmioRead(hmmio,(HPSTR)&ckinfo->cksize, 4))
		{
			ckinfo->cksize=DWORDSWAP(ckinfo->cksize);
		}
		else
		{
			res=2;
		}
	}
	return res;
}

LPCSTR CSmf::f_Load(LPCSTR filename)
{
	static CString str;
    MMIOINFO mmioinfo;
    MMCKINFO ckRIFF;
    MMCKINFO ckDATA;
    MMCKINFO ckMThd;
    MMCKINFO ckMTrk;
 
    memset(&mmioinfo, 0, sizeof(MMIOINFO));
    hmmio = mmioOpen((LPSTR)filename, &mmioinfo, MMIO_READ|MMIO_ALLOCBUF);
    if (hmmio==(HMMIO)NULL)
    {
		return "file open error";
    }

	memset(&ckRIFF, 0, sizeof(ckRIFF));
	memset(&ckMThd, 0, sizeof(ckMThd));

	if (4==mmioRead(hmmio,(HPSTR)&ckMThd.fccType, 4) &&
		ckMThd.fccType==mmioFOURCC('M','T','h','d'))
	{
		mmioSeek(hmmio,0,SEEK_SET);
	}
	else
	{
		mmioSeek(hmmio,0,SEEK_SET);
		if (0 == mmioDescend(hmmio, &ckRIFF, NULL, MMIO_FINDRIFF) &&
			ckRIFF.fccType == mmioFOURCC('R','M','I','D'))
		{
			ckDATA.ckid = mmioFOURCC('d','a','t','a');
			
			if (0 != mmioDescend(hmmio, &ckDATA, &ckRIFF, MMIO_FINDCHUNK))
			{
				mmioClose(hmmio, 0);
				hmmio = (HMMIO)NULL;
				return "RMID file format error";
			}
		}
		else
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			return "MIDI file format error";
		}
	}

	ckMThd.ckid = mmioFOURCC('M','T','h','d');
	if ( 0!=f_FindChunk(hmmio,&ckMThd))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "MThd chunk error";
	}

	if (ckMThd.cksize != (DWORD)mmioRead(hmmio,(HPSTR)&(header),ckMThd.cksize))
	{
	    mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "MThd read error";
	}
	header.format=WORDSWAP(header.format);
	header.track_num=WORDSWAP(header.track_num);
	header.division=WORDSWAP(header.division);

	tracks=(CSmfTrack**)SdpAllocBuffer(sizeof(CSmfTrack*)*header.track_num);
	if (!tracks)
	{
		mmioClose(hmmio, 0);
		hmmio = (HMMIO)NULL;
		return "smf tracks allocation error";
	}

	ckMTrk.ckid = mmioFOURCC('M','T','r','k');
	for (int i=0 ; i<header.track_num ; i++)
	{
		if ( 0!=f_FindChunk(hmmio,&ckMTrk) )
		{
		    mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			str.Format("MTrk(%d) chunk error",i);
			return str;
		}
		tracks[i]= new CSmfTrack;
		tracks[i]->no=i;
		if (!tracks[i]->f_AllocBuffer(ckMTrk.cksize))
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			str.Format("smf track(%d) buf alloc error",i);
			return str;
		}
		if (tracks[i]->buf_size!=(DWORD)mmioRead(hmmio,(HPSTR)tracks[i]->buf,tracks[i]->buf_size))
		{
			mmioClose(hmmio, 0);
			hmmio = (HMMIO)NULL;
			str.Format("smf track(%d) read error",i);
			return str;
		}
	}

	mmioClose(hmmio, 0);
	hmmio = (HMMIO)NULL;

//	return "success";
	return "";
}

void CSmf::f_Rewind()
{
	for (int i=0 ; i<header.track_num ; i++)
	{
		tracks[i]->f_Rewind();
	}
	last_tick=0;
	last_tick_time=0;
	first_note_on=0;
	seek_status=0;
	f_GetNextEvent();
}

LPCSTR CSmf::f_Check()
{
	static CString str;

	f_Rewind();

	for (int i=0 ; i<header.track_num ; i++)
	{
		while(1)
		{
			if (tracks[i]->event.byte[3] == MEVT_NOP
				&& tracks[i]->event.byte[0] == (BYTE)0xFF
				&& tracks[i]->event.byte[1] == (BYTE)0x2F
				)	break;
			str=tracks[i]->f_LoadEvent();
			if (str!="")	return str;
		}
		tracks[i]->final_tick=tracks[i]->next_tick;
		tracks[i]->final_tick_time=tracks[i]->next_tick_time;

		if (final_tick<tracks[i]->final_tick)
		{
			final_tick=tracks[i]->final_tick;
		}
		if (final_tick_time<tracks[i]->final_tick_time)
		{
			final_tick_time=tracks[i]->final_tick_time;
		}
	}

	f_Rewind();

//	return "success";
	return "";
}

CSmfTrack *CSmf::f_GetNextEvent()
{
	CSmfTrack *track=NULL;
	next_tick_time=final_tick_time;

	for (int i=0 ; i<header.track_num ; i++)
	{
		if (tracks[i]->next_tick < tracks[i]->final_tick)
		{
			if (tracks[i]->next_tick_time < next_tick_time)
			{
				track=tracks[i];
				next_tick_time=track->next_tick_time;
			}
		}
	}

	return track;
}

void CSmf::f_GetInfo()
{
	CSmfTrack *track;
	f_Rewind();
	while(track=f_GetNextEvent())
	{
		if (tempo_base<1.0 && track->event.byte[3]==MEVT_TEMPO)
		{
			if ((track->event.dw & 0x00FFFFFFL)>0)
			{
				tempo_base=60000000.0/(double)(track->event.dw & 0x00FFFFFFL); // bpm
			}
			else
			{
				tempo_base=0.0;
			}
		}
		else if (track->event.byte[0]==0xFF && track->event.byte[1]==0x03)
		{
			char *cp=track->param_pos + track->params;
			char c=*cp;
			*cp='\0';
			track->name=(LPSTR)track->param_pos;
			*cp=c;
			if (track->no==0)
			{
				name=track->name;
			}
		}
		track->f_LoadEvent();
	}
	f_Rewind();

	time_division=header.division;
    if (time_division & 0x8000)
    {
		time_format=1;
		time_frame = -(int)(char)((time_division >> 8)&0xFF);
		if (29 == time_frame)	{time_frame=30;}
		sec_per_tick_time = time_frame* (time_division & 0xFF);
		if (sec_per_tick_time>0.0)
		{
			sec_per_tick_time = 1.0/sec_per_tick_time;
		}
		else
		{
			sec_per_tick_time = 1.0 ;
		}
		tempo_base=0.0;
    }
	else
	{
		time_format=0;
		time_frame=0;
		if (tempo_base<1.0)
		{
			tempo_base=120.0; // 120 bpm
		}
		sec_per_tick_time = 60.0/(tempo_base*time_division);
	}
}

int CSmf::f_ReadEvent(MIDIHDR *mh,DWORD max_tick_time)
{
	CSmfTrack *track;
	LPDWORD buf_dw;
	
	do
	{
		track=f_GetNextEvent();
		if (track==NULL || track->next_tick_time>max_tick_time)
		{
			return 0;
		}

		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		
		if (track->event.byte[3]==MEVT_SHORTMSG||track->event.byte[3]==MEVT_TEMPO)
		{
			if (!first_note_on && (track->event.byte[0] & 0xF0)==0x90)
			{
				return 2;
			}
			if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
			{
				return 1;
			}
			*buf_dw++ =track->next_tick_time - last_tick_time;
			*buf_dw++ =0;
			*buf_dw++ =track->event.dw;
			mh->dwBytesRecorded+=3*sizeof(DWORD);
		}
		else if (track->event.byte[3]==MEVT_LONGMSG)
		{
			DWORD length;
			length=track->params;
			if (track->event.byte[0]==(BYTE)0xF0)
			{
				length++;
			}
			DWORD rounded = (length + 3) & (~3L);
			if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD)+rounded)
			{
				return 1;
			}
			*buf_dw++ =track->next_tick_time - last_tick_time;
			*buf_dw++ =0;
			*buf_dw++ =(track->event.dw & 0xFF000000L) | (length & 0x00FFFFFFL);

			LPBYTE buf_b=(LPBYTE)buf_dw ;

			if (track->event.byte[0]==(BYTE)0xF0)
			{
				*buf_b++=(BYTE)0xF0;
			}
			memcpy(buf_b,track->param_pos,track->params);
			buf_b+=track->params;
			memset(buf_b,0,rounded-length);

			mh->dwBytesRecorded+=3*sizeof(DWORD) + rounded;
			buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		}
		last_tick_time=track->next_tick_time;
		last_tick++;
		track->f_LoadEvent();
	}while(1);
	return 0;
}

int CSmf::f_Seek(MIDIHDR *mh,DWORD seek_tick_time,int mode)
{
	switch(mode)
	{
	default:
	case 0: // normal mode
		return f_Seek2(mh,seek_tick_time,mode);
//		return f_Seek1(mh,seek_tick_time);
		break;
	case 1: // repeat mode
		return f_Seek2(mh,seek_tick_time,mode);
		break;
	}
}

int CSmf::f_Seek1(MIDIHDR *mh,DWORD seek_tick_time)
{
	CSmfTrack *track;
	LPDWORD buf_dw;

	do
	{
		track=f_GetNextEvent();
		if (track==NULL || track->next_tick_time>=seek_tick_time)
		{
			if (last_tick_time<seek_tick_time)
			{
				last_tick_time=seek_tick_time;
			}
			return 0;
		}
		
		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		
		if (track->event.byte[3]==MEVT_TEMPO)
		{
			if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
			{
				return 1;
			}
			*buf_dw++ =0;
			*buf_dw++ =0;
			*buf_dw++ =track->event.dw;
			mh->dwBytesRecorded+=3*sizeof(DWORD);
		}
		else if (track->event.byte[3]==MEVT_SHORTMSG)
		{
			if ((track->event.byte[0] & 0xF0)==0xB0 ||
				(track->event.byte[0] & 0xF0)==0xC0 )
			{
				if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
				{
					return 1;
				}
				*buf_dw++ =0;
				*buf_dw++ =0;
				*buf_dw++ =track->event.dw;
				mh->dwBytesRecorded+=3*sizeof(DWORD);
			}
		}
		else if (track->event.byte[3]==MEVT_LONGMSG)
		{
			DWORD length;
			length=track->params;
			if (track->event.byte[0]==(BYTE)0xF0)
			{
				length++;
			}
			DWORD rounded = (length + 3) & (~3L);
			if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD)+rounded)
			{
				return 1;
			}
			*buf_dw++ =0;
			*buf_dw++ =0;
			*buf_dw++ =(track->event.dw & 0xFF000000L) | (length & 0x00FFFFFFL);

			LPBYTE buf_b=(LPBYTE)buf_dw ;
			
			if (track->event.byte[0]==(BYTE)0xF0)
			{
				*buf_b++=(BYTE)0xF0;
			}
			memcpy(buf_b,track->param_pos,track->params);
			buf_b+=track->params;
			memset(buf_b,0,rounded-length);
			
			mh->dwBytesRecorded+=3*sizeof(DWORD) + rounded;
			buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		}
		last_tick_time=track->next_tick_time;
		last_tick++;
		track->f_LoadEvent();
	}while(1);
	return 0;
}

int CSmf::f_Seek2(MIDIHDR *mh,DWORD seek_tick_time,int mode)
{
	CSmfTrack *track;
	LPDWORD buf_dw;
	int i,j;

	switch (seek_status)
	{
	case 0:
		for (i=0 ; i<16 ; i++)
		{
			program_change_state[i]=0;
			for (j=0 ; j<120 ; j++)
			{
				control_change_state[i][j]=0;
			}
		}
		tempo_state=0;
	case 1:
		seek_status=1;
		do
		{
			track=f_GetNextEvent();
			if (track==NULL || track->next_tick_time>=seek_tick_time)
			{
				if (last_tick_time<seek_tick_time)
				{
					last_tick_time=seek_tick_time;
				}
				break;
			}
	
			buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
	
			if (track->event.byte[3]==MEVT_TEMPO)
			{
				tempo_state=track->event.dw;
			}
			else if (track->event.byte[3]==MEVT_SHORTMSG)
			{
				BYTE ms=track->event.byte[0]&0xF0;
				BYTE ch=track->event.byte[0]&0x0F;
				BYTE no=track->event.byte[1]&0x7F;
				if (ms==0xC0) // program change
				{
					program_change_state[ch]=track->event.dw;
				}
				else if (ms==0xB0 && no<120) // control change
				{
					switch (no)
					{
					case 6:  // data entry MSB
					case 38: // data entry LSB
					case 96: // data increment
					case 97: // data decrement
					case 98: // NRPN LSB
					case 99: // NRPN MSB
					case 100:// RPN LSB
					case 101:// RPN MSB
						if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
						{
							return 1;
						}
						*buf_dw++ =0;
						*buf_dw++ =0;
						*buf_dw++ =track->event.dw;
						mh->dwBytesRecorded+=3*sizeof(DWORD);
						break;
					default:
						control_change_state[ch][no]=track->event.dw;
						break;
					}
				}
				else if (mode==0 && ms==0xB0) // mode massage, normal mode only
				{
					if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
					{
						return 1;
					}
					*buf_dw++ =0;
					*buf_dw++ =0;
					*buf_dw++ =track->event.dw;
					mh->dwBytesRecorded+=3*sizeof(DWORD);
				}
			}
			else if (mode==0 && track->event.byte[3]==MEVT_LONGMSG) // SysEx, normal mode only
			{
				DWORD length;
				length=track->params;
				if (track->event.byte[0]==(BYTE)0xF0)
				{
					length++;
				}
				DWORD rounded = (length + 3) & (~3L);
				if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD)+rounded)
				{
					return 1;
				}
				*buf_dw++ =0;
				*buf_dw++ =0;
				*buf_dw++ =(track->event.dw & 0xFF000000L) | (length & 0x00FFFFFFL);
	
				LPBYTE buf_b=(LPBYTE)buf_dw ;
				
				if (track->event.byte[0]==(BYTE)0xF0)
				{
					*buf_b++=(BYTE)0xF0;
				}
				memcpy(buf_b,track->param_pos,track->params);
				buf_b+=track->params;
				memset(buf_b,0,rounded-length);
				
				mh->dwBytesRecorded+=3*sizeof(DWORD) + rounded;
				buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
			}
			last_tick_time=track->next_tick_time;
			last_tick++;
			track->f_LoadEvent();
		}while(1);
	case 2:
		seek_status=2;
		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		// tempo
		if (tempo_state!=0)
		{
			if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
			{
				return 1;
			}
			*buf_dw++ =0;
			*buf_dw++ =0;
			*buf_dw++ =tempo_state;
			mh->dwBytesRecorded+=3*sizeof(DWORD);
			tempo_state=0;
		}

		for (i=0 ; i<16 ; i++)
		{
			// control change MSB + LSB
			for (j=0 ; j<32 ; j++)
			{
				if (control_change_state[i][j]!=0)
				{
					if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
					{
						return 1;
					}
					*buf_dw++ =0;
					*buf_dw++ =0;
					*buf_dw++ =control_change_state[i][j];
					mh->dwBytesRecorded+=3*sizeof(DWORD);
					control_change_state[i][j]=0;
				}
				if (control_change_state[i][j+32]!=0)
				{
					if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
					{
						return 1;
					}
					*buf_dw++ =0;
					*buf_dw++ =0;
					*buf_dw++ =control_change_state[i][j+32];
					mh->dwBytesRecorded+=3*sizeof(DWORD);
					control_change_state[i][j+32]=0;
				}
				// program change
				if (j==0 && program_change_state[i]!=0)
				{
					if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
					{
						return 1;
					}
					*buf_dw++ =0;
					*buf_dw++ =0;
					*buf_dw++ =program_change_state[i];
					mh->dwBytesRecorded+=3*sizeof(DWORD);
					program_change_state[i]=0;
				}
			}
			// control change 7bit and mode massage
			for (j=32 ; j<120 ; j++)
			{
				if (control_change_state[i][j]!=0)
				{
					if (mh->dwBufferLength - mh->dwBytesRecorded < 3*sizeof(DWORD))
					{
						return 1;
					}
					*buf_dw++ =0;
					*buf_dw++ =0;
					*buf_dw++ =control_change_state[i][j];
					mh->dwBytesRecorded+=3*sizeof(DWORD);
					control_change_state[i][j]=0;
				}
			}
		}
		seek_status=0;
		break;
	default:
		seek_status=0;
		return 1;
		break;
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////
// 構築/消滅
//////////////////////////////////////////////////////////////////////

CMusicMIDI::CMusicMIDI()
{
	m_object_type=1;	// midi data
	m_header_num=4;
	m_header_size=4;  // 4 kbype, min 2kbyte
	m_smf=new CSmf;
	m_mh=NULL;

//	m_open_flag=CALLBACK_WINDOW;
}

CMusicMIDI::~CMusicMIDI()
{
	f_UnLoad();
	delete m_smf;
	f_SetStatusText(0,"CMusicMIDI deleted");
//	Sleep(1000);
}


//////////////////////////////////////////////////////////////////////
// MIDI データ演奏
//////////////////////////////////////////////////////////////////////
int CMusicMIDI::f_UnLoad()
{
	// close MIDI output device if open
	f_Close();

	// free MIDI stream buffer
	if (m_mh!=NULL)
	{
		for (int i=0 ; i<m_header_num ; i++)
		{
			SdpFreeBuffer(m_mh[i].lpData);
		}
		SdpFreeBuffer((LPSTR)m_mh);
		m_mh=NULL;
	}
	m_nbuf=0;

	// unload smf
	m_smf->f_UnLoad();

	m_music_info=_T("");

	m_stat_load=0;
	m_status_text0="not loaded";
	m_status_text1="";
	f_SetStatusText();
	return 1;
}

int CMusicMIDI::f_Load()
{
	f_UnLoad();

	// alloc buffer and load new smf file
	m_status_text0="loading ...";
	m_status_text1=m_filename;
	f_SetStatusText();

	CString cstr="";
	cstr=m_smf->f_Load(m_filename);
	if (cstr!="")
	{
		cstr="loading failed ("+cstr+")";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	if (m_smf->header.format>1)
	{
		cstr.Format("loading failed (can't handle format %d)",m_smf->header.format);
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	cstr=m_smf->f_Check();
	if (cstr!="")
	{
		cstr="loading failed ("+cstr+")";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}
	m_smf->f_GetInfo();

	if (m_title=="")
	{
		m_title=m_smf->name;
	}

	// alloc midi stream header

	if (NULL==(m_mh=(MIDIHDR*)SdpAllocBuffer(sizeof(MIDIHDR)*m_header_num)))
	{
		cstr="loading failed (stream header alloc error)";
		f_SetStatusText(0,cstr);
		m_status_text0="not loaded";
		return 0;
	}

	for (int i=0 ; i<m_header_num ; i++)
	{
		m_mh[i].dwBufferLength=1024*m_header_size;
		m_mh[i].dwBytesRecorded=0;
		m_mh[i].dwFlags=0;
		m_mh[i].dwOffset=0;
		m_mh[i].dwUser=0;
		if (NULL==(m_mh[i].lpData=SdpAllocBuffer(m_mh[i].dwBufferLength)))
		{
			while(i>0)
			{
				SdpFreeBuffer(m_mh[--i].lpData);
			}
			SdpFreeBuffer((LPSTR)m_mh);
			m_mh=NULL;
			cstr="loading failed (stream buffer alloc error)";
			f_SetStatusText(0,cstr);
			m_status_text0="not loaded";
			return 0;
		}
	}
	m_nbuf=0;

	m_length=m_smf->final_tick_time*m_smf->sec_per_tick_time;
	m_start=0.0;
	m_end=m_length;
	if (m_rep_start=="")
	{
		m_return_mark=m_start;
	}
	else
	{
		m_return_mark=SdpSetStringToTime(m_rep_start,2)/1000.0;
	}

	if (m_rep_end=="")
	{
		m_repeat_mark=m_end;
	}
	else
	{
		m_repeat_mark=SdpSetStringToTime(m_rep_end,2)/1000.0;
	}

	m_stat_load=1;
	f_SetStartTime(m_start-m_wait_time);
	f_SetEndTime(m_end);
	f_SetRepTime(m_return_mark,m_repeat_mark);
	f_SetSpeed();
	f_SetTempo();
	f_SetPitch();
	f_SetMusicInfo();
	
	m_status_text0="loading OK";
	f_SetStatusText();
	return 1;
}

void CMusicMIDI::f_SetMusicInfo(void)
{
	if (m_smf->time_format==1)
	{
		m_music_info.Format(" -- MIDI %dtr %dtk %dfr %ddiv --",m_smf->header.track_num,m_smf->final_tick,m_smf->time_frame,m_smf->header.division&0x00FF);
	}
	else
	{
		m_music_info.Format(" -- MIDI %dtr %dtk %ddiv --",m_smf->header.track_num,m_smf->final_tick,m_smf->header.division);
	}
}

int CMusicMIDI::f_Close(void)
{
	if (m_stat_ready)
	{
		f_Reset();
		if (m_fadeout_vol>0)
		{
			f_SetVolume(m_fadeout_vol);
			m_fadeout_vol=0;
		}
		midiStreamClose(m_hmo);
		m_stat_ready=0;
		f_SetStatusText(0,"midi closed");
		m_status_text0="not ready";
		m_output_device=_T("");
//		Sleep(1000);
	}
	// clear reapeat status
	m_repeat_num=0;
	m_repeat_mod=0.0;

	return 1;
}

int CMusicMIDI::f_Open(void)
{
	if (!m_stat_load) 
	{
		f_SetStatusText(0,"not loaded");
		return 0;
	}

	// close midi output device if open
	f_Close();

	// open midi output device
	UINT id;
	id=MIDI_MAPPER;
//	m_open_flag=CALLBACK_WINDOW;
	switch (m_open_flag)
	{
	  case CALLBACK_FUNCTION:
		m_mmres=midiStreamOpen(&m_hmo,&id,1,(DWORD)midiOutProc,(DWORD)this,CALLBACK_FUNCTION);
		break;
	  case CALLBACK_WINDOW:
		m_mmres=midiStreamOpen(&m_hmo,&id,1,(DWORD)this->m_hWnd,(DWORD)this,CALLBACK_WINDOW);
		break;
	  default:
		f_SetStatusText(0,"midiStreamOpen flag is invalid");
		return 0;
		break;
	}
	if (m_mmres!=MMSYSERR_NOERROR )
	{
		f_SetStatusText(0,"midiStreamOpen failed");
//		char err[MAXERRORLENGTH];
//		midiOutGetErrorText(m_mmres,err,MAXERRORLENGTH);
//		f_SetStatusText(0,err);
		return 0;
	}

/*	MIDIPROPTIMEDIV mptd;
	mptd.cbStruct=sizeof(mptd);
	mptd.dwTimeDiv=m_smf->header.time_division;
	m_mmres=midiStreamProperty(m_hmo,(LPBYTE)&mptd,MIDIPROP_SET|MIDIPROP_TIMEDIV);
	if (m_mmres!=MMSYSERR_NOERROR )
	{
		midiStreamClose(m_hmo);
		f_SetStatusText(0,"midiStreamProperty failed");
		return 0;
	}
*/
	MIDIOUTCAPS caps;
	midiOutGetID((HMIDIOUT)m_hmo,&id);
	midiOutGetDevCaps(id,&caps,sizeof(MIDIOUTCAPS));
	m_output_device=caps.szPname;
	m_output_device_info=" "+m_output_device;

	m_stat_ready=1;
	m_status_text0="ready";
	f_SetStatusText();
	return 1;
}

int CMusicMIDI::f_Start(void)
{
	CString str;

	if (m_stat_play)
	{
		if (!m_stat_playing) midiStreamRestart(m_hmo);
		m_stat_playing=1;
		m_status_text0="playing";
		f_SetStatusText();
		return 1;
	}
	else if (m_stat_ready)
	{
		// reset midi stream
		f_Reset();

		// set play rate
		MIDIPROPTIMEDIV mptd;
		mptd.cbStruct=sizeof(mptd);
		mptd.dwTimeDiv=(DWORD)m_smf->time_division;
		m_mmres=midiStreamProperty(m_hmo,(LPBYTE)&mptd,MIDIPROP_SET|MIDIPROP_TIMEDIV);
		if (m_mmres!=MMSYSERR_NOERROR )
		{
			f_SetStatusText(0,"midiStreamProperty failed");
			return 0;
		}

		// prepare header
		for (int i=0,nmh=0 ; i<m_header_num ; i++)
		{
			if (f_setMidiHeader(&m_mh[i]))
			{
				m_mmres=midiOutPrepareHeader((HMIDIOUT)m_hmo,&m_mh[i],sizeof(MIDIHDR));
				if (m_mmres!=MMSYSERR_NOERROR )
				{
					f_Reset();
					f_SetStatusText(0,"midiOutPrepareHeader failed");
					return 0;
				}
				nmh++;
			}
		}


		// play start
		midiStreamPause(m_hmo);
		for (i=0 ; i<nmh ; i++)
		{
			m_mmres=midiStreamOut(m_hmo,&m_mh[i],sizeof(MIDIHDR));
			if (m_mmres!=MMSYSERR_NOERROR )
			{
//				char err[MAXERRORLENGTH];
//				midiOutGetErrorText(m_mmres,err,MAXERRORLENGTH);
//				f_SetStatusText(0,err);
				f_Reset();
				f_SetStatusText(0,"midiStreamOut failed");
				return 0;
			}
			m_nbuf++;
		}

		if (m_nbuf>0)
		{
			m_stat_play=1;
			m_stat_playing=1;
			midiStreamRestart(m_hmo);
			m_status_text0="playing";
			f_SetStatusText();
			return 1;
		}
		else
		{
			return 0;
		}
	}
//  else
	f_SetStatusText(0,"not ready");
	return 0;
}

int CMusicMIDI::f_Stop(void)
{
	if (m_stat_playing)
	{
		midiStreamPause(m_hmo);
		m_stat_playing=0;
		m_status_text0="pause";
		f_SetStatusText();
		return 1;
	}
	else if (m_stat_ready)
	{
		if (f_GetTime()==0.0)
		{
			m_status_text0="ready";
			f_SetStatusText();
		}
		else
		{
			m_status_text0="stop";
			f_SetStatusText();
		}
		return 1;
	}
//	else
	f_SetStatusText(0,"not ready");
	return 0;
}

int CMusicMIDI::f_Reset(void)
{
	m_repeat_num=0;
	m_repeat_mod=0.0;
	if (m_stat_ready)
	{
		m_stat_play=0;
		m_stat_playing=0;
		while(m_stat_preparing)	{Sleep(0);}
		midiStreamStop(m_hmo);

		switch (m_open_flag)
		{
		  case CALLBACK_FUNCTION:
			while(m_nbuf)	{Sleep(0);}
			break;
		  case CALLBACK_WINDOW:
			m_nbuf=0;
			break;
		  default:
			m_nbuf=0;
			break;
		}

		for (int i=0 ; i<m_header_num ; i++)
		{
			midiOutUnprepareHeader((HMIDIOUT)m_hmo,&m_mh[i],sizeof(MIDIHDR));
			m_mh[i].dwUser=0;
		}
		m_status_text0="ready";
		f_SetStatusText();
		return 1;
	}
//	else
	f_SetStatusText(0,"not ready");
	return 0;
}

double CMusicMIDI::f_GetTime(void)
{
	MMTIME mmt;
	mmt.wType=TIME_TICKS;

//	CString str;
//	str.Format("playing headers %d",m_nbuf);
//	f_SetStatusText(0,str);


	if (m_stat_ready)
	{
		midiStreamPosition(m_hmo,&mmt,sizeof(MMTIME));
	}
	else
	{
		return 0.0;
	}

	if (mmt.wType==TIME_TICKS)
	{
		double time;
		time=(double)mmt.u.ticks*m_smf->sec_per_tick_time-(m_repeat_mark-m_return_mark)*m_repeat_num - m_repeat_mod + m_start;
		if (mmt.u.ticks > 4000000000)
		{
			if ( m_stat_play && m_stat_playing)
			{
				f_Reset();
				f_SetStartTime(time);
				f_Start();
			}
			else
			{
				f_Reset();
				f_SetStartTime(time);
			}
		}
		return time;
	}
	else
	{
		return 0.0;
	}
}

double CMusicMIDI::f_GetLength(void)
{
	return m_length;
}

int CMusicMIDI::f_SetStartTime(double start)
{
	if (m_stat_play)
	{
		return 0;
	}
	else if (m_stat_load)
	{
		SdpSetTimeToString(start,m_time,1);
		if (start<0.0)				{start=0.0;}
		else if (start>m_length)	{start=m_length;}
		m_start_tick_time=(DWORD)(start/m_smf->sec_per_tick_time) ;
		m_start=start;
		m_seek_pos=m_start_tick_time;
		m_seek_mode=0;
		m_smf->f_Rewind();
		return 1;
	}
	return 0;
}

int CMusicMIDI::f_SetEndTime(double end)
{
	if (m_stat_load)
	{
		if (end<0.0)
		{
			m_stop_tick_time=0;
			end=0.0;
		}
		else if (end>=m_length)
		{
			m_stop_tick_time=m_smf->final_tick_time;
			end=m_length;
		}
		else
		{
			m_stop_tick_time=(DWORD)(end/m_smf->sec_per_tick_time);
		}
		m_end=end;
		return 1;
	}
	return 0;
}

int CMusicMIDI::f_SetRepTime(double start,double end)
{
	if (m_stat_load)
	{
		double min=1.0;

		if (start>m_length-min)		{start=m_length-min;}
		if (start<0.0)				{start=0.0;}
		if (end<start+min)			{end=start+min;}
		if (end>m_length)			{end=m_length;}

		m_ret_tick_time=(DWORD)(start/m_smf->sec_per_tick_time);
		m_rep_tick_time=(DWORD)(end/m_smf->sec_per_tick_time);
		m_return_mark=start;
		m_repeat_mark=end;
		return 1;
	}
	return 0;
}

int CMusicMIDI::f_ChangeSpeed()
{
	f_ChangePitch();
	f_ChangeTempo();
	return 1;
}

int CMusicMIDI::f_ChangePitch()
{
	union
	{
		DWORD dw;
		BYTE  byte[4];
	} msg;

	if (m_stat_ready)
	{
		if (m_stat_playing)
		{
			midiStreamPause(m_hmo);
		}
		
		for (int i=0 ; i<16 ; i++)
		{
			msg.byte[0]=0xB0|(BYTE)i;
			msg.byte[3]=0x00;//   0
	
		// coarse tuning
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x00;//   0
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x02;//   2
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// data entry MSB 6(0x06)
			msg.byte[1]=0x06;//   6
			msg.byte[2]=m_tone_tune;  //   
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// data entry LSB 38(0x26)
			msg.byte[1]=0x26;//  38
			msg.byte[2]=0x00;//   
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
	
		// fine tuning
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x00;//   0
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x01;//   1
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// data entry MSB 6(0x06)
			msg.byte[1]=0x06;//   6
			msg.byte[2]=m_fine_tune_MSB; //   
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// data entry LSB 38(0x26)
			msg.byte[1]=0x26;//  38
			msg.byte[2]=m_fine_tune_LSB; //   
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);

		// RPN null
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x7F;// 127
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x7F;// 127
			midiOutShortMsg((HMIDIOUT)m_hmo,msg.dw);
		}

		if (m_stat_playing)
		{
			midiStreamRestart(m_hmo);
		}
	}
	return 1;
}

int CMusicMIDI::f_ChangeTempo()
{
	double time;

	if (m_stat_ready)
	{
		if ( m_stat_play && m_stat_playing)
		{
			f_Stop();
			time=f_GetTime();
			f_Reset();
			f_SetStartTime(time);
			return f_Start();
		}
		else
		{
			f_Stop();
			time=f_GetTime();
			f_Reset();
			f_SetStartTime(time);
		}
	}
	return 1;
}

void CMusicMIDI::f_SetSpeed()
{
	f_SetPitch();
	f_SetTempo();
}

void CMusicMIDI::f_SetTempo()
{
	m_speed_change=1.0;
	m_tempo_change=(1.0+m_speed/1000.0)*(1.0+m_tempo/1000.0);
	if (m_tempo_change<0.1)			{m_tempo_change=0.1;}
	else if (m_tempo_change>10.0)	{m_tempo_change=10.0;}

	if (m_stat_load)
	{
	    if (m_smf->time_format==1)
	    {
			m_smf->time_division= (m_smf->header.division & 0xFF00) |
								(BYTE)((m_smf->header.division & 0x00FF)*m_tempo_change);
			m_tempo_bpm=m_tempo_bpm0*(double)(m_smf->time_division & 0x00FF)/(double)(m_smf->header.division & 0x00FF);
		}
		else
		{
			m_smf->time_division=(WORD)(m_smf->header.division*m_tempo_change);
			m_tempo_bpm=m_tempo_bpm0*(double)m_smf->time_division/(double)m_smf->header.division;
		}
	}
	else
	{
		m_tempo_bpm=m_tempo_bpm0*m_tempo_change;
	}
}

void CMusicMIDI::f_SetPitch()
{
	WORD p1;
	double pitch;

	m_pitch_change=m_pitch/10.0
					+log(1.0+m_speed/1000.0)*6.0/log(2.0);

	if (m_pitch_change>24.0)	{m_pitch_change=24.0;}
	if (m_pitch_change<-24.0)	{m_pitch_change=-24.0;}

	pitch=m_pitch_change*2.0;
	m_tone_tune=0x40 + (int)(pitch);
	
	pitch=(pitch - (int)pitch)*8192.0;
	p1=(0x40 << 7) + (int)pitch;
	m_fine_tune_MSB = (p1 & 0x3F80)>>7 ;
	m_fine_tune_LSB = (p1 & 0x007F) ;
}

double CMusicMIDI::f_GetTempoInFile()
{
	return 	m_smf->tempo_base;
}

int CMusicMIDI::f_GetVolume(DWORD *vol)
{
	return midiOutGetVolume((HMIDIOUT)m_hmo,vol);
}

int CMusicMIDI::f_SetVolume(DWORD vol)
{
	return midiOutSetVolume((HMIDIOUT)m_hmo,vol);
}

//////////////////////////////////////////////////////////////////////
// wave header operation
//////////////////////////////////////////////////////////////////////

int CMusicMIDI::f_setMidiHeader(MIDIHDR *mh)
{
	DWORD end_pos;
	LPDWORD buf_dw;
	union
	{
		DWORD dw;
		BYTE  byte[4];
	} msg;

	if (mh->dwUser>0)
	{
		m_repeat_num++;
		m_repeat_mod+=m_smf->sec_per_tick_time*(double)(mh->dwUser-1);
	}
	mh->dwUser=0;

	if (m_stat_repeat)
	{
		end_pos=m_rep_tick_time;
	}
	else
	{
		end_pos=m_stop_tick_time;
	}

//	CString str;
//	DWORD last_tick_time=m_smf->last_tick_time;

	mh->dwBytesRecorded=0;

	if (m_smf->f_Seek(mh,m_seek_pos,m_seek_mode))
	{
//		str.Format("seek(hd%d %d) %d -> %d,%d",mh-m_mh,mh->dwBytesRecorded,last_tick_time,m_smf->last_tick_time,m_smf->next_tick_time);
//		f_SetStatusText(1,str);
//		Sleep(1000);
		return 1;
	}
	m_seek_mode=0;

	int res=m_smf->f_ReadEvent(mh,end_pos);
	if (res==2 && // fist tone on is detected
		mh->dwBufferLength - mh->dwBytesRecorded >= 3*sizeof(DWORD)*10*16 )
	{
		// set pitch
		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);

		for (int i=0 ; i<16 ; i++)
		{
			msg.byte[0]=0xB0|(BYTE)i;
			msg.byte[3]=MEVT_SHORTMSG;
	
		// coarse tuning
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x00;//   0
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x02;//   2
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// data entry MSB 6(0x06)
			msg.byte[1]=0x06;//   6
			msg.byte[2]=m_tone_tune;  //   
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// data entry LSB 38(0x26)
			msg.byte[1]=0x26;//  38
			msg.byte[2]=0x00;//   
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
	
		// fine tuning
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x00;//   0
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x01;//   1
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// data entry MSB 6(0x06)
			msg.byte[1]=0x06;//   6
			msg.byte[2]=m_fine_tune_MSB; //   
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// data entry LSB 38(0x26)
			msg.byte[1]=0x26;//  38
			msg.byte[2]=m_fine_tune_LSB; //   
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			
		// RPN null
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x7F;// 127
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x7F;// 127
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
		}
		mh->dwBytesRecorded += 3*sizeof(DWORD)*10*16;
		m_smf->first_note_on=1;
	}
	else if (res==0 && // all data is read
			mh->dwBufferLength - mh->dwBytesRecorded >= 3*sizeof(DWORD) )
	{
		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		if (m_smf->last_tick_time < end_pos )
		{
			*buf_dw++= end_pos - m_smf->last_tick_time ;
			*buf_dw++= 0 ;
			*buf_dw++= ((DWORD)MEVT_NOP)<<24 ;
			mh->dwBytesRecorded += 3*sizeof(DWORD);
			m_smf->last_tick_time=end_pos;
		}
	}

	if ( mh->dwBytesRecorded>0 )
	{
//		str.Format("read(hd%d %d) %d -> %d,%d",mh-m_mh,mh->dwBytesRecorded,last_tick_time,m_smf->last_tick_time,m_smf->next_tick_time);
//		f_SetStatusText(1,str);
//		Sleep(1000);
		return 1;
	}
	else if (m_stat_repeat)
	{
		mh->dwBytesRecorded=0;
		buf_dw=(LPDWORD)(mh->lpData + mh->dwBytesRecorded);
		
		mh->dwUser=1;
		if (m_smf->last_tick_time >= end_pos )
		{
			mh->dwUser+= m_smf->last_tick_time - end_pos ;
		}
		else
		{
			*buf_dw++= end_pos - m_smf->last_tick_time ;
			*buf_dw++= 0 ;
			*buf_dw++= ((DWORD)MEVT_NOP)<<24 ;
			mh->dwBytesRecorded += 3*sizeof(DWORD);
		}
		
		for (int i=0 ; i<16 ; i++)
		{
			msg.byte[0]=0xB0|(BYTE)i;
			msg.byte[3]=MEVT_SHORTMSG;
			
			// all sound off Bn 120 0
			// all note off  Bn 123 0
			msg.byte[1]=123;
			msg.byte[2]=0;
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			
			// RPN null
			// RPN MSB 101(0x65)
			msg.byte[1]=0x65;// 101
			msg.byte[2]=0x7F;// 127
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			// RPN LSB 100(0x64)
			msg.byte[1]=0x64;// 100
			msg.byte[2]=0x7F;// 127
			*buf_dw++= 0;
			*buf_dw++= 0 ;
			*buf_dw++= msg.dw;
			
			mh->dwBytesRecorded += 3*sizeof(DWORD)*3;
		}
		m_seek_pos=m_ret_tick_time;
		if (m_stop_tick_time<m_rep_tick_time)
		{
			m_stat_repeat=0;
		}
		BOOL note_on_state=m_smf->first_note_on;
		m_smf->f_Rewind();
		m_smf->first_note_on=note_on_state;
		m_seek_mode=1; // repeat mode
		if (m_smf->f_Seek(mh,m_seek_pos,m_seek_mode)==0)
		{
			m_seek_mode=0;
		}

//		str.Format("repeat(hd%d %d) %d -> %d,%d",mh-m_mh,mh->dwBytesRecorded,last_tick_time,m_smf->last_tick_time,m_smf->next_tick_time);
//		f_SetStatusText(1,str);
//		Sleep(1000);
		return 1;
	}

//	str.Format("done(hd%d %d) %d -> %d,%d",mh-m_mh,mh->dwBytesRecorded,last_tick_time,m_smf->last_tick_time,m_smf->next_tick_time);
//	f_SetStatusText(1,str);
//	Sleep(1000);
	return 0;

}

//////////////////////////////////////////////////////////////////////
// midiOutProc
//////////////////////////////////////////////////////////////////////
void CMusicMIDI::f_OutProcDone(WPARAM wParam, LPARAM lParam)
{
	HMIDIOUT hmo=(HMIDIOUT)wParam;
	MIDIHDR *mh=(MIDIHDR*)lParam;
//	CString str;

//	str.Format("done(hd%d) %d",mh - m_mh,m_nbuf);
//	str.Format("playing (buf %d)",m_nbuf);
//	f_SetStatusText(0,str);

//	midiOutProc(hmo,MOM_DONE,(DWORD)this,(DWORD)lParam,0);
//	Sleep(1000);

	while(m_stat_preparing)	{Sleep(0);}
	m_stat_preparing=1;
	if ( m_stat_play )
	{
		midiOutUnprepareHeader(hmo,mh,sizeof(MIDIHDR));
		if ( f_setMidiHeader(mh) )
		{
			midiOutPrepareHeader(hmo,mh,sizeof(MIDIHDR));
			if (MMSYSERR_NOERROR==midiStreamOut((HMIDISTRM)hmo,mh,sizeof(MIDIHDR)))
			{
				m_nbuf++;
			}
		}

		if (m_nbuf<=0)
		{
			m_stat_play=0;
			m_stat_playing=0;
			midiStreamPause((HMIDISTRM)hmo);
		}
	}
	
	if (m_nbuf<0)
	{
		m_nbuf=0;
	}

//	str.Format("playing (buf %d)",m_nbuf);
//	f_SetStatusText(0,str);

	m_stat_preparing=0;

}

void CALLBACK midiOutProc
( HMIDIOUT hmo,      
   UINT uMsg,         
   DWORD dwInstance,  
   DWORD dwParam1,    
   DWORD dwParam2   )
{
//	MIDIHDR *mh;
	CMusicMIDI *midi;

 	switch (uMsg)
	{
		case MOM_OPEN :
			break;
		case MOM_CLOSE :
			break;
		case MOM_DONE :
//			mh=(MIDIHDR *)dwParam1;
			midi=(CMusicMIDI*)dwInstance;
			midi->m_nbuf--;
			if ( midi->m_stat_play )
			{
				midi->f_OutProcDone((WPARAM)hmo,(LPARAM)dwParam1);
				::PostMessage(midi->m_hWnd,MM_MOM_DONE,(WPARAM)hmo,(LPARAM)dwParam1);
			}
/*
			while(midi->m_stat_preparing)	{Sleep(0);}
			midi->m_stat_preparing=1;
			if ( midi->m_stat_play )
			{
				midiOutUnprepareHeader(hmo,mh,sizeof(MIDIHDR));
				if ( midi->f_setMidiHeader(mh) )
				{
					midiOutPrepareHeader(hmo,mh,sizeof(MIDIHDR));
					if (MMSYSERR_NOERROR==midiStreamOut((HMIDISTRM)hmo,mh,sizeof(MIDIHDR)))
					{
						midi->m_nbuf++;
					}
				}

				if (midi->m_nbuf<=0)
				{
					midi->m_stat_play=0;
					midi->m_stat_playing=0;
					midiStreamPause((HMIDISTRM)hmo);
				}
			}
*/
			if (midi->m_nbuf<0)
			{
				midi->m_nbuf=0;
			}
//			midi->m_stat_preparing=0;
 			break;
	  default:
			break;
	}
	return;
}
