#if !defined(AFX_SLIDER_H__E2792A32_2D3F_11D7_BC30_009027BFCD6A__INCLUDED_)
#define AFX_SLIDER_H__E2792A32_2D3F_11D7_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Slider.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSlider ウィンドウ

class CSlider : public CSliderCtrl
{
	DECLARE_DYNAMIC(CSlider)
// コンストラクション
public:
	CSlider();

// アトリビュート
public:

// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSlider)
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CSlider();

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CSlider)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SLIDER_H__E2792A32_2D3F_11D7_BC30_009027BFCD6A__INCLUDED_)
