#if !defined(AFX_DLGBAR_H__0F6633E1_826D_11D5_BC30_009027BFCD6A__INCLUDED_)
#define AFX_DLGBAR_H__0F6633E1_826D_11D5_BC30_009027BFCD6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgBar.h : ヘッダー ファイル
//

#include "TimeDisp.h"
#include "Slider.h"
#include "ColorButton.h"

/////////////////////////////////////////////////////////////////////////////
// CDlgBar ウィンドウ

class CDlgBar : public CDialogBar
{
// コンストラクション
public:
	CDlgBar();

// アトリビュート
public:

// オペレーション
public:
	void f_InitDialog();
	void f_RestartDialog();
	void f_CreateSubWindow();
	void f_DestroySubWindow();
	void f_set_cb_string(class CSdpProfile *profile,class CMusic *music,CTime *time,CString &res);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CDlgBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~CDlgBar();

	// ダイアログ データ
	CSlider	m_time_slider;
	CSlider	m_speed_slider;
	CSlider	m_pitch_slider;
	CSlider	m_tempo_slider;
	CSpinButtonCtrl	m_resttime_updown;
	CSpinButtonCtrl	m_tiptime_updown;
	CString	m_tip_timer;
	int m_rest_time;
	int m_tip_time;
	
	CColorButton m_patter_radio;
	CColorButton m_singing_radio;
	CColorButton m_go_button;
	CColorButton m_edit_button;
	CColorButton m_add_button;
	CColorButton m_unload_button;
	CColorButton m_load_button;
	CColorButton m_rewind_button;
	CColorButton m_play_button;
	CColorButton m_repeat_button;
	CColorButton m_reset_button;

	// Music Data
	class CMusic *music;
	class CMusic *music_data[2];
	int m_music_sel;
	void f_MusicSelect(int sel);
	class CSdPlayerView *list_data[2];

	// Tip Timer
	int m_timer_mode;
	int m_timer_state;
	int m_timer_count;
	CTimeDisp *m_pwndTimeDisp;

	// メッセージ ハンドラ
	void OnEditData();
	void OnAddData();
	void OnRADIOPatter();
	void OnRADIOSinging();
	void OnLoad();
	void OnUnLoad();
	void OnRewind();
	void OnPlay();
	void OnRepeat();
	void OnReset();
	void OnTipTimer();
	void OnPlayByKey();
	void OnRepeatByKey();
	void OnCueSheet();

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CDlgBar)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
public:
	afx_msg void OnUpdateUnLoad(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLoad(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRewind(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePlay(CCmdUI* pCmdUI);
	afx_msg void OnUpdateRepeat(CCmdUI* pCmdUI);
	afx_msg void OnUpdateReset(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePatter(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSinging(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTipTimer(CCmdUI* pCmdUI);
	afx_msg void OnUpdateTimerMode(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSlider(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCueSheet(CCmdUI* pCmdUI);
	//
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_DLGBAR_H__0F6633E1_826D_11D5_BC30_009027BFCD6A__INCLUDED_)
