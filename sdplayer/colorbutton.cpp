// ColorButton.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "sdplayer.h"
#include "ColorButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorButton

IMPLEMENT_DYNAMIC(CColorButton, CButton)

CColorButton::CColorButton()
{
}

CColorButton::~CColorButton()
{
}

BOOL CColorButton::AutoLoad(const UINT nID, CWnd* pParent)
{
	// ダイナミックサブクラス化
	if (!SubclassDlgItem(nID, pParent))
	{
		return FALSE;
	}
	return TRUE;
}

BEGIN_MESSAGE_MAP(CColorButton, CButton)
	//{{AFX_MSG_MAP(CColorButton)
	ON_WM_PAINT()
	ON_WM_LBUTTONUP()
	ON_WM_KEYUP()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorButton メッセージ ハンドラ

void CColorButton::OnPaint() 
{

	CButton::OnPaint();
	f_Repaint();

	//CPaintDC dc(this); // 描画用のデバイス コンテキスト
	
	// TODO: この位置にメッセージ ハンドラ用のコードを追加してください
	
	// 描画用メッセージとして CButton::OnPaint() を呼び出してはいけません
}

void CColorButton::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	
	CButton::OnLButtonUp(nFlags, point);
	f_Repaint();
}

void CColorButton::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	
	CButton::OnKeyUp(nChar, nRepCnt, nFlags);
	f_Repaint();
}

void CColorButton::f_Repaint()
{
	if (GetFocus()==this)
	{
		CDC *dc=GetDC();
		CBrush br;
		RECT rect;

		br.CreateSolidBrush(RGB(255,0,0));
		GetClientRect(&rect);
		dc->FrameRect(&rect,&br);

		ReleaseDC(dc);
	}
}
