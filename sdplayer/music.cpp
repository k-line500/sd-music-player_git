// Music.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "SdPlayer.h"
#include "Music.h"
#include "MusicWave.h"
#include "MusicMIDI.h"
#include "MusicMPEG.h"
#include "MainFrm.h"

#include "Global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Global function
/////////////////////////////////////////////////////////////////////////////

LPSTR SdpAllocBuffer(DWORD size)
{
	return (LPSTR)calloc(1,size);
}

LPSTR SdpFreeBuffer(LPSTR buf)
{
	if (buf!=NULL)
	{
		free(buf);
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CMusic

CMusic::CMusic()
{
	EnableAutomation();
	m_object_type=-1;
	m_header_size=0;
	m_header_num=0;
	m_dialog=NULL;
	m_open_flag=CALLBACK_FUNCTION;

	m_status_text0=_T("not loaded");
	m_status_text1=_T("");

	m_stat_playing=0;
	m_stat_play=0;
	m_stat_ready=0;
	m_stat_load=0;
	m_stat_repeat=0;
	m_stat_preparing=0;
	m_dsp_on=1;
	f_InitData();

}

CMusic::~CMusic()
{
}

void CMusic::OnFinalRelease()
{
	// オートメーション オブジェクトの最後の参照が解放された時に
	// OnFinalRelease が呼び出されます。基本クラスは自動的にオブジェクト 
	// を破棄します。オブジェクトをメモリから破棄する前に必要な
	// 終了処理があれば追加して下さい。

	CWnd::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CMusic, CWnd)
	//{{AFX_MSG_MAP(CMusic)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
	ON_MESSAGE(MM_WOM_DONE,OnWaveOutDone)
	ON_MESSAGE(MM_MOM_DONE,OnMidiOutDone)
END_MESSAGE_MAP()


BEGIN_DISPATCH_MAP(CMusic, CWnd)
	//{{AFX_DISPATCH_MAP(CMusic)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// メモ: IID_IMusic に対して VBA からタイプセーフ バインディングをサポートするため
// のサポ ートを追加します。  この IID は .ODL ファイルのディスプインターフェイ
// スにアタッチされている GUID とマッチしなければなりません。

// {CDF70E04-B419-11D5-BC30-009027BFCD6A}
static const IID IID_IMusic =
{ 0xcdf70e04, 0xb419, 0x11d5, { 0xbc, 0x30, 0x0, 0x90, 0x27, 0xbf, 0xcd, 0x6a } };

BEGIN_INTERFACE_MAP(CMusic, CWnd)
	INTERFACE_PART(CMusic, IID_IMusic, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMusic メンバ関数
void CMusic::f_SetStatusText(void)
{
	f_SetStatusText(0,m_status_text0);
	f_SetStatusText(1,m_status_text1);
}

void CMusic::f_SetStatusText(int no,const char *text)
{
	CMainFrame *wnd;
	if (wnd=(CMainFrame*)AfxGetMainWnd())
	{
		wnd->f_SetStatusText(no,text);
	}
}

int CMusic::f_GetObjectType()
{
	return m_object_type;
}

CMusic* CMusic::f_ObjectTypeChange(CMusic *music)
{
	if (music->m_object_type==music->m_data_type || music->m_data_type ==-1)
	{
		return music;
	}

	CMusic *music_new;

	if (music->m_data_type==1)
	{
		music_new=new CMusicMIDI;
	}
	else if (music->m_data_type==2)
	{
		music_new=new CMusicMPEG;
	}
	else
	{
		music_new=new CMusicWave;
	}
	
	music_new->Attach(music->Detach());
	
	music_new->m_base_dir      =music->m_base_dir;
	music_new->m_dialog        = music->m_dialog;

	music_new->m_title         = music->m_title;
	music_new->m_label         = music->m_label;
	music_new->m_label_no      = music->m_label_no;
	music_new->m_category      = music->m_category;
	music_new->m_auto_repeat   = music->m_auto_repeat;
	music_new->m_rep_start     = music->m_rep_start;
	music_new->m_rep_end       = music->m_rep_end;
	music_new->m_wait_time     = music->m_wait_time;
	music_new->m_tempo_bpm     = music->m_tempo_bpm;
	music_new->m_speed         = music->m_speed;
	music_new->m_pitch         = music->m_pitch;
	music_new->m_tempo         = music->m_tempo;
	music_new->m_data_type     = music->m_data_type;
	music_new->m_filename      = music->m_filename;
	music_new->m_rep_mix       = music->m_rep_mix;
	music_new->m_tempo_org     = music->m_tempo_org;
	music_new->m_beats_per_rep = music->m_beats_per_rep;
	music_new->m_data_num      = music->m_data_num;
	music_new->m_flags         = music->m_flags;
	music_new->m_cue_file      = music->m_cue_file;
	music_new->m_cue_prog      = music->m_cue_prog;

	music_new->f_SetDialogData();

	delete music;
	return music_new;
}

int CMusic::f_GetFileType()
{
 	HMMIO    hmmio ;
	MMIOINFO mmioinfo;
	LPCSTR filename=m_filename;
    MMCKINFO ckRIFF;
    MMCKINFO ckinfo;
	WORD format_tag=0;
	char b;
	DWORD offset;
	CString str;

	SetCurrentDirectory(m_base_dir);
	m_status_text1=filename;
	f_SetStatusText(1,filename);

    memset(&mmioinfo, 0, sizeof(MMIOINFO));
    hmmio = mmioOpen((LPSTR)filename, &mmioinfo, MMIO_READ|MMIO_ALLOCBUF);

    if (hmmio==(HMMIO)NULL)
    {
		switch (mmioinfo.wErrorRet)
		{
		case MMIOERR_FILENOTFOUND:
			str="file is not found";
			break;
		case MMIOERR_OUTOFMEMORY:
			str="out of memory";
			break;
		case MMIOERR_CANNOTOPEN:
			str="cannot open";
			break;
		case MMIOERR_PATHNOTFOUND:
			str="path is incorrect";
			break;
		case MMIOERR_ACCESSDENIED:
			str="file is protected";
			break;
		case MMIOERR_SHARINGVIOLATION:
			str="file in use";
			break;
		case MMIOERR_NETWORKERROR:
			str="network is not responding";
			break;
		case MMIOERR_TOOMANYOPENFILES:
			str="no more file handles";
			break;
		case MMIOERR_INVALIDFILE:
			str="invalid file";
			break;
		default:
			str.Format("file open error (%d)",mmioinfo.wErrorRet);
			break;
		}
		mmioClose(hmmio, 0);
		m_status_text0=str;
		f_SetStatusText(0,str);
		return -1;
    }

	offset=0;

	if (3==mmioRead(hmmio,(HPSTR)&ckinfo.fccType, 3))
	{
		if ((ckinfo.fccType & 0x00FFFFFF)==mmioFOURCC('I','D','3',0))
		{
			if (1!=mmioRead(hmmio,&b,1)) // version
			{
				mmioClose(hmmio, 0);
				str="invalid ID3 header";
				m_status_text0=str;
				f_SetStatusText(0,str);
				return -1; //unknown
			}
			if (1!=mmioRead(hmmio,&b,1)) // revision
			{
				mmioClose(hmmio, 0);
				str="invalid ID3 header";
				m_status_text0=str;
				f_SetStatusText(0,str);
				return -1; //unknown
			}
			if (1!=mmioRead(hmmio,&b,1)) // frags
			{
				mmioClose(hmmio, 0);
				str="invalid ID3 header";
				m_status_text0=str;
				f_SetStatusText(0,str);
				return -1; //unknown
			}

			ckinfo.cksize=0;
			for (int i=0 ; i<4 ; i++)
			{
				if (1!=mmioRead(hmmio,&b,1))
				{
					mmioClose(hmmio, 0);
					str="invalid ID3 header";
					m_status_text0=str;
					f_SetStatusText(0,str);
					return -1; //unknown
				}
				ckinfo.cksize=(ckinfo.cksize<<7)|(b & 0x7F);
			}
			offset+=ckinfo.cksize+10;
		}
	}
	mmioSeek(hmmio,offset,SEEK_SET);

	for ( ; 1==mmioRead(hmmio,&b,1) && b==0 ; offset++);
	mmioSeek(hmmio,offset,SEEK_SET);

	if (4==mmioRead(hmmio,(HPSTR)&ckinfo.fccType, 4))
	{
		if (ckinfo.fccType==mmioFOURCC('M','T','h','d'))
		{
			mmioClose(hmmio, 0);
			str="file type is MIDI";
			m_status_text0=str;
			f_SetStatusText(0,str);
			return 1; // midi smf
		}
		else if ((ckinfo.fccType & 0x0000E0FF) == 0x0000E0FF)
		{
			mmioClose(hmmio, 0);
			str="file type is MPEG";
			m_status_text0=str;
			f_SetStatusText(0,str);
			return 2; // mpeg audio
		}

	}
	else
	{
		mmioClose(hmmio, 0);
		str="unknown file type";
		m_status_text0=str;
		f_SetStatusText(0,str);
		return -1; //unknown
	}

	mmioSeek(hmmio,offset,SEEK_SET);
	memset(&ckRIFF, 0, sizeof(ckRIFF));
	memset(&ckinfo, 0, sizeof(ckinfo));
	if (0 == mmioDescend(hmmio, &ckRIFF, NULL, MMIO_FINDRIFF))
	{
		if (ckRIFF.fccType == mmioFOURCC('W','A','V','E'))
		{
			ckinfo.ckid = mmioFOURCC('f','m','t',' ');
			if (0!=mmioDescend(hmmio, &ckinfo, &ckRIFF, MMIO_FINDCHUNK) ||
				2!=mmioRead(hmmio,(HPSTR)&format_tag, 2) )
			{
			}
			else if (format_tag == WAVE_FORMAT_PCM)
			{
				mmioClose(hmmio, 0);
				str="file type is PCM";
				m_status_text0=str;
				f_SetStatusText(0,str);
				return 0;
			}
			else if (format_tag == 85 )
			{
				mmioClose(hmmio, 0);
				str="file type is MPEG";
				m_status_text0=str;
				f_SetStatusText(0,str);
				return 2;
			}
		}
		else if (ckRIFF.fccType == mmioFOURCC('R','M','I','D'))
		{
			mmioClose(hmmio, 0);
			str="file type is RMID";
			m_status_text0=str;
			f_SetStatusText(0,str);
			return 1;
		}
		else if (ckRIFF.fccType == mmioFOURCC('R','M','P','3'))
		{
			mmioClose(hmmio, 0);
			str="file type is MPEG";
			m_status_text0=str;
			f_SetStatusText(0,str);
			return 2;
		}
	}

	mmioClose(hmmio, 0);
	str="unknown file type";
	m_status_text0=str;
	f_SetStatusText(0,str);
	return -1;
}

void CMusic::f_InitData()
{
	m_base_dir=_T("");

	m_title = _T("");
	m_label = _T("");
	m_label_no = _T("");
	m_category = _T("");
	m_auto_repeat = FALSE;
	m_rep_start = _T("");
	m_rep_end = _T("");
	m_wait_time = 0;
	m_tempo_bpm = 0.0;
	m_speed = 0;
	m_pitch = 0;
	m_tempo = 0;
	m_data_type = 0;
	m_filename=_T("");
//	m_pitch_oct = 0.0;
	m_rep_mix = 0.0;
	m_tempo_org = 0.0;
	m_beats_per_rep = 0;
	m_data_num = -1; 

	m_time = _T("");
	m_music_len = _T("");
	m_music_name = _T("");
	m_music_info=_T("");
	m_output_device=_T("");
	m_output_device_info=_T("");
	m_play_on = FALSE;
	m_tempo_bpm0 = 128.0;
	m_speed_change=1.0;
	m_pitch_change=1.0;
	m_tempo_change=1.0;
	f_SetSpeedData(m_speed);
	f_SetPitchData(m_pitch);
	f_SetTempoData(m_tempo);
	m_cue_file=_T("");
	m_cue_prog=_T("");

	m_wait = 0;
	m_nbuf = 0;
	m_fadeout=0;
	m_fadeout_len=0;
	m_fadeout_vol=0;

	m_length=0;
	m_start=0;
	m_end=0;
	m_paused_time=0;
	m_return_mark=0;
	m_repeat_mark=0;

}

void CMusic::f_SetData(CStringArray *sa)
{
	if (!sa)	{return;}

	CString dir=m_base_dir;

	f_InitData();

	m_base_dir=dir;

	m_title=sa->GetAt(0);
	m_label=sa->GetAt(1);
	m_label_no=sa->GetAt(2);
	m_category=sa->GetAt(3);
	m_auto_repeat=sa->GetAt(4)=="ON";
	m_rep_start=sa->GetAt(5);
	m_rep_end=sa->GetAt(6);
	sscanf(sa->GetAt(7),"%d",&m_wait_time);
	sscanf(sa->GetAt(8),"%lg",&m_tempo_bpm);
	sscanf(sa->GetAt(9),"%d",&m_speed);
	sscanf(sa->GetAt(10),"%d",&m_pitch);
	sscanf(sa->GetAt(11),"%d",&m_tempo);
	if (sa->GetAt(12)=="Wave")
	{
		m_data_type=0;
	}
	else if (sa->GetAt(12)=="MIDI")
	{
		m_data_type=1;
	}
	else if (sa->GetAt(12)=="MPEG")
	{
		m_data_type=2;
	}
	else
	{
		m_data_type=-1;
	}
	m_filename=sa->GetAt(13);

 	sscanf(sa->GetAt(14),"%lg",&m_rep_mix);
	sscanf(sa->GetAt(15),"%lg",&m_tempo_org);
	sscanf(sa->GetAt(16),"%d",&m_beats_per_rep);
	sscanf(sa->GetAt(17),"%d",&m_data_num);
	m_flags=sa->GetAt(18);

	int n0=0;
	int n1=sa->GetAt(19).Find(';',n0);
	if (n1>0)
	{
		m_cue_file=sa->GetAt(19).Mid(n0,n1-n0);
	}
	n0=n1+1;
	if (n0>0)
	{
		n1=sa->GetAt(19).Find(';',n0);
		if (n1>0)
		{
			m_cue_prog=sa->GetAt(19).Mid(n0,n1-n0);
		}
	}

	f_SetDialogData();
}

void CMusic::f_SetDialogData()
{
	if (m_tempo_org<1.0)
	{
		m_tempo_bpm0=128.0;
	}
	else
	{
		m_tempo_bpm0=m_tempo_org;
	}

	m_stat_repeat=m_auto_repeat;
	m_music_name=m_title+" ("+m_label+" "+m_label_no+") "+m_category;

	f_SetSpeedData(m_speed);
	f_SetPitchData(m_pitch);
	f_SetTempoData(m_tempo);
	f_SetTempo();
}

void CMusic::f_SetSpeedData(int speed)
{
	if (-500<=speed && speed<=1000)
	{
		m_speed=speed;
		m_speed_control.Format("%+g %%",m_speed/10.0);
		f_SetSpeed();
	}
}

void CMusic::f_SetPitchData(int pitch)
{
	if (-60<=pitch && pitch<=60)
	{
		m_pitch=pitch;
		m_pitch_control.Format("%+g T",m_pitch/10.0);
		f_SetPitch();
	}
}

void CMusic::f_SetTempoData(int tempo)
{
	if (-500<=tempo && tempo<=1000)
	{
		m_tempo=tempo;
		m_tempo_control.Format("%+g %%",m_tempo/10.0);
		f_SetTempo();
	}
}

void CMusic::f_SetSpeed()
{
	f_SetPitch();
	f_SetTempo();
}

void CMusic::f_SetTempo()
{
	m_tempo_change=1.0;
	m_speed_change=1.0;
	m_tempo_bpm=m_tempo_bpm0*(1.0+m_speed/1000.0)*(1.0+m_tempo/1000.0);
}

void CMusic::f_SetPitch()
{
	m_pitch_change=1.0;
}

void CMusic::f_GetData(CStringArray *sa)
{
	CString str;

	if (!sa)	{return;}
	if (sa->GetSize()<20)
	{
		sa->SetSize(20);
	}

	sa->SetAt(0,m_title);
	sa->SetAt(1,m_label);
	sa->SetAt(2,m_label_no);
	sa->SetAt(3,m_category);
	if (m_auto_repeat)
	{
		sa->SetAt(4,"ON");
	}
	else
	{
		sa->SetAt(4,"OFF");
	}
	sa->SetAt(5,m_rep_start);
	sa->SetAt(6,m_rep_end);

	str.Format("%d",m_wait_time);
	sa->SetAt(7,str);

	str.Format("%.2f",m_tempo_bpm);
	sa->SetAt(8,str);

	str.Format("%+d",m_speed);
	sa->SetAt(9,str);

	str.Format("%+d",m_pitch);
	sa->SetAt(10,str);

	str.Format("%+d",m_tempo);
	sa->SetAt(11,str);

	if (m_data_type==0)
	{
		sa->SetAt(12,"Wave");
	}
	else if (m_data_type==1)
	{
		sa->SetAt(12,"MIDI");
	}
	else if (m_data_type==2)
	{
		sa->SetAt(12,"MPEG");
	}
	else
	{
		sa->SetAt(12,"unknown");
	}

	sa->SetAt(13,m_filename);

	str.Format("%.3f",m_rep_mix);
	sa->SetAt(14,str);

	str.Format("%.4f",m_tempo_org);
	sa->SetAt(15,str);

	str.Format("%d",m_beats_per_rep);
	sa->SetAt(16,str);

	str.Format("%d",m_data_num);
	sa->SetAt(17,str);

	sa->SetAt(18,m_flags);
	
	sa->SetAt(19,m_cue_file+";"+m_cue_prog+";");

	return;
}

int CMusic::f_SetRepTime(void)
{
	double start,end;
	if (m_rep_start=="")
	{
		start=0.0;
	}
	else
	{
		start=SdpSetStringToTime(m_rep_start,2)/1000.0;
	}
	if (m_rep_end=="")
	{
		end=f_GetLength();
	}
	else
	{
		end=SdpSetStringToTime(m_rep_end,2)/1000.0;
	}
	return f_SetRepTime(start,end);
}

double CMusic::f_GetTempoInFile()
{
	return 0.0;
}

void CMusic::f_Activate(void)
{
	f_Open();
	f_SetStartTime(m_paused_time);
}

void CMusic::f_InActivate(void)
{
	m_paused_time=f_GetTime();
	f_Close();
}

void CMusic::OnLoad() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	CWaitCursor wait;
	SetCurrentDirectory(m_base_dir);

	if (f_Load())
	{
		f_Open();
		DWORD vol;
		f_GetVolume(&vol);
		if (vol==0)
		{
			f_SetVolume(0xFFFFFFFF);
		}
		if (m_wait_time>0)
		{
			m_wait=m_wait_time;
		}
		else
		{
			m_wait=0;
		}
	}
	else
	{
		m_wait=0;
	}
	OnTimer(0);
	SdpSetTimeToString(f_GetLength(),m_music_len,2);
}

void CMusic::OnUnLoad() 
{
	f_UnLoad();
	m_stat_playing=0;
	m_stat_play=0;
	m_stat_ready=0;
	m_stat_load=0;
	m_stat_repeat=0;
	m_stat_preparing=0;
	m_dsp_on=1;
	f_InitData();
}

void CMusic::OnPlay() 
{
	m_fadeout_len=m_fadeout=0;
	if (m_play_on)
	{
		if (m_fadeout_vol>0)
		{
			f_SetVolume(m_fadeout_vol);
			m_fadeout_vol=0;
		}
		if (m_wait>0)
		{
			SetTimer(2,1000,NULL);
			m_status_text0="playing (wait)";
			f_SetStatusText();
		}
		else if (m_wait<=0 && f_Start())
		{
			m_wait=0;
			SetTimer(1,100,NULL);
		}
		else
		{
			m_play_on=0;
			OnTimer(1);
//			if (m_dialog)	{m_dialog->UpdateData(FALSE);}
		}
	}
	else
	{
		KillTimer(1);
		OnTimer(0);
		f_Stop();
	}
//	if (m_dialog)	{m_dialog->UpdateData(FALSE);}
}

void CMusic::OnFadeOut(int len)
{
	if (m_play_on)
	{
		if (m_fadeout_len==0)
		{
			if (len>0)
			{
				m_fadeout_len=m_fadeout=len;
				DWORD vol;
				f_GetVolume(&vol);
				if (vol>0){m_fadeout_vol=vol;}
			}
			else
			{
				m_play_on=0;
				OnPlay();
			}
		}
	}
}

void CMusic::OnRewind() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	KillTimer(1);
//	if (f_Reset())  // WinME,2000だとリセット後の再生で前の音が一瞬残ってしまうので、再オープンするようにする。
	if (f_Open())
	{
		if (m_wait_time>0)
		{
			m_wait=m_wait_time;
		}
		else
		{
			m_wait=0;
		}
		f_SetStartTime(-m_wait_time);
	}
	OnTimer(0);
	OnPlay();
}

/////////////////////////////////////////////////////////////////////////////
// CMusic メッセージ ハンドラ

void CMusic::OnTimer(UINT nIDEvent) 
{
	// TODO: この位置にメッセージ ハンドラ用のコードを追加するかまたはデフォルトの処理を呼び出してください
	double time;

	time=f_GetTime();

	if (nIDEvent==1)
	{
		if (!m_stat_playing)
		{
			KillTimer(1);
			f_Stop();
			m_play_on=0;
			m_fadeout=0;
		}

		if (m_fadeout_len>0)
		{
			DWORD vl,vr,vol;
			double v;
			m_fadeout--;
			if (m_fadeout<0){m_fadeout=0;}
			vl=m_fadeout_vol&0xFFFF;
			vr=(m_fadeout_vol>>16)&0xFFFF;
			v=(double)m_fadeout/(double)m_fadeout_len;
			vl=DWORD(vl*v);
			vr=DWORD(vr*v);
			vol=vr<<16|vl;
			f_SetVolume(vol);
			if (m_fadeout==0)
			{
				m_play_on=0;
				OnPlay();
//				f_SetVolume(m_fadeout_vol);
			}
		}
	}
	else if (nIDEvent==2)
	{
		if (m_fadeout_len>0)
		{
			KillTimer(2);
			m_fadeout_len=m_fadeout=0;
			f_Stop();
//			f_SetVolume(m_fadeout_vol);
		}
		else if (--m_wait<=0)
		{
			KillTimer(2);
			m_wait=0;
			OnPlay();
		}
		else if (!m_play_on)
		{
			KillTimer(2);
			f_Stop();
		}
	}

	if (m_wait>0)	{time-=m_wait;}
	SdpSetTimeToString(time,m_time,1);
	if (m_dialog)	{m_dialog->PostMessage(WM_TIMER,nIDEvent);}
	
//	CWnd::OnTimer(nIDEvent);
}

LRESULT CMusic::OnWaveOutDone(WPARAM wParam, LPARAM lParam)
{
	if (m_object_type==0 || m_object_type==2 )
	{
		CString str;
	//	str.Format("done(hd%d) %d",wh - m_wh,m_nbuf);
		str.Format("playing (buf %d)",m_nbuf);
		f_SetStatusText(0,str);
//		f_OutProcDone(wParam,lParam);
	}
	return 1;
}

LRESULT CMusic::OnMidiOutDone(WPARAM wParam, LPARAM lParam)
{
	if (m_object_type==1)
	{
		CString str;
	//	str.Format("done(hd%d) %d",wh - m_wh,m_nbuf);
		str.Format("playing (buf %d)",m_nbuf);
		f_SetStatusText(0,str);
//		f_OutProcDone(wParam,lParam);
	}
	return 1;
}
